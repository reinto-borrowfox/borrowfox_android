/*
 *
 * CircularIndicator.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 8.7.16 12:56
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class CircularIndicator extends LinearLayout {

    private List<String> urls;
    private int active_index = 0;
    private int number_of_pages = 0;
    private List<ImageView> indicators;
    private int indicator_height = 12;

    public CircularIndicator(Context context) {
        super(context);
        setupUI();
    }

    public CircularIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CircularIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public void setIndicator_height(int x) {
        indicator_height = x;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public int getActive_index() {
        return active_index;
    }

    public void setActive_index(int active_index) {
        this.active_index = active_index;
    }

    public int getNumber_of_pages() {
        return number_of_pages;
    }

    public void setNumber_of_pages(int number_of_pages) {
        this.number_of_pages = number_of_pages;
    }

    public void setupUI() {
        indicators = new ArrayList<>();

        this.setOrientation(LinearLayout.HORIZONTAL);
        this.removeAllViews();

        LayoutParams params = new LayoutParams(Utilities.convertDpToPixels(indicator_height, getContext()),
                Utilities.convertDpToPixels(indicator_height, getContext()));
        params.setMargins(0, 0, Utilities.convertDpToPixels(4, getContext()), 0);

        for (int i = 0; i < number_of_pages; i++) {
            ImageView imageview = new ImageView(getContext());
            imageview.setLayoutParams(params);

            if (active_index == i) {
                imageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.filled_circular_indicator));
            } else {
                imageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.empty_circular_indicator));
            }

            this.addView(imageview);
            indicators.add(imageview);
        }
    }

    public void changeIndicator(int x) {
        indicators.get(active_index).setImageDrawable(getContext().getResources().getDrawable(R.drawable.empty_circular_indicator));
        active_index = x;
        indicators.get(active_index).setImageDrawable(getContext().getResources().getDrawable(R.drawable.filled_circular_indicator));
    }
}
