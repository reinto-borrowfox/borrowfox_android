package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

public class CustomDistanceSeekBar extends RelativeLayout {

    private static final int MAX_DISTANCE = 30;
    private ImageView selector;

    public CustomDistanceSeekBar(Context context) {
        super(context);
        setupUI();
    }

    public CustomDistanceSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomDistanceSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_distance_seekbar, this);

        final TextView percent2 = (TextView) findViewById(R.id.percent2);
        selector = (ImageView) findViewById(R.id.view_selected);
        final int[] _xDelta = new int[2];

        ImageView slider2 = (ImageView) findViewById(R.id.slider2);
        slider2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        LayoutParams lParams = (LayoutParams) view.getLayoutParams();
                        _xDelta[1] = X + lParams.rightMargin;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                        if (-X + _xDelta[1] < Utilities.convertDpToPixels(0, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(0, getContext());
                        } else if (-X + _xDelta[1] > Utilities.convertDpToPixels(293, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(293, getContext());
                        } else {
                            layoutParams.rightMargin = -X + _xDelta[1];
                        }

                        int q = Utilities.convertDpToPixels(293, getContext());
                        int v = Utilities.convertDpToPixels(0, getContext());

                        float xx = MAX_DISTANCE - ((layoutParams.rightMargin - v) / (float) (q - v)) * MAX_DISTANCE;

                        int rounded_value = Math.round(xx * 10);
                        percent2.setText(String.format("%sm", String.valueOf(rounded_value / 10.0f)));

                        view.setLayoutParams(layoutParams);
                        updateSelectorRight(layoutParams.rightMargin - v);
                        break;
                }
                invalidate();
                return true;
            }
        });
    }

    public void updateSelectorRight(int mar) {
        LayoutParams params = (LayoutParams) selector.getLayoutParams();
        params.rightMargin = mar;
        selector.setLayoutParams(params);
    }
}
