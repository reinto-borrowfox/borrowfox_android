/*
 *
 * CustomDiscountSeekBar.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 10:38
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

public class CustomDiscountSeekBar extends RelativeLayout {

    private static final int MAX_DISTANCE = 100;
    private ImageView selector;
    private int money = 5;
    private TextView min_price;
    private int rounded_value = 55;

    private TextView percent2;

    public CustomDiscountSeekBar(Context context) {
        super(context);
        setupUI();
    }

    public CustomDiscountSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomDiscountSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public int getDiscount() {
        String temp = percent2.getText().toString();
        return Integer.valueOf(temp.substring(0, temp.length() - 1));
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_discount_seekbar, this);

        min_price = (TextView) findViewById(R.id.min_price);
        final TextView zero_per = (TextView) findViewById(R.id.percent1);
        percent2 = (TextView) findViewById(R.id.percent2);
        selector = (ImageView) findViewById(R.id.view_selected);
        final int[] _xDelta = new int[2];

        ImageView slider2 = (ImageView) findViewById(R.id.slider2);
        slider2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        LayoutParams lParams = (LayoutParams) view.getLayoutParams();
                        _xDelta[1] = X + lParams.rightMargin;
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                        if (-X + _xDelta[1] < Utilities.convertDpToPixels(0, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(0, getContext());
                        } else if (-X + _xDelta[1] > Utilities.convertDpToPixels(370, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(370, getContext());
                        } else {
                            layoutParams.rightMargin = -X + _xDelta[1];
                        }

                        if (-X + _xDelta[1] > Utilities.convertDpToPixels(345, getContext())) {
                            zero_per.setVisibility(INVISIBLE);
                        } else {
                            zero_per.setVisibility(VISIBLE);
                        }

                        int q = Utilities.convertDpToPixels(370, getContext());
                        int v = Utilities.convertDpToPixels(0, getContext());

                        float xx = MAX_DISTANCE - ((layoutParams.rightMargin - v) / (float) (q - v)) * MAX_DISTANCE;

                        rounded_value = Math.round(xx);

                        ((MainActivity) getContext()).newItem.setPercentStepTwo(rounded_value);

                        percent2.setText(String.format("%s%%", String.valueOf(rounded_value)));

                        min_price.setText("£" + String.valueOf(Math.round(money * (rounded_value / 100f))));

                        view.setLayoutParams(layoutParams);
                        updateSelectorRight(layoutParams.rightMargin - v);
                        break;
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                invalidate();
                return true;
            }
        });
    }

    public void setMoney(int x) {
        money = x;
        min_price.setText("£" + String.valueOf(Math.round(money * (rounded_value / 100f))));
    }

    public void setPercent(int x) {
        rounded_value = x;
        percent2.setText(String.format("%s%%", String.valueOf(rounded_value)));

    }

    public void updateSelectorRight(int mar) {
        LayoutParams params = (LayoutParams) selector.getLayoutParams();
        params.rightMargin = mar;
        selector.setLayoutParams(params);
    }
}
