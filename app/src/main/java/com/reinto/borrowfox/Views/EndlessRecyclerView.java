package com.reinto.borrowfox.Views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by budac on 19.11.16.
 */

public class EndlessRecyclerView extends RecyclerView {

    private OnEndReachListener onEndReachListener;
    private LinearLayoutManager layoutManager;
    private int threshold = 5;

    public EndlessRecyclerView(Context context) {
        super(context);
    }

    public EndlessRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EndlessRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnEndReachListener(OnEndReachListener onEndReachListener){
        this.onEndReachListener = onEndReachListener;
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);

        if(layout instanceof LinearLayoutManager)
            layoutManager = (LinearLayoutManager) layout;
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);

        if(layoutManager == null) {
            Log.e("LL", "[EndlessRecyclerView] LinearLayoutManager not set");
            return;
        }

        if(dy > 0) { //check for scroll down
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount - threshold) {
                onEndReachListener.onEndReach();
            }
        }

    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
}
