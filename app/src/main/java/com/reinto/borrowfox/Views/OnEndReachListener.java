package com.reinto.borrowfox.Views;

/**
 * Created by budac on 19.11.16.
 */

public interface OnEndReachListener {

    void onEndReach();

}
