/*
 *
 * CustomSwitch.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 4.7.16 9:05
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;

public class CustomSwitch extends LinearLayout {

    private boolean is_currier_selected = true;
    private LinearLayout linearLayout;
    private TextView[] array;
    private OnStatusChange onStatusChange;

    public CustomSwitch(Context context) {
        super(context);
        setupUI();
    }

    public CustomSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public void setupTargetLayout(LinearLayout layout) {
        linearLayout = layout;
    }

    public void setupText(TextView... textviews) {
        array = textviews;
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_custom_switch, this);

        final TextView person = (TextView) findViewById(R.id.person_delivery);
        final TextView currier = (TextView) findViewById(R.id.currier);

        person.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_currier_selected) {
                    linearLayout.setVisibility(GONE);
                    person.setTextColor(getResources().getColor(R.color.white));
                    person.setBackgroundResource(R.color.weird_green);
                    currier.setTextColor(getResources().getColor(R.color.dialog_grey));
                    currier.setBackgroundResource(android.R.color.transparent);

                    array[0].setTextColor(getResources().getColor(R.color.black87));
                    array[1].setTextColor(getResources().getColor(R.color.black87));

                    is_currier_selected = false;
                    onStatusChange.OnChange(2);
                }
            }
        });

        currier.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!is_currier_selected) {
                    linearLayout.setVisibility(VISIBLE);
                    currier.setTextColor(getResources().getColor(R.color.white));
                    currier.setBackgroundResource(R.color.weird_green);
                    person.setTextColor(getResources().getColor(R.color.dialog_grey));
                    person.setBackgroundResource(android.R.color.transparent);

                    array[0].setTextColor(getResources().getColor(R.color.black54tra));
                    array[1].setTextColor(getResources().getColor(R.color.black54tra));

                    is_currier_selected = true;
                    onStatusChange.OnChange(1);
                }
            }
        });
    }

    public void setOnStatusChange(OnStatusChange onStatusChange) {
        this.onStatusChange = onStatusChange;
    }

    public interface OnStatusChange {
        void OnChange(int x);
    }
}
