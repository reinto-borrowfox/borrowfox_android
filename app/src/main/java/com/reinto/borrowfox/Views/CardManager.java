/*
 *
 * CardManager.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 13.7.16 11:29
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.reinto.borrowfox.Models.CardInfo;
import com.reinto.borrowfox.R;

import java.util.ArrayList;

public class CardManager extends LinearLayout {

    ArrayList<CardInfo> list;
    private boolean is_open = false;
    private Button parent;
    private ArrayList<CardInfoView> views;

    public CardManager(Context context) {
        super(context);
    }

    public CardManager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardManager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setupUI() {
        setOrientation(VERTICAL);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.card_manager, this, true);
    }

    public void setData(ArrayList<CardInfo> list) {
        setupUI();
        this.list = new ArrayList<>(list);

        views = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            CardInfoView v = new CardInfoView(getContext(), list.get(i), null);
            addView(v, 0);
            views.add(v);
        }

        parent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_open) {
                   // findViewById(R.id.cardmanager).setVisibility(View.GONE);
                   // findViewById(R.id.divider).setVisibility(View.GONE);
                    is_open = false;
                } else {
                   // findViewById(R.id.cardmanager).setVisibility(View.VISIBLE);
                    findViewById(R.id.divider).setVisibility(View.VISIBLE);
                    is_open = true;
                }
            }
        });

        setupCardClick();
    }

    private void setupCardClick() {
        final Button add_new_card = (Button) findViewById(R.id.add_new_card);

        for (int i = 0; i < views.size(); i++) {
            final int finalI = i;
            views.get(i).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((CardInfoView) view).isActive) {
                        ((CardInfoView) view).hideIt();
                        add_new_card.setVisibility(VISIBLE);
                        showRest();
                    } else {
                        ((CardInfoView) view).showIt();
                        hideRest(finalI);
                        add_new_card.setVisibility(GONE);
                    }
                }
            });
        }
    }

    private void hideRest(final int id) {
        for (int i = 0; i < views.size(); i++) {
            if (i == id) {
                continue;
            }

            views.get(i).hideuFully();
        }
    }

    private void showRest() {
        for (int i = 0; i < views.size(); i++) {
            views.get(i).showFully();
        }
    }

    public void setMainButton(Button layout) {
        this.parent = layout;
    }
}
