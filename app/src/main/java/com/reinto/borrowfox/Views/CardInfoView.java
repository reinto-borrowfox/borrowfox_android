/*
 *
 * CardInfoView.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 12.7.16 14:32
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.reinto.borrowfox.Models.CardInfo;
import com.reinto.borrowfox.R;

public class CardInfoView extends LinearLayout {

    public boolean isActive = false;
    private CardInfo cardInfo;

    public CardInfoView(Context context, CardInfo info, LayoutTransition transition) {
        super(context);
        cardInfo = info;
        setupUI();
    }

    public CardInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CardInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    @Override
    public String toString() {
        return "CardInfoView{" +
                "cardInfo=" + cardInfo +
                '}';
    }

    public void addData(CardInfo info) {
        cardInfo = info;
        invalidate();
    }

    private void setupUI() {
        setOrientation(VERTICAL);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_settings_show_card, this, true);

        if (cardInfo != null) {

            /*TextView card_number = (TextView) findViewById(R.id.card_number);
            card_number.setText(cardInfo.getCard_number());

            card_number.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    findViewById(R.id.parent).setVisibility(VISIBLE);
                }
            });

            TextView user_name = (TextView) findViewById(R.id.name);
            user_name.setText(cardInfo.getUser_name());

            TextView month = (TextView) findViewById(R.id.month);
            month.setText(String.valueOf(cardInfo.getMonth()));

            TextView year = (TextView) findViewById(R.id.year);
            year.setText(String.valueOf(cardInfo.getYear()));*/
        }
    }

    public ViewGroup getData() {
        return (RelativeLayout) findViewById(R.id.parent);
    }


    public void showIt() {
        findViewById(R.id.parent).setVisibility(VISIBLE);
        isActive = true;
    }

    public void hideIt() {
        findViewById(R.id.parent).setVisibility(GONE);
        isActive = false;
    }

    public void showFully() {
        setVisibility(VISIBLE);
    }

    public void hideuFully() {
        setVisibility(GONE);
    }
}
