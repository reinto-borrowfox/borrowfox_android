package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class RatioImageView extends ImageView {

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth / 2, parentHeight);
        this.setLayoutParams(new RelativeLayout.LayoutParams(parentWidth / 2, parentHeight));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //        Log.d(App.TAG, "RatioImageView, onMeasure: " + widthMeasureSpec + ", " + heightMeasureSpec);
        //
        //        int height = (int) (widthMeasureSpec * 0.75f);
        //        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //
        //        Log.d(App.TAG, "RatioImageView, onMeasure: " + widthMeasureSpec + ", " + height);
        //
        //        int width = getMeasuredWidth();
        //        //int height = Math.round(width * .75f);
        //        setMeasuredDimension(width, height);

        //        int w = MeasureSpec.getSize(widthMeasureSpec);
        //        int h = MeasureSpec.getSize(heightMeasureSpec);
        //
        //        float aspect = 0.75f;
        //        if (w == 0) {
        //            h = 0;
        //        } else if (h / w < aspect) {
        //            w = (int) (h / aspect);
        //        } else {
        //            h = (int) (w * aspect);
        //        }
        //
        //        super.onMeasure(
        //                MeasureSpec.makeMeasureSpec(w,
        //                        MeasureSpec.getMode(widthMeasureSpec)),
        //                MeasureSpec.makeMeasureSpec(h,
        //                        MeasureSpec.getMode(heightMeasureSpec)));
    }
}
