/*
 *
 * DragableView.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 28.6.16 13:29
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.fragments.add_new_item.PhotosListFragment;

public class DragableView extends FrameLayout {

    public ImageView background;
    public ImageButton delete;
    public String address;
    private Bitmap bitmap;

    public DragableView(Context context) {
        super(context);
        setupUI();
    }

    public DragableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public DragableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_just_imageview, this);

        background = (ImageView) findViewById(R.id.source);
        delete = (ImageButton) findViewById(R.id.delete);

        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = ((BitmapDrawable) background.getDrawable()).getBitmap();
                background.setImageBitmap(null);
                setVisibility(INVISIBLE);
                ((PhotosListFragment) ((MainActivity) getContext()).getSupportFragmentManager()
                        .findFragmentByTag(PhotosListFragment.class.getName())).setOrder();

                RelativeLayout parent = ((PhotosListFragment) ((MainActivity) getContext()).getSupportFragmentManager()
                        .findFragmentByTag(PhotosListFragment.class.getName())).parent;

                Snackbar snackbar = Snackbar.make(parent, "Item deleted.", Snackbar.LENGTH_LONG);
                snackbar.setAction("TAKE BACK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((PhotosListFragment) ((MainActivity) getContext()).getSupportFragmentManager()
                                .findFragmentByTag(PhotosListFragment.class.getName())).putBitmapBack(bitmap);
                        ((PhotosListFragment) ((MainActivity) getContext()).getSupportFragmentManager()
                                .findFragmentByTag(PhotosListFragment.class.getName())).setOrder();
                    }
                });

                snackbar.setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        if (event != DISMISS_EVENT_ACTION) {
                            ((PhotosListFragment) ((MainActivity) getContext()).getSupportFragmentManager()
                                    .findFragmentByTag(PhotosListFragment.class.getName())).numberOfPhotos--;
                            address = null;
                        }
                        super.onDismissed(snackbar, event);
                    }
                });

                snackbar.setActionTextColor(getContext().getResources().getColor(R.color.button_red));

                snackbar.show();
            }
        });
    }

    public void setBitmap(Bitmap b) {
        if (background != null) {
            background.setImageBitmap(b);
        }
    }


    public void setupEditMode(boolean b) {
        if (b) {
            delete.setVisibility(VISIBLE);
        } else {
            delete.setVisibility(GONE);
        }
    }
}

