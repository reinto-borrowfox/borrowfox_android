/*
 *
 * CalendarDialogStepThree.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 13:24
 * Copyright (c) year Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.CalendaDayInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.managers.CalendarManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarCreateItem extends LinearLayout {

    public boolean showApiData = false;
    private int month_number = 0;
    private TableLayout table;
    private List<Long> selectedDays;
    private List<TextView> textViews;
    private List<Long> calendar_times;
    private List<CalendaDayInfo> list;
    private List<Long> selecteddays;

    public CalendarCreateItem(Context context) {
        super(context);
        setupUI();
    }

    public CalendarCreateItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CalendarCreateItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_add_item_step_three_calendar, this);

        table = (TableLayout) findViewById(R.id.table);
        selectedDays = new ArrayList<>();
        selecteddays = new ArrayList<>();
        calendar_times = new ArrayList<>();
        textViews = new ArrayList<>();

        prepareMonthTitle();
    }

    public void importSelectedData(List<Long> list) {
        selecteddays = new ArrayList<>(list);
    }

    public void importAPIData(List<CalendaDayInfo> list) {
        this.list = new ArrayList<>(list);
        drawMonth(month_number);
    }

    public List<Long> getSelectedDays() {
        return selectedDays;
    }

    public void setSelectedDays(List<Long> list) {
        selecteddays.clear();
        selecteddays.addAll(list);
        setupUI();
    }

    private void prepareMonthTitle() {
        final TextView month = (TextView) findViewById(R.id.month);
        ImageButton next_month = (ImageButton) findViewById(R.id.next_month);
        final ImageButton previousMonth = (ImageButton) findViewById(R.id.previous_month);

        final java.util.Calendar today = java.util.Calendar.getInstance();
        drawMonth(month_number);

        month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));

        next_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                today.add(Calendar.MONTH, 1);
                month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                month_number++;

                drawMonth(month_number);
                previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
            }
        });

        previousMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (month_number > 0) {
                    today.add(Calendar.MONTH, -1);
                    month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                    month_number--;

                    drawMonth(month_number);

                    if (month_number == 0) {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left_trans, null));
                    } else {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
                    }
                }
            }
        });
    }

    private void drawMonth(int added_month_count) {
        cleanCalendar();
        calendar_times.clear();
        textViews.clear();

        Calendar today = Calendar.getInstance();
        today.add(Calendar.MONTH, added_month_count);

        Calendar dynamic_today = java.util.Calendar.getInstance();
        dynamic_today.add(Calendar.MONTH, added_month_count);

        final Calendar calendar = java.util.Calendar.getInstance();
        calendar.add(Calendar.MONTH, added_month_count);

        int diff = calendar.get(java.util.Calendar.DAY_OF_WEEK) - 2;
        if (diff == -1) diff = 6;
        calendar.setTimeInMillis(calendar.getTimeInMillis() - 1000 * 60 * 60 * 24 * diff);
        // month is the same
        while (dynamic_today.get(java.util.Calendar.MONTH) + 1 == calendar.get(java.util.Calendar.MONTH) + 1) {
            calendar.add(java.util.Calendar.DAY_OF_MONTH, -7);
        }

        int temp = 0;
        int temp2 = 0;

        for (int i = 1; i < 7; i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < 7; j++) {
                LinearLayout l = (LinearLayout) row.getChildAt(j);
                TextView text = (TextView) l.getChildAt(0);
                text.setTag(calendar.getTimeInMillis());
                text.setText(String.valueOf(calendar.get(java.util.Calendar.DAY_OF_MONTH)));
                textViews.add(text);

                if (calendar.get(Calendar.MONTH) != today.get(Calendar.MONTH)) {
                    text.setTextColor(getResources().getColor(R.color.black54tra));
                }

                if (!showApiData) {
                    calendar_times.add(calendar.getTimeInMillis());
                    text.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view.getAlpha() == 0.99f) {
                                ((TextView) view).setTextColor(getResources().getColor(R.color.black54));
                                view.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                                view.setAlpha(1);
                                selectedDays.remove(calendar_times.get(textViews.indexOf(view)));
                            } else {
                                ((TextView) view).setTextColor(getResources().getColor(R.color.white));
                                view.setBackgroundColor(getResources().getColor(R.color.button_red));
                                view.setAlpha(0.99f);
                                selectedDays.add(calendar_times.get(textViews.indexOf(view)));
                            }
                        }
                    });

                    // import already selected days
                    for (int q = temp2; q < selectedDays.size(); q++) {
                        Calendar qq = Calendar.getInstance();
                        qq.setTimeInMillis(selectedDays.get(q));
                        if (checkTheSameDay(qq, calendar)) {
                            temp = q;
                            text.setBackgroundResource(R.color.button_red);
                            text.setTextColor(getContext().getResources().getColor(R.color.white));
                        }
                    }
                } else {
                    //
                    text.setOnClickListener(null);

                    for (int k = temp; k < list.size(); k++) {
                        if (checkTheSameDay(list.get(k).getC(), calendar)) {
                            temp = k;
                            text.setBackgroundResource(R.color.black54tra);
                            text.setTextColor(getContext().getResources().getColor(R.color.black54));
                        }
                    }

                    for (int q = temp2; q < selecteddays.size(); q++) {
                        Calendar qq = Calendar.getInstance();
                        qq.setTimeInMillis(selecteddays.get(q));
                        if (checkTheSameDay(qq, calendar)) {
                            temp = q;
                            text.setBackgroundResource(R.color.button_red);
                            text.setTextColor(getContext().getResources().getColor(R.color.white));
                        }
                    }
                }

                calendar.add(java.util.Calendar.DAY_OF_MONTH, 1);
            }
        }

        TextView sixth = (TextView) findViewById(R.id.sixths);
        if (Integer.valueOf(sixth.getText().toString()) < 15) {
            TableRow row = (TableRow) findViewById(R.id.sixth_week);
            row.setVisibility(View.GONE);
        } else {
            TableRow row = (TableRow) findViewById(R.id.sixth_week);
            row.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkTheSameDay(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void cleanCalendar() {
        for (int i = 1; i < 7; i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < 7; j++) {
                LinearLayout l = (LinearLayout) row.getChildAt(j);
                final TextView text = (TextView) l.getChildAt(0);
                text.setBackgroundResource(android.R.color.transparent);
                text.setTextColor(getContext().getResources().getColor(R.color.black54));
            }
        }
    }
}
