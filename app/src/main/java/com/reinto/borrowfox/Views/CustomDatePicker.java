package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;

import java.util.Calendar;

public class CustomDatePicker extends LinearLayout {

    private static final String TAG = "tag";
    private boolean firstActive = false;
    private boolean secondActive = false;

    private int l1 = 0, l2, l3;
    private int r1 = 0, r2, r3;

    public CustomDatePicker(Context context) {
        super(context);
        setupUI();
    }

    public CustomDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomDatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public String getFromDate() {
        if (l1 != 0) {
            return l1 + "-" + (l2+1) + "-" + l3;
        } else {
            return "";
        }
    }

    public String getToDate() {
        if (r1 != 0) {
            return r1 + "-" + (r2+1) + "-" + r3;
        } else {
            return "";
        }
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_dates_selector, this);
        final String[] months = new String[]{
                "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
        };

        final TextView first = (TextView) findViewById(R.id.first_date);
        final TextView second = (TextView) findViewById(R.id.second_date);

        Calendar c = Calendar.getInstance();

        l1 = c.get(Calendar.YEAR);
        l2 = c.get(Calendar.MONTH);
        l3 = c.get(Calendar.DAY_OF_MONTH);

        r1 = c.get(Calendar.YEAR);
        r2 = c.get(Calendar.MONTH) + 1;
        r3 = c.get(Calendar.DAY_OF_MONTH);

        final DatePicker datePicker = (DatePicker) findViewById(R.id.datePickerbla);
        datePicker.init(l1, l2, l3, new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                if (firstActive) {
                    first.setText(months[i1] + "\n" + i2 + "\n" + i);
                    l1 = i;
                    l2 = i1;
                    l3 = i2;
                    Log.d(TAG, "onDateChanged: first saved" + l1 + ", " + l2 + ", " + l3);
                } else if (secondActive) {
                    second.setText(months[i1] + "\n" + i2 + "\n" + i);
                    r1 = i;
                    r2 = i1;
                    r3 = i2;
                    Log.d(TAG, "onDateChanged: second saved" + r1 + ", " + r2 + ", " + r3);
                }
            }
        });

        final ImageButton leftDate = (ImageButton) findViewById(R.id.imageButton);
        final ImageButton rightDate = (ImageButton) findViewById(R.id.imageButton2);

        leftDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (firstActive) {
                    datePicker.setVisibility(GONE);
                    firstActive = false;
                    leftDate.setImageResource(R.drawable.arrow_down);
                } else {
                    if (secondActive) {
                        secondActive = false;
                        rightDate.setImageResource(R.drawable.arrow_down);
                    }
                    datePicker.setVisibility(VISIBLE);
                    firstActive = true;
                    datePicker.updateDate(l1, l2, l3);
                    leftDate.setImageResource(R.drawable.arrow_up);
                }
            }
        });

        rightDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (secondActive) {
                    datePicker.setVisibility(GONE);
                    secondActive = false;
                    rightDate.setImageResource(R.drawable.arrow_down);
                } else {
                    if (firstActive) {
                        firstActive = false;
                        leftDate.setImageResource(R.drawable.arrow_down);
                    }

                    datePicker.setVisibility(VISIBLE);
                    secondActive = true;
                    datePicker.updateDate(r1, r2, r3);
                    rightDate.setImageResource(R.drawable.arrow_up);
                }
            }
        });
    }
}
