package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.ArrayList;

public class StartRating extends LinearLayout {

    private int number_of_filled_start = 0;
    private ArrayList<ImageView> stars;

    public StartRating(Context context, int number) {
        super(context);
        number_of_filled_start = number;
        setupUI();
    }

    public StartRating(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public StartRating(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        stars = new ArrayList<>();
        this.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams params = new LayoutParams(Utilities.convertDpToPixels(14, getContext()),
                Utilities.convertDpToPixels(14, getContext()));
        params.setMargins(0, 0, Utilities.convertDpToPixels(4, getContext()), 0);

        for (int i = 0; i < 5; i++) {
            ImageView imageview = new ImageView(getContext());
            imageview.setLayoutParams(params);

            if (i < number_of_filled_start) {
                imageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.filled_star));
            } else {
                imageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.empty_star));
            }

            this.addView(imageview);
            stars.add(imageview);
        }
    }

    public void setFilledStart(int x) {
        number_of_filled_start = x;
        for (int i = 0; i < 5; i++) {
            if (i < x) {
                stars.get(i).setImageDrawable(getContext().getResources().getDrawable(R.drawable.filled_star));
            } else {
                stars.get(i).setImageDrawable(getContext().getResources().getDrawable(R.drawable.empty_star));
            }
        }
    }
}
