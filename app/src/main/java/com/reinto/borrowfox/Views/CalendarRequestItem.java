package com.reinto.borrowfox.Views;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.CalendaDayInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.managers.CalendarManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarRequestItem extends Dialog {

    private int month_number = 0;
    private TableLayout table;
    private List<CalendaDayInfo> list;
    private int min_rental_period;

    private CalendarManager data;
    private TextView from;
    private TextView to;

    private OnMyDialogResult mDialogResult;

    public CalendarRequestItem(Context context, List<CalendaDayInfo> list, int minimal_rental_length) {
        super(context);
        this.list = new ArrayList<>(list);
        min_rental_period = minimal_rental_length;
        Log.d(App.TAG, "CalendarRequestItem, CalendarRequestItem: " + minimal_rental_length);
        setupUI();
    }

    public CalendarRequestItem(Context context, int themeResId) {
        super(context, themeResId);
        setupUI();
    }

    protected CalendarRequestItem(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setupUI();
    }

    private void setupUI() {
        setContentView(R.layout.layout_item_detail_calendar);
        getWindow().setBackgroundDrawableResource(R.color.dialog_grey);

        data = new CalendarManager(getContext());

        table = (TableLayout) findViewById(R.id.table);

        data.generateCalendar(month_number);
        data.setMinimalRentalLength(min_rental_period + 1);
        data.importAPIData(list);

        from = (TextView) findViewById(R.id.date_from);
        to = (TextView) findViewById(R.id.date_to);
        data.setTextviews(from, to);

        refreshCalendar();

        final TextView month = (TextView) findViewById(R.id.month);
        ImageButton next_month = (ImageButton) findViewById(R.id.next_month);
        final ImageButton previousMonth = (ImageButton) findViewById(R.id.previous_month);

        final Calendar today = Calendar.getInstance();

        month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));

        next_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                today.add(Calendar.MONTH, 1);
                month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                month_number++;
                data.generateCalendar(month_number);
                data.importAPIData(list);
                refreshCalendar();
                previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
            }
        });

        previousMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (month_number > 0) {
                    today.add(Calendar.MONTH, -1);
                    month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                    month_number--;
                    data.generateCalendar(month_number);
                    data.importAPIData(list);
                    refreshCalendar();

                    if (month_number == 0) {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left_trans, null));
                    } else {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
                    }
                }
            }
        });

        Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Button ok = (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new OKListener());
    }

    private void refreshCalendar() {
        TableRow row = (TableRow) table.getChildAt(6);
        row.setVisibility(View.VISIBLE);

        TableRow.LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                Utilities.convertDpToPixels(30, getContext()));
        params.weight = 1;
        params.gravity = Gravity.CENTER;

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(Utilities.convertDpToPixels(30, getContext()),
                Utilities.convertDpToPixels(30, getContext()));
        params2.gravity = Gravity.CENTER;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(data.month_data.get(0).time);

        c.add(Calendar.DAY_OF_MONTH, 7);

        int month = c.get(Calendar.MONTH);

        // gets to new month
        while (month == c.get(Calendar.MONTH)) {
            c.add(Calendar.DAY_OF_MONTH, 1);
        }

        // return to monday
        while (c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            c.add(Calendar.DAY_OF_MONTH, -1);
        }

        Calendar cc = Calendar.getInstance();
        cc.setTimeInMillis(data.month_data.get(0).time);

        long difference = c.getTimeInMillis() - cc.getTimeInMillis();
        int index = (int) (difference / (1000 * 60 * 60 * 24));

        for (int i = 1; i < 7; i++) {
            row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < 7; j++) {

                if (i == 6 && j == 0 && Integer.valueOf(data.month_data.get(index).textview.getText().toString()) < 15) {
                    row.setVisibility(View.GONE);
                    break;
                }

                LinearLayout l = (LinearLayout) row.getChildAt(j);

                l.removeAllViews();

                data.month_data.get(index).textview.setLayoutParams(params2);
                l.addView(data.month_data.get(index).textview, 0);

                index++;
            }
        }
    }

    public void setDialogResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    public interface OnMyDialogResult {
        void finish(String from, String to, List<Long> days);
    }

    private class OKListener implements android.view.View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mDialogResult != null) {
                mDialogResult.finish(from.getText().toString(), to.getText().toString(), data.getBorderDays());
            }
            dismiss();
        }
    }
}