package com.reinto.borrowfox.Views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

/**
 * Created by budac on 19.11.16.
 */

public class EndlessListView extends ListView implements AbsListView.OnScrollListener{

    private OnScrollListener onScrollListener;
    private OnEndReachListener onEndReachListener;
    private int visibleThreshold = 5;

    public EndlessListView(Context context) {
        super(context);
        init();
    }

    public EndlessListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EndlessListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EndlessListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    void init(){
        super.setOnScrollListener(this);
    }

    @Override
    public void setOnScrollListener(OnScrollListener l) {
        onScrollListener = l;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if(onScrollListener != null)
            onScrollListener.onScrollStateChanged(absListView, i);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if(onScrollListener != null)
            onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

        if(getAdapter() == null || getAdapter().getCount() == 0)
            return;

        if ((firstVisibleItem + visibleItemCount ) >= totalItemCount - visibleThreshold) {
            if(onEndReachListener != null)
                onEndReachListener.onEndReach();
        }
    }

    public void setOnEndReachListener(OnEndReachListener onEndReachListener){
        this.onEndReachListener = onEndReachListener;
    }

    public void setVisibleThreshold(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }
}
