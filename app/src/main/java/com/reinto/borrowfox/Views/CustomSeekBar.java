package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.Locale;

public class CustomSeekBar extends RelativeLayout {

    private static final int MAX_PRICE = 250;
    private ImageView selector;
    private TextView max_price;

    public CustomSeekBar(Context context) {
        super(context);
        setupUI();
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public int getMaxPrice() {
        return Integer.valueOf(max_price.getText().toString().replace("£",""));
    }

    private void setupUI() {
        inflate(getContext(), R.layout.speial_seekbar_layout, this);

        final TextView min_price = (TextView) findViewById(R.id.min_price);
        max_price = (TextView) findViewById(R.id.max_price);

        selector = (ImageView) findViewById(R.id.view_selected);

        final int[] _xDelta = new int[2];

        final ImageView slider1 = (ImageView) findViewById(R.id.slider1);
        slider1.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta[0] = X - lParams.leftMargin;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        Log.d("tag", "onTouch: " + (X - _xDelta[0]));
                        if (X - _xDelta[0] < Utilities.convertDpToPixels(0, getContext())) {
                            layoutParams.leftMargin = Utilities.convertDpToPixels(0, getContext());
                        } else if (X - _xDelta[0] > Utilities.convertDpToPixels(293, getContext())) {
                            layoutParams.leftMargin = Utilities.convertDpToPixels(293, getContext());
                        } else {
                            layoutParams.leftMargin = X - _xDelta[0];
                        }

                        int q = Utilities.convertDpToPixels(293, getContext());
                        int v = Utilities.convertDpToPixels(0, getContext());
                        float xx = ((layoutParams.leftMargin - v) / (float) (q - v)) * MAX_PRICE;
                        min_price.setText(String.format(Locale.ENGLISH, "£%d", (int) xx));

                        view.setLayoutParams(layoutParams);
                        updateSelectorLeft(layoutParams.leftMargin - v);
                        break;
                }
                invalidate();
                return true;
            }
        });

        ImageView slider2 = (ImageView) findViewById(R.id.slider2);
        slider2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta[1] = X + lParams.rightMargin;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        if (-X + _xDelta[1] < Utilities.convertDpToPixels(0, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(0, getContext());
                        } else if (-X + _xDelta[1] > Utilities.convertDpToPixels(293, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(293, getContext());
                        } else {
                            layoutParams.rightMargin = -X + _xDelta[1];
                        }

                        int q = Utilities.convertDpToPixels(293, getContext());
                        int v = Utilities.convertDpToPixels(0, getContext());
                        float xx = MAX_PRICE - ((layoutParams.rightMargin - v) / (float) (q - v)) * MAX_PRICE;

                        max_price.setText(String.format(Locale.ENGLISH, "£%d", (int) xx));
                        view.setLayoutParams(layoutParams);
                        updateSelectorRight(layoutParams.rightMargin - v);
                        break;
                }
                invalidate();
                return true;
            }
        });
    }

    public void updateSelectorLeft(int marginLeft) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) selector.getLayoutParams();
        params.leftMargin = marginLeft;
        selector.setLayoutParams(params);
    }

    public void updateSelectorRight(int mar) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) selector.getLayoutParams();
        params.rightMargin = mar;
        selector.setLayoutParams(params);
    }
}
