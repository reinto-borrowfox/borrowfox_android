package com.reinto.borrowfox.Views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

public class ExpandableTextView extends LinearLayout {

    private int text_height = 0;
    private static final String TAG = "tag";
    private final int staticTextHeight = 54;
    private boolean is_showing_more = false;
    private TextView text;

    public ExpandableTextView(Context context) {
        super(context);
        setupUI();
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_expandable_textview, this);

        text = (TextView) findViewById(R.id.text);
        final ImageButton show_more = (ImageButton) findViewById(R.id.more_button);

        text.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        text_height = text.getHeight();
                        if (text_height < Utilities.convertDpToPixels(staticTextHeight, getContext())) {
                            show_more.setVisibility(GONE);
                        }
                        text.setHeight(Utilities.convertDpToPixels(staticTextHeight, getContext()));
                        if (Build.VERSION.SDK_INT > 16) {
                            text.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            text.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
                });

        show_more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_showing_more) {
                    unfade(text, text_height);
                    is_showing_more = false;
                } else {
                    fade(text, text_height);
                    is_showing_more = true;
                }
            }
        });
    }

    private void fade(final View view_to_manipulate, int size) {
        ValueAnimator anim = ValueAnimator.ofInt(Utilities.convertDpToPixels(staticTextHeight, getContext()), size);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view_to_manipulate.getLayoutParams();
                layoutParams.height = val;
                view_to_manipulate.setLayoutParams(layoutParams);
            }
        });

        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }

    private void unfade(final View view_to_manipulate, int size) {
        ValueAnimator anim = ValueAnimator.ofInt(size, Utilities.convertDpToPixels(staticTextHeight, getContext()));
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view_to_manipulate.getLayoutParams();
                layoutParams.height = val;
                view_to_manipulate.setLayoutParams(layoutParams);
            }
        });

        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }
}