/*
 *
 * CalendarEditItem.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 11.7.16 10:27
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class CalendarEditItem extends LinearLayout {

    private int month_number = 0;
    private TableLayout table;
    private List<Long> selectedDays;
    private List<TextView> textViews;
    private List<Long> calendarTimes;
    private List<Long> bookedDays;

    public CalendarEditItem(Context context) {
        super(context);
        setupUI();
    }

    public CalendarEditItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CalendarEditItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_calendar_edit_item, this);

        table = (TableLayout) findViewById(R.id.table);
        selectedDays = new ArrayList<>();
        calendarTimes = new ArrayList<>();
        textViews = new ArrayList<>();
        bookedDays = new ArrayList<>();

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 6);
        bookedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        bookedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        bookedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        bookedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 30);
        bookedDays.add(c.getTimeInMillis());

        c = Calendar.getInstance();
        selectedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        selectedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        selectedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 12);
        selectedDays.add(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 1);
        selectedDays.add(c.getTimeInMillis());

        for (int i = 0; i < selectedDays.size(); i++) {
            Calendar c1 = Calendar.getInstance();
            c1.setTimeInMillis(selectedDays.get(i));
            Log.d(App.TAG, "CalendarEditItem, checkTheSameDayLong: " + c1.get(Calendar.DAY_OF_MONTH) + "/" + (c1.get(Calendar.MONTH) + 1) + "/" + c1.get(Calendar.YEAR));
        }

        prepareMonthTitle();
    }

    private void convertToDate(String selec, long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Log.d(App.TAG, "CalendarEditItem, checkTheSameDayLong: " + selec + ",," + c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR));
    }

    private void prepareMonthTitle() {
        final TextView month = (TextView) findViewById(R.id.month);
        ImageButton nextMonth = (ImageButton) findViewById(R.id.next_month);
        final ImageButton previousMonth = (ImageButton) findViewById(R.id.previous_month);

        final Calendar today = Calendar.getInstance();
        drawMonth(month_number);

        month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));

        nextMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                today.add(Calendar.MONTH, 1);
                month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                month_number++;

                drawMonth(month_number);
                previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
            }
        });

        previousMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (month_number > 0) {
                    today.add(Calendar.MONTH, -1);
                    month.setText(getContext().getResources().getStringArray(R.array.months)[today.get(Calendar.MONTH)] + " " + today.get(Calendar.YEAR));
                    month_number--;

                    drawMonth(month_number);

                    if (month_number == 0) {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left_trans, null));
                    } else {
                        previousMonth.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.arrow_left, null));
                    }
                }
            }
        });
    }

    private void drawMonth(int added_month_count) {
        cleanCalendar();
        calendarTimes.clear();
        textViews.clear();

        Collections.sort(selectedDays);
        Collections.sort(bookedDays);

        Calendar today = Calendar.getInstance();
        today.add(Calendar.MONTH, added_month_count);

        Calendar dynamic_today = java.util.Calendar.getInstance();
        dynamic_today.add(Calendar.MONTH, added_month_count);

        final Calendar calendar = java.util.Calendar.getInstance();
        calendar.add(Calendar.MONTH, added_month_count);

        int diff = calendar.get(java.util.Calendar.DAY_OF_WEEK) - 2;
        if (diff == -1) diff = 6;
        calendar.setTimeInMillis(calendar.getTimeInMillis() - 1000 * 60 * 60 * 24 * diff);
        // month is the same
        while (dynamic_today.get(java.util.Calendar.MONTH) + 1 == calendar.get(java.util.Calendar.MONTH) + 1) {
            calendar.add(java.util.Calendar.DAY_OF_MONTH, -7);
        }

        int temp = 0;
        int temp2 = 0;

        for (int i = 1; i < 7; i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < 7; j++) {
                LinearLayout l = (LinearLayout) row.getChildAt(j);
                TextView text = (TextView) l.getChildAt(0);
                text.setTag(calendar.getTimeInMillis());
                text.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                textViews.add(text);
                text.setAlpha(1f);
                text.setEnabled(true);
                calendarTimes.add(calendar.getTimeInMillis());

                if (calendar.get(Calendar.MONTH) != today.get(Calendar.MONTH)) {
                    text.setTextColor(Utilities.getColor(R.color.black54));
                }

                text.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (view.getAlpha() == 0.99f) {
                            ((TextView) view).setTextColor(Utilities.getColor(R.color.black54));
                            view.setBackgroundColor(Utilities.getColor(android.R.color.transparent));
                            view.setAlpha(1);
                            selectedDays.remove(calendarTimes.get(textViews.indexOf(view)));
                            convertToDate("textc", calendarTimes.get(textViews.indexOf(view)));
                        } else {
                            ((TextView) view).setTextColor(Utilities.getColor(R.color.white));
                            view.setBackgroundColor(Utilities.getColor(R.color.button_red));
                            view.setAlpha(0.99f);
                            selectedDays.add(calendarTimes.get(textViews.indexOf(view)));
                            convertToDate("textc", calendarTimes.get(textViews.indexOf(view)));
                        }
                    }
                });

                for (int ll = temp; ll < bookedDays.size(); ll++) {
                    if (checkTheSameDayLong(bookedDays.get(ll), calendar.getTimeInMillis())) {
                        temp = ll;
                        text.setBackgroundResource(R.color.light_weird_green);
                        text.setTextColor(Utilities.getColor(R.color.black54));
                        text.setOnClickListener(null);
                        text.setEnabled(false);
                        break;
                    }
                }

                for (int kk = temp2; kk < selectedDays.size(); kk++) {
                    if (checkTheSameDayLong(selectedDays.get(kk), calendar.getTimeInMillis())) {
                        temp2 = kk;

                        text.setBackgroundResource(R.color.button_red);
                        text.setTextColor(Utilities.getColor(R.color.white));
                        text.setAlpha(0.99f);
                        break;
                    }
                }

                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        TextView sixth = (TextView) findViewById(R.id.sixths);
        if (Integer.valueOf(sixth.getText().toString()) < 15) {
            TableRow row = (TableRow) findViewById(R.id.sixth_week);
            row.setVisibility(View.GONE);
        } else {
            TableRow row = (TableRow) findViewById(R.id.sixth_week);
            row.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkTheSameDay(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean checkTheSameDayLong(long k1, long k2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(k1);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(k2);

        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void cleanCalendar() {
        for (int i = 1; i < 7; i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < 7; j++) {
                LinearLayout l = (LinearLayout) row.getChildAt(j);
                final TextView text = (TextView) l.getChildAt(0);
                text.setBackgroundResource(android.R.color.transparent);
                text.setTextColor(Utilities.getColor(R.color.black54));
            }
        }
    }
}
