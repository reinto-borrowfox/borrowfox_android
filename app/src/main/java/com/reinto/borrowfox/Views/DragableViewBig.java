/*
 *
 * DragableViewBig.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 28.6.16 15:51
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.reinto.borrowfox.R;

public class DragableViewBig extends FrameLayout {

    public ImageView background;
    public ImageButton delete;
    public String address;

    public DragableViewBig(Context context) {
        super(context);
        setupUI();
    }

    public DragableViewBig(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public DragableViewBig(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_just_imageview2, this);

        background = (ImageView) findViewById(R.id.source);
        delete = (ImageButton) findViewById(R.id.delete);
    }
}

