/*
 *
 * CustomSearchView.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 8.7.16 15:55
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.fragments.general.SearchResultFragment;
import com.reinto.borrowfox.managers.FragManager;

public class CustomSearchView extends LinearLayout {

    private OnFilterClick onFilterPress;
    private OnSearchClick onSearchClick;
    private EditText text;
    private TextView title;

    public CustomSearchView(Context context) {
        super(context);
        setupUI();
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public CustomSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_custom_searchview, this);

        final ImageButton search = (ImageButton) findViewById(R.id.search);
        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.title).setVisibility(GONE);
                TextView cross = (TextView) findViewById(R.id.cross);
                cross.setVisibility(View.VISIBLE);
                EditText editt = (EditText) findViewById(R.id.editt);
                editt.setVisibility(View.VISIBLE);
                editt.requestFocus();

                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        });

        Log.d(App.TAG, "CustomSearchView, setupUI: ");

        title = (TextView) findViewById(R.id.title);

        text = (EditText) findViewById(R.id.editt);
        //text.addTextChangedListener(new SearchListener());

        final ImageButton filterr = (ImageButton) findViewById(R.id.filterr);
        filterr.setOnClickListener(new FilterListener());

        final TextView cross = (TextView) findViewById(R.id.cross);
        cross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText("");
                text.setVisibility(GONE);
                cross.setVisibility(GONE);
                findViewById(R.id.title).setVisibility(VISIBLE);
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        });

        text.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);


        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String enteredText = editable.toString();
                if (enteredText.length() == 0) {
                    return;
                }

                if (enteredText.charAt((enteredText.length() - 1)) == '\n') {
                    if (text.getText().toString().length() == 0) {
                        Toast.makeText(getContext(), R.string.enter_word_to_search, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    FragManager.get().setFragment(SearchResultFragment.newInstance(text.getText().toString(), 0, 0, "", "", SearchResultFragment.ITEM_TYPE));
                    text.setText("");
                }
            }
        });

        //        text.setOnKeyListener(new OnKeyListener() {
        //            @Override
        //            public boolean onKey(View view, int keyCode, KeyEvent event) {
        //                Log.d(App.TAG, "CustomSearchView, onKey: ");
        //                // If the event is a key-down event on the "enter" button
        //                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
        //                    Log.d(App.TAG, "CustomSearchView, onKey: ");
        //                    if (text.getText().toString().length() == 0) {
        //                        Toast.makeText(getContext(), R.string.enter_word_to_search, Toast.LENGTH_SHORT).show();
        //                        return true;
        //                    }
        //
        //                    FragManager.get().setFragment(SearchResultFragment.newInstance(text.getText().toString(), 0, 0, "", ""));
        //
        //                    return true;
        //                }
        //                return false;
        //            }
        //        });
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void hideIcons() {
        findViewById(R.id.cross).setVisibility(GONE);
        findViewById(R.id.editt).setVisibility(GONE);
        findViewById(R.id.search).setVisibility(INVISIBLE);
        findViewById(R.id.filterr).setVisibility(INVISIBLE);
        findViewById(R.id.title).setVisibility(VISIBLE);
        text.setText("");
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }

    public void showIcons() {
        findViewById(R.id.search).setVisibility(VISIBLE);
        findViewById(R.id.filterr).setVisibility(VISIBLE);
    }

    public void setOnFilterIconClick(OnFilterClick dialogResult) {
        onFilterPress = dialogResult;
    }

    public void setOnSearchIconClick(OnSearchClick onSearchIconClick) {
        onSearchClick = onSearchIconClick;
    }

    public interface OnFilterClick {
        void onFilterClick();
    }

    public interface OnSearchClick {
        void onSeachClick(String text);
    }

    private class FilterListener implements android.view.View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (onFilterPress != null) {
                onFilterPress.onFilterClick();
            }
        }
    }

    ////////////////////////////////////////////////////////
    private class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            onSearchClick.onSeachClick(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}
