/*
 *
 * TopBarCustomView.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 4.7.16 9:11
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.fragments.request_item.RequestItemStepOneFragment;
import com.reinto.borrowfox.fragments.request_item.RequestItemStepThreeFragment;
import com.reinto.borrowfox.fragments.request_item.RequestItemStepTwoFragment;
import com.reinto.borrowfox.managers.FragManager;

public class TopBarCustomView extends LinearLayout {

    private int isActive = 1;
    private boolean _second = false, _third = false;
    private Button seconds;
    private Button third;

    public TopBarCustomView(Context context) {
        super(context);
        setupUI();
    }

    public TopBarCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public TopBarCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_request_item_top_bar, this);

        Button first = (Button) findViewById(R.id.first);
        first.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(App.TAG, "TopBarCustomView, onClick: 1");
                if (isActive != 1) {
                    FragManager.get().setFragmentControl(RequestItemStepOneFragment.class);
                    isActive = 1;
                }
            }
        });

        seconds = (Button) findViewById(R.id.second);
        seconds.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(App.TAG, "TopBarCustomView, onClick: 2");
                if (isActive != 2) {
                    _second = true;
                    seconds.setBackgroundResource(R.drawable.top_button_dark);
                    FragManager.get().setFragmentControl(RequestItemStepTwoFragment.class);
                    isActive = 2;
                }
            }
        });

        third = (Button) findViewById(R.id.third);
        third.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(App.TAG, "TopBarCustomView, onClick: 3");
                if (isActive != 3 && _second) {
                    _third = true;
                    third.setBackgroundResource(R.drawable.top_button_dark);
                    FragManager.get().setFragmentControl(RequestItemStepThreeFragment.class);
                    isActive = 3;
                }
            }
        });
    }

    public void setupStepTwo() {
        seconds.setBackgroundResource(R.drawable.top_button_dark);
        isActive = 2;
        _second = true;
    }

    public void setupStepThree() {
        seconds.setBackgroundResource(R.drawable.top_button_dark);
        third.setBackgroundResource(R.drawable.top_button_dark);
        isActive = 3;
        _second = true;
    }
}
