/*
 *
 * CustomDiscountSeekBar.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 10:38
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

public class MinimalPeriodSeekBar extends RelativeLayout {

    private static final int MAX_DISTANCE = 6;
    private ImageView selector;
    private int rounded_value = 3;

    private TextView percent2;

    public MinimalPeriodSeekBar(Context context) {
        super(context);
        setupUI();
    }

    public MinimalPeriodSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public MinimalPeriodSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_minimal_renta_seekbar, this);

        final TextView zero_per = (TextView) findViewById(R.id.percent1);
        percent2 = (TextView) findViewById(R.id.percent2);
        percent2.setText("3 days");
        selector = (ImageView) findViewById(R.id.view_selected);
        final int[] _xDelta = new int[2];

        ImageView slider2 = (ImageView) findViewById(R.id.slider2);
        slider2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        LayoutParams lParams = (LayoutParams) view.getLayoutParams();
                        _xDelta[1] = X + lParams.rightMargin;
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                        if (-X + _xDelta[1] < Utilities.convertDpToPixels(0, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(0, getContext());
                        } else if (-X + _xDelta[1] > Utilities.convertDpToPixels(370, getContext())) {
                            layoutParams.rightMargin = Utilities.convertDpToPixels(370, getContext());
                        } else {
                            layoutParams.rightMargin = -X + _xDelta[1];
                        }

                        if (-X + _xDelta[1] > Utilities.convertDpToPixels(320, getContext())) {//345
                            zero_per.setVisibility(INVISIBLE);
                        } else {
                            zero_per.setVisibility(VISIBLE);
                        }

                        int q = Utilities.convertDpToPixels(370, getContext());
                        int v = Utilities.convertDpToPixels(0, getContext());

                        float xx = MAX_DISTANCE - ((layoutParams.rightMargin - v) / (float) (q - v)) * MAX_DISTANCE;

                        rounded_value = Math.round(xx);

                        percent2.setText(getResources().getQuantityString(R.plurals.day, rounded_value + 1, rounded_value + 1));

                        view.setLayoutParams(layoutParams);
                        updateSelectorRight(layoutParams.rightMargin - v);
                        break;

                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                invalidate();
                return true;
            }
        });
    }

    public void setValue(int x) {
        rounded_value = x;
        percent2.setText(getResources().getQuantityString(R.plurals.day, rounded_value, rounded_value));
    }

    public int getMinRentalPeriod() {
        if (percent2.getText().toString().contains("days")) {
            return Integer.valueOf(percent2.getText().toString().replace(" days", ""));
        } else {
            return Integer.valueOf(percent2.getText().toString().replace(" day", ""));
        }
    }

    public void updateSelectorRight(int mar) {
        LayoutParams params = (LayoutParams) selector.getLayoutParams();
        params.rightMargin = mar;
        selector.setLayoutParams(params);
    }
}
