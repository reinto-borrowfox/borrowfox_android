package com.reinto.borrowfox.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.fragments.general.SearchResultFragment;
import com.reinto.borrowfox.managers.FragManager;

public class Filter extends LinearLayout {

    private ImageButton hide;
    private CustomDatePicker customDatePicker;
    private CustomSeekBar customSeekBar;

    public Filter(Context context) {
        super(context);
        setupUI();
    }

    public Filter(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupUI();
    }

    public Filter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupUI();
    }

    private void setupUI() {
        inflate(getContext(), R.layout.layout_filter, this);

        hide = (ImageButton) findViewById(R.id.hide);
        hide.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFilter();
            }
        });

        customDatePicker = (CustomDatePicker) findViewById(R.id.customDatePicker);
        customSeekBar = (CustomSeekBar) findViewById(R.id.customSeekbar);


        Button filterItems = (Button) findViewById(R.id.filter_filter_items);
        filterItems.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getFilteredItems(SearchResultFragment.ITEM_TYPE);
            }
        });

        Button filterMap = (Button) findViewById(R.id.filter_map);
        filterMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getFilteredItems(SearchResultFragment.MAP_TYPE);
            }
        });
    }

    @Override
    public void setVisibility(int visibility) {
        findViewById(R.id.filter_parent).setVisibility(visibility);
    }

    public void showFilter() {
        ((MainActivity) getContext()).filterIsVisible = true;
        setVisibility(View.VISIBLE);
        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.filter_slide_in);
        bottomUp.setInterpolator(new DecelerateInterpolator());
        startAnimation(bottomUp);
    }

    public void hideFilter() {
        ((MainActivity) getContext()).filterIsVisible = false;
        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.filter_slide_out);
        bottomUp.setInterpolator(new DecelerateInterpolator());
        startAnimation(bottomUp);
        bottomUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    public void getFilteredItems(int type) {
        if (customDatePicker.getFromDate().equals("") || customDatePicker.getToDate().equals("")) {
            Toast.makeText(getContext(), "Please set FROM and TO dates", Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d(App.TAG, "Filter, getFilteredItems: " + customDatePicker.getFromDate());
        Log.d(App.TAG, "Filter, getFilteredItems: " + customDatePicker.getToDate());

        hideFilter();
        FragManager.get().setFragment(SearchResultFragment.newInstance(" ", 0, customSeekBar.getMaxPrice(), customDatePicker.getFromDate(), customDatePicker.getToDate(), type));
    }
}
