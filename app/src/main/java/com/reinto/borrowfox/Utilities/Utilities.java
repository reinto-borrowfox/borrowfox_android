package com.reinto.borrowfox.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.Address;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utilities {

    public static final String HOME_ADDRESS_IS = "home_address_is";
    public static final String WORD_ADDRESS_ID = "word_address_id";
    private static final String NOTIFICATIONS_MESSAGES = "notif_messages";
    private static final String NOTIFICATIONS_ACTIVITY = "notif_activity";
    private static final String NOTIFICATIONS_ACCOUNT = "notif_account";
    private static final String USER_ID = "user_id";
    private static final String LOG_FB = "log_fb";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_FB_URL = "user_fb_url";
    private static final String USER_NAME = "user_name";
    private static final String USER_ADDRESS = "user_address";

    public static int convertDpToPixels(int dp, Context context) {
        Resources r = context.getResources();
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static int convertSpToPixels(int sp, Context context) {
        Resources r = context.getResources();
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, r.getDisplayMetrics()));
    }

    public static void saveUserId(Context context, int id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }

    public static void loginViaFb(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOG_FB, true);
        editor.apply();
    }

    public static boolean isUserLoggedInFb(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(LOG_FB, false);
    }

    public static void logoutViaFb(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOG_FB, false);
        editor.apply();
    }

    public static int getUserId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(USER_ID, 0);
    }

    public static void saveUserEmail(Context context, String email) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_EMAIL, email);
        editor.apply();
    }

    //////              FB IMAGE

    public static void saveFbImage(Context context, String url) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_FB_URL, url);
        editor.apply();
    }

    public static String getFbImage(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(USER_FB_URL, "");
    }

    //////

    public static String getUserEmail(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public static void saveUserName(Context context, String email) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_NAME, email);
        editor.apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(USER_NAME, "");
    }

    public static void saveUserLocation(Context context, String address) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ADDRESS, address);
        editor.apply();
    }

    public static void saveHomeAddressId(Context context, int id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(HOME_ADDRESS_IS, id);
        editor.apply();
    }

    public static int getHomeAddressId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(HOME_ADDRESS_IS, 0);
    }

    public static void saveWorkAddressId(Context context, int id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(WORD_ADDRESS_ID, id);
        editor.apply();
    }

    public static int getWorkAddressId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(WORD_ADDRESS_ID, 0);
    }

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(USER_ID, 0) != 0;
    }

    public static String getUserLocation(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences.getString(USER_ADDRESS, "").equals("")) {
            return "Unknown location";
        }
        return sharedPreferences.getString(USER_ADDRESS, "Olomoucká 166, Brno");
    }

    //    SETTINGS
    public static boolean getNotifMessages(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(NOTIFICATIONS_MESSAGES, false);
    }

    public static boolean getNotifActivity(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(NOTIFICATIONS_ACTIVITY, false);
    }

    public static boolean getNotifAccount(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(NOTIFICATIONS_ACCOUNT, false);
    }

    public static void setNotifMessages(Context context, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NOTIFICATIONS_MESSAGES, value);
        editor.apply();
    }

    public static void setNotifActivity(Context context, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NOTIFICATIONS_ACTIVITY, value);
        editor.apply();
    }

    public static void setNotifAccount(Context context, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NOTIFICATIONS_ACCOUNT, value);
        editor.apply();
    }

    public static String addZero(int x) {
        if (x < 10) {
            return "0" + String.valueOf(x);
        } else {
            return String.valueOf(x);
        }
    }

    public static Spannable getFormatedPrice(String text) {
        String s1 = "£" + text + " / DAY";
        final SpannableStringBuilder span = new SpannableStringBuilder(s1);
        span.setSpan(new RelativeSizeSpan(0.6f), s1.length() - 6, s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(Typeface.BOLD), 0, s1.length() - 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    public static Spannable getFormatedPrice(int value, String afterText) {
        String s1 = "£" + value + afterText;
        final SpannableStringBuilder span = new SpannableStringBuilder(s1);
        span.setSpan(new RelativeSizeSpan(0.6f), s1.length() - afterText.length(), s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(Typeface.BOLD), 0, String.valueOf(value).length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    public static String getFormatedDateFromLong(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        String year = String.valueOf(calendar.get(Calendar.YEAR));

        return fixDay(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + fixDay(calendar.get(Calendar.MONTH) + 1) + "/" +
                year.substring(year.length() - 2, year.length());
    }

    public static String getFormatedDateFromLong2(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return fixDay(calendar.get(Calendar.YEAR)) + "-" + fixDay(calendar.get(Calendar.MONTH) + 1) + "-" + fixDay(calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static Spannable getFormatedColoredDateFromLong(String text, long time, int color) {
        String date = getFormatedDateFromLong(time);

        final SpannableStringBuilder span = new SpannableStringBuilder(text + date);
        span.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(App.getAppContext().getResources(),
                color, null)), text.length(), text.length() + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return span;
    }

    public static String fixDay(int x) {
        if (x < 10) {
            return "0" + String.valueOf(x);
        } else {
            return String.valueOf(x);
        }
    }

    public static void setToolbarTitle(Context c, String text) {
        ((MainActivity) c).customSearchView.setTitle(text.toUpperCase());
    }

    public static void hideToolbarIcons(Context c) {
        ((MainActivity) c).customSearchView.hideIcons();
    }

    public static void showToolbarIcons(Context c) {
        ((MainActivity) c).customSearchView.showIcons();
    }

    public static void dissapearView(final View view) {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(600);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animation);
    }

    public static void appearView(View view) {
        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        AlphaAnimation animation = new AlphaAnimation(0f, 1f);
        animation.setFillAfter(true);
        animation.setDuration(600);
        view.startAnimation(animation);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        Log.d(App.TAG, "MyProfileFragment, calculateInSampleSize: " + inSampleSize);

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String address,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        BitmapFactory.decodeFile(address, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(address, options);
    }

    public static String bitMapToString(Bitmap bitmap) {
        Log.d(App.TAG, "MyProfileFragment, BitMapToString: " + bitmap.getWidth() + ", " + bitmap.getHeight());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static void updateProfileImage(final Context context) {
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.getProfileImage(new TaskDoneListener<String>() {
            @Override
            public void OnTaskDone(String object) {
                Log.d(App.TAG, "MyProfileFragment, OnTaskDone: " + object);
                if (!object.equals("")) {
                    Utilities.saveFbImage(context, object);
                    ((MainActivity) context).setupNavigationHeader();
                }
            }
        });
    }

    public static void updateHomeAddress(final Context context) {
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadHomeAddress(new TaskDoneListener<Address>() {
            @Override
            public void OnTaskDone(Address object) {
                if (object != null) {
                    Log.d(App.TAG, "Utilities, OnTaskDone: " + object.toString());
                    saveUserLocation(context, object.getStreet() + ", " + object.getCity());
                    ((MainActivity) context).setupNavigationHeader();
                } else {
                    Log.d(App.TAG, "Utilities, OnTaskDone: downloading home address not successful");
                }
            }
        });
    }

    public static long getLongFromString(String text) {
        Date d;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            d = sdf.parse(text);

            return d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return System.currentTimeMillis();
    }

    public static void printDebugTxtFile(String text) {
        FileOutputStream fos = null;

        try {
            File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
            boolean isPresent = true;
            if (!docsFolder.exists()) {
                isPresent = docsFolder.mkdir();
            }
            if (isPresent) {
                File file = new File(docsFolder.getAbsolutePath(), "test.txt");
                fos = new FileOutputStream(file);
                fos.write(text.getBytes());
            } else {
                // Failure
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getLatestDate(long time1, long time2, long time3, long time4) {
        List<Long> list = new ArrayList<>();
        list.add(time1);
        list.add(time2);
        list.add(time3);
        list.add(time4);

        long max = Collections.max(list);

        String x = getFormatedDateFromLong(max);

        if (x.equals("01/01/70")) {
            return "0";
        } else {
            return x;
        }
    }

    public static int getColor(int color) {
        return ResourcesCompat.getColor(App.getInstance().getApplicationContext().getResources(), color, null);
    }

    public static String formatDoublePrice(double price) {
        if (price == (long) price)
            return String.format("%d", (long) price);
        else
            return String.format("%.2f", price).replace(",", "."); //replace because of parsing double from string
    }
}