package com.reinto.borrowfox.Models;

import android.util.Log;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;

import java.util.ArrayList;

public class LendData {

    int id;
    String name;
    String description;
    int price_per_day;
    int price_per_extra_day;
    int deposit;
    int minimum_rental_period;
    int delivery;
    int item_image_id;
    long loanEventBegin, loanEventEnd, requestedEventBegin, requestedEventEnd,
            returnedEventBegin, returnedEventEnd, acceptedEventBegin, acceptedEventEnd;
    int requestedEventId;
    int status;
    String imageUrl = "";

    OnMyDialogResult onMyDialogResult;

    public LendData() {
    }

    public LendData(int id, String name, String description, int price_per_day, int price_per_extra_day,
                    int deposit, int minimum_rental_period, int delivery, int item_image_id,
                    int requestedEventId,
                    long loanEventBegin, long loanEventEnd, long requestedEventBegin,
                    long requestedEventEnd, long returnedEventBegin, long returnedEventEnd,
                    long acceptedEventBegin, long acceptedEventEnd, int status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price_per_day = price_per_day;
        this.price_per_extra_day = price_per_extra_day;
        this.deposit = deposit;
        this.minimum_rental_period = minimum_rental_period;
        this.delivery = delivery;
        this.item_image_id = item_image_id;
        this.requestedEventId = requestedEventId;
        this.loanEventBegin = loanEventBegin;
        this.loanEventEnd = loanEventEnd;
        this.requestedEventBegin = requestedEventBegin;
        this.requestedEventEnd = requestedEventEnd;
        this.returnedEventBegin = returnedEventBegin;
        this.returnedEventEnd = returnedEventEnd;
        this.acceptedEventBegin = acceptedEventBegin;
        this.acceptedEventEnd = acceptedEventEnd;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice_per_day() {
        return price_per_day;
    }

    public void setPrice_per_day(int price_per_day) {
        this.price_per_day = price_per_day;
    }

    public int getPrice_per_extra_day() {
        return price_per_extra_day;
    }

    public void setPrice_per_extra_day(int price_per_extra_day) {
        this.price_per_extra_day = price_per_extra_day;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getMinimum_rental_period() {
        return minimum_rental_period;
    }

    public void setMinimum_rental_period(int minimum_rental_period) {
        this.minimum_rental_period = minimum_rental_period;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getItem_image_id() {
        return item_image_id;
    }

    public void setItem_image_id(int item_image_id) {
        this.item_image_id = item_image_id;
    }

    public long getLoanEventBegin() {
        return loanEventBegin;
    }

    public void setLoanEventBegin(long loanEventBegin) {
        this.loanEventBegin = loanEventBegin;
    }

    public long getLoanEventEnd() {
        return loanEventEnd;
    }

    public void setLoanEventEnd(long loanEventEnd) {
        this.loanEventEnd = loanEventEnd;
    }

    public long getRequestedEventBegin() {
        return requestedEventBegin;
    }

    public void setRequestedEventBegin(long requestedEventBegin) {
        this.requestedEventBegin = requestedEventBegin;
    }

    public long getRequestedEventEnd() {
        return requestedEventEnd;
    }

    public void setRequestedEventEnd(long requestedEventEnd) {
        this.requestedEventEnd = requestedEventEnd;
    }

    public long getReturnedEventBegin() {
        return returnedEventBegin;
    }

    public void setReturnedEventBegin(long returnedEventBegin) {
        this.returnedEventBegin = returnedEventBegin;
    }

    public long getReturnedEventEnd() {
        return returnedEventEnd;
    }

    public void setReturnedEventEnd(long returnedEventEnd) {
        this.returnedEventEnd = returnedEventEnd;
    }

    public long getAcceptedEventBegin() {
        return acceptedEventBegin;
    }

    public void setAcceptedEventBegin(long acceptedEventBegin) {
        this.acceptedEventBegin = acceptedEventBegin;
    }

    public long getAcceptedEventEnd() {
        return acceptedEventEnd;
    }

    public void setAcceptedEventEnd(long acceptedEventEnd) {
        this.acceptedEventEnd = acceptedEventEnd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "LendData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price_per_day=" + price_per_day +
                ", price_per_extra_day=" + price_per_extra_day +
                ", deposit=" + deposit +
                ", minimum_rental_period=" + minimum_rental_period +
                ", delivery=" + delivery +
                ", item_image_id=" + item_image_id +
                ", loanEventBegin=" + loanEventBegin +
                ", loanEventEnd=" + loanEventEnd +
                ", requestedEventBegin=" + requestedEventBegin +
                ", requestedEventEnd=" + requestedEventEnd +
                ", returnedEventBegin=" + returnedEventBegin +
                ", returnedEventEnd=" + returnedEventEnd +
                ", acceptedEventBegin=" + acceptedEventBegin +
                ", acceptedEventEnd=" + acceptedEventEnd +
                ", status=" + status +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    public void downloadImages(final OnMyDialogResult onMyDialogResult) {
        this.onMyDialogResult = onMyDialogResult;
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadImagesForItems(id, new TaskDoneListener<ArrayList<String>>() {
            @Override
            public void OnTaskDone(ArrayList<String> object) {
                if (object == null) {
                    Log.d(App.TAG, "ItemData, OnTaskDone: error");
                    return;
                }

                if (object.size() == 0) {
                    imageUrl = "";
                    onMyDialogResult.finish();
                    return;
                }

                imageUrl = object.get(0);
                onMyDialogResult.finish();
            }
        });
    }

    public int getRequestedEventId() {
        return requestedEventId;
    }

    public void setRequestedEventId(int requestedEventId) {
        this.requestedEventId = requestedEventId;
    }

    public interface OnMyDialogResult {
        void finish();
    }
}
