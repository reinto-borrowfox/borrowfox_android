package com.reinto.borrowfox.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class CalendaDayInfo implements Parcelable {

    private Calendar c;
    private int type;

    public CalendaDayInfo(Calendar c, int type) {
        this.c = c;
        this.type = type;
    }

    public Calendar getC() {
        return c;
    }

    public void setC(Calendar c) {
        this.c = c;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "CalendaDayInfo{" +
                "c=" + printCalendar(c) +
                ", type=" + type +
                '}';
    }

    private String printCalendar(Calendar c) {
        return c.get(Calendar.DAY_OF_MONTH) + ", " + c.get(Calendar.MONTH);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.c);
        dest.writeInt(this.type);
    }

    protected CalendaDayInfo(Parcel in) {
        this.c = (Calendar) in.readSerializable();
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<CalendaDayInfo> CREATOR = new Parcelable.Creator<CalendaDayInfo>() {
        @Override
        public CalendaDayInfo createFromParcel(Parcel source) {
            return new CalendaDayInfo(source);
        }

        @Override
        public CalendaDayInfo[] newArray(int size) {
            return new CalendaDayInfo[size];
        }
    };
}
