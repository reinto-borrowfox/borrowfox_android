package com.reinto.borrowfox.Models;

public class ChatMessage {

    private int type;
    private long time;
    private String message;
    private String time_string;
    private int chat_id;


    public ChatMessage(int type, long time, String message, String time_string, int chat_id) {
        this.type = type;
        this.time = time;
        this.message = message;
        this.time_string = time_string;
        this.chat_id = chat_id;
    }

    public ChatMessage() {
    }

    public String getTime_string() {
        return time_string;
    }

    public void setTime_string(String time_string) {
        this.time_string = time_string;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "type=" + type +
                ", time=" + time +
                ", message='" + message + '\'' +
                ", time_string='" + time_string + '\'' +
                '}';
    }
}
