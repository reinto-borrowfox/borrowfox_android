/*
 *
 * NewItemInfo.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 30.6.16 8:46
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

import java.util.ArrayList;
import java.util.List;

public class NewItemInfo {

    private String item_name = "", description = "";
    private List<String> addresses;
    private int category = 1;
    private int price_per_day;
    private int additional_day_discount;
    private int deposit;
    private int minimim_day_rental;
    private int item_collection_and_return;
    private List<Long> blocked_days;
    private int percentStepTwo = 55;

    public NewItemInfo() {
        addresses = new ArrayList<>();
        blocked_days = new ArrayList<>();
    }

    public NewItemInfo(String item_name, String description, List<String> addresses,
                       int category, int price_per_day, int additional_day_discount,
                       int deposit, int minimim_day_rental, int item_collection_and_return,
                       List<Long> blocked_days) {
        this.item_name = item_name;
        this.description = description;
        this.addresses = addresses;
        this.category = category;
        this.price_per_day = price_per_day;
        this.additional_day_discount = additional_day_discount;
        this.deposit = deposit;
        this.minimim_day_rental = minimim_day_rental;
        this.item_collection_and_return = item_collection_and_return;
        this.blocked_days = blocked_days;
    }

    @Override
    public String toString() {
        return "NewItemInfo{" +
                "blocked_days=" + blocked_days +
                ", \nitem_collection_and_return=" + item_collection_and_return +
                ", \nminimim_day_rental=" + minimim_day_rental +
                ", \ndeposit=" + deposit +
                ", \nadditional_day_discount=" + additional_day_discount +
                ", \nprice_per_day=" + price_per_day +
                ", \ncategory=" + category +
                ", \naddresses=" + addresses +
                ", \ndescription='" + description + '\'' +
                ", \nitem_name='" + item_name + '\'' +
                '}';
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getPrice_per_day() {
        return price_per_day;
    }

    public void setPrice_per_day(int price_per_day) {
        this.price_per_day = price_per_day;
    }

    public int getAdditional_day_discount() {
        return additional_day_discount;
    }

    public void setAdditional_day_discount(int additional_day_discount) {
        this.additional_day_discount = additional_day_discount;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getMinimim_day_rental() {
        return minimim_day_rental;
    }

    public void setMinimim_day_rental(int minimim_day_rental) {
        this.minimim_day_rental = minimim_day_rental;
    }

    public int getItem_collection_and_return() {
        return item_collection_and_return;
    }

    public void setItem_collection_and_return(int item_collection_and_return) {
        this.item_collection_and_return = item_collection_and_return;
    }

    public List<Long> getBlocked_days() {
        return blocked_days;
    }

    public void setBlocked_days(List<Long> blocked_days) {
        this.blocked_days = blocked_days;
    }

    public int getPercentStepTwo() {
        return percentStepTwo;
    }

    public void setPercentStepTwo(int percentStepTwo) {
        this.percentStepTwo = percentStepTwo;
    }
}
