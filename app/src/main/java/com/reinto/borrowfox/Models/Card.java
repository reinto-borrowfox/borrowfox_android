/*
 *
 * Card.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 9.8.16 12:06
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

public class Card {
    public int last4;
    public String manuf;
    private int id;
    private String user_name;

    public Card(int id, int last4, String manuf, String user_name) {
        this.last4 = last4;
        this.manuf = manuf;
        this.id = id;
        this.user_name = user_name;
    }

    public String getInfo() {
        return manuf + " **** **** **** " + last4;
    }

    public int getLast4() {
        return last4;
    }

    public void setLast4(int last4) {
        this.last4 = last4;
    }

    public String getManuf() {
        return manuf;
    }

    public void setManuf(String manuf) {
        this.manuf = manuf;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
