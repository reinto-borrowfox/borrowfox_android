package com.reinto.borrowfox.Models;

import com.reinto.borrowfox.Utilities.Utilities;

public class CalendarBlockedDay {

    private long date;
    private int type;

    public CalendarBlockedDay(long date, int type) {
        this.date = date;
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CalendarBlockedDay{" +
                "date=" + Utilities.getFormatedDateFromLong(date) +
                ", type=" + type +
                '}';
    }
}
