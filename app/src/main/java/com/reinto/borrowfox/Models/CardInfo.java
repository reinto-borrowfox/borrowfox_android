/*
 *
 * CardInfo.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 12.7.16 14:53
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

public class CardInfo {

    private String card_number;
    private String user_name;
    private int month, year;

    public CardInfo(String card_number, String user_name, int month, int year) {
        this.card_number = card_number;
        this.user_name = user_name;
        this.month = month;
        this.year = year;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "CardInfo{" +
                "card_number='" + card_number + '\'' +
                ", user_name='" + user_name + '\'' +
                '}';
    }
}
