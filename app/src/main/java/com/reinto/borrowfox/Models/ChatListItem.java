package com.reinto.borrowfox.Models;

public class ChatListItem {

    private String name, address, time, image_url, full_name;
    private int chat_id, lender_id, item_id;

    public ChatListItem(String name, String address, String time, String image_url, int id, String full_name, int item_id, int lender_id) {
        this.name = name;
        this.address = address;
        this.time = time;
        this.image_url = image_url;
        this.chat_id = id;
        this.full_name = full_name;
        this.item_id = item_id;
        this.lender_id = lender_id;
    }

    public ChatListItem() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getId() {
        return chat_id;
    }

    public void setId(int id) {
        this.chat_id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getLender_id() {
        return lender_id;
    }

    public void setLender_id(int lender_id) {
        this.lender_id = lender_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }
}

