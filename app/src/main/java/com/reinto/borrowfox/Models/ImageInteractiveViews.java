/*
 *
 * ImageInteractiveViews.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 27.6.16 16:52
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

import android.widget.ImageView;

public class ImageInteractiveViews {

    private ImageView source, delete;

    public ImageInteractiveViews(ImageView source, ImageView delete) {
        this.source = source;
        this.delete = delete;
    }

    public ImageView getSource() {
        return source;
    }

    public void setSource(ImageView source) {
        this.source = source;
    }

    public ImageView getDelete() {
        return delete;
    }

    public void setDelete(ImageView delete) {
        this.delete = delete;
    }
}
