/*
 *
 * ItemData.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 21.7.16 9:10
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;

import java.util.ArrayList;

public class ItemData implements Parcelable {

    public static final Parcelable.Creator<ItemData> CREATOR = new Parcelable.Creator<ItemData>() {
        @Override
        public ItemData createFromParcel(Parcel source) {
            return new ItemData(source);
        }

        @Override
        public ItemData[] newArray(int size) {
            return new ItemData[size];
        }
    };


    OnMyDialogResult mDialogResult;
    String name;
    String description;
    int price_per_day;
    int price_per_extra_day;
    int deposit;
    int minimum_rental_period;
    int delivery;
    int item_image_id;
    String user_locality;
    private int id;
    private int lend_user_id;
    private ArrayList<String> images;
    private int rating = 0;
    String user_filename;

    public ItemData(String name, String description, int price_per_day, int price_per_extra_day,
                    int deposit, int minimum_rental_period, int delivery, int item_image_id, String
                            user_locality, int id, int lend_user_id, ArrayList<String> images, int rating,String user_filename) {
        this.name = name;
        this.description = description;
        this.price_per_day = price_per_day;
        this.price_per_extra_day = price_per_extra_day;
        this.deposit = deposit;
        this.minimum_rental_period = minimum_rental_period;
        this.delivery = delivery;
        this.item_image_id = item_image_id;
        this.user_locality = user_locality;
        this.id = id;
        this.lend_user_id = lend_user_id;
        this.images = images;
        this.rating = rating;
        this.user_filename = user_filename;
    }

    public ItemData() {

    }

    protected ItemData(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.price_per_day = in.readInt();
        this.price_per_extra_day = in.readInt();
        this.deposit = in.readInt();
        this.minimum_rental_period = in.readInt();
        this.delivery = in.readInt();
        this.item_image_id = in.readInt();
        this.user_locality = in.readString();
        this.id = in.readInt();
        this.lend_user_id = in.readInt();
        this.images = in.createStringArrayList();
        this.rating = in.readInt();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice_per_day() {
        return price_per_day;
    }

    public void setPrice_per_day(int price_per_day) {
        this.price_per_day = price_per_day;
    }

    public int getPrice_per_extra_day() {
        return price_per_extra_day;
    }

    public void setPrice_per_extra_day(int price_per_extra_day) {
        this.price_per_extra_day = price_per_extra_day;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getMinimum_rental_period() {
        return minimum_rental_period;
    }

    public void setMinimum_rental_period(int minimum_rental_period) {
        this.minimum_rental_period = minimum_rental_period;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getItem_image_id() {
        return item_image_id;
    }

    public void setItem_image_id(int item_image_id) {
        this.item_image_id = item_image_id;
    }

    public String getUser_locality() {
        return user_locality;
    }

    public void setUser_locality(String user_locality) {
        this.user_locality = user_locality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLend_user_id() {
        return lend_user_id;
    }

    public void setLend_user_id(int lend_user_id) {
        this.lend_user_id = lend_user_id;
    }

    @Override
    public String toString() {
        return "ItemData{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price_per_day=" + price_per_day +
                ", price_per_extra_day=" + price_per_extra_day +
                ", deposit=" + deposit +
                ", minimum_rental_period=" + minimum_rental_period +
                ", delivery=" + delivery +
                ", item_image_id=" + item_image_id +
                ", user_locality='" + user_locality + '\'' +
                ", id=" + id +
                ", lend_user_id=" + lend_user_id +
                '}';
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.price_per_day);
        dest.writeInt(this.price_per_extra_day);
        dest.writeInt(this.deposit);
        dest.writeInt(this.minimum_rental_period);
        dest.writeInt(this.delivery);
        dest.writeInt(this.item_image_id);
        dest.writeString(this.user_locality);
        dest.writeInt(this.id);
        dest.writeInt(this.lend_user_id);
        dest.writeStringList(this.images);
        dest.writeInt(this.rating);
    }

    public void downloadImages(final OnMyDialogResult onMyDialogResult) {
        mDialogResult = onMyDialogResult;
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadImagesForItems(id, new TaskDoneListener<ArrayList<String>>() {
            @Override
            public void OnTaskDone(ArrayList<String> object) {
                if (object == null) {
                    Log.d(App.TAG, "ItemData, OnTaskDone: error");
                    return;
                }

                images = new ArrayList<>(object);
                onMyDialogResult.finish();
            }
        });
    }

    public interface OnMyDialogResult {
        void finish();
    }

    public String getUser_filename() {
        return user_filename;
    }

    public void setUser_filename(String user_filename) {
        this.user_filename = user_filename;
    }
}
