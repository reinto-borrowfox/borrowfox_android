/*
 *
 * FullItemData.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 8.8.16 12:08
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

import java.util.ArrayList;
import java.util.List;

public class FullItemData {

    private String name;
    private String description;
    private int price_per_day;
    private int price_per_extra_day;
    private int deposit;
    private int minimum_rental_period;
    private int delivery;
    private int item_image_id;
    private String user_locality;
    private int id;
    private int lend_user_id;
    private ArrayList<String> images;
    private int rating = 0;
    private String user_url;
    private String user_full_name;
    private List<CalendarBlockedDay> blockedDays;
    private List<CalendarBlockedDay> unavailableDays;
    private int status;
    private int categoryId;

    public FullItemData() {
    }

    public FullItemData(String name, String description, int price_per_day, int price_per_extra_day,
                        int deposit, int minimum_rental_period, int delivery, int item_image_id,
                        String user_locality, int id, int lend_user_id, ArrayList<String> images,
                        int rating, String user_url, String user_full_name, List<CalendarBlockedDay> blockedDays, List<CalendarBlockedDay> unavailableDays, int status, int categoryId) {
        this.name = name;
        this.description = description;
        this.price_per_day = price_per_day;
        this.price_per_extra_day = price_per_extra_day;
        this.deposit = deposit;
        this.minimum_rental_period = minimum_rental_period;
        this.delivery = delivery;
        this.item_image_id = item_image_id;
        this.user_locality = user_locality;
        this.id = id;
        this.lend_user_id = lend_user_id;
        this.images = images;
        this.rating = rating;
        this.user_url = user_url;
        this.user_full_name = user_full_name;
        this.blockedDays = blockedDays;
        this.unavailableDays = unavailableDays;
        this.status = status;
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice_per_day() {
        return price_per_day;
    }

    public void setPrice_per_day(int price_per_day) {
        this.price_per_day = price_per_day;
    }

    public int getPrice_per_extra_day() {
        return price_per_extra_day;
    }

    public void setPrice_per_extra_day(int price_per_extra_day) {
        this.price_per_extra_day = price_per_extra_day;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getMinimum_rental_period() {
        return minimum_rental_period;
    }

    public void setMinimum_rental_period(int minimum_rental_period) {
        this.minimum_rental_period = minimum_rental_period;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getItem_image_id() {
        return item_image_id;
    }

    public void setItem_image_id(int item_image_id) {
        this.item_image_id = item_image_id;
    }

    public String getUser_locality() {
        return user_locality;
    }

    public void setUser_locality(String user_locality) {
        this.user_locality = user_locality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLend_user_id() {
        return lend_user_id;
    }

    public void setLend_user_id(int lend_user_id) {
        this.lend_user_id = lend_user_id;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getUser_url() {
        return user_url;
    }

    public void setUser_url(String user_url) {
        this.user_url = user_url;
    }

    public String getUser_full_name() {
        return user_full_name;
    }

    public void setUser_full_name(String user_full_name) {
        this.user_full_name = user_full_name;
    }

    public List<CalendarBlockedDay> getBlockedDays() {
        return blockedDays;
    }

    public void setBlockedDays(List<CalendarBlockedDay> blockedDays) {
        this.blockedDays = blockedDays;
    }

    public List<CalendarBlockedDay> getUnavailableDays() {
        return unavailableDays;
    }

    public void setUnavailableDays(List<CalendarBlockedDay> unavailableDays) {
        this.unavailableDays = unavailableDays;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FullItemData{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price_per_day=" + price_per_day +
                ", price_per_extra_day=" + price_per_extra_day +
                ", deposit=" + deposit +
                ", minimum_rental_period=" + minimum_rental_period +
                ", delivery=" + delivery +
                ", item_image_id=" + item_image_id +
                ", user_locality='" + user_locality + '\'' +
                ", id=" + id +
                ", lend_user_id=" + lend_user_id +
                ", images=" + images +
                ", rating=" + rating +
                ", user_url='" + user_url + '\'' +
                ", user_full_name='" + user_full_name + '\'' +
                ", blockedDays=" + blockedDays +
                ", unavailableDays=" + unavailableDays +
                ", status=" + status +
                ", categoryId=" + categoryId +
                '}';
    }
}
