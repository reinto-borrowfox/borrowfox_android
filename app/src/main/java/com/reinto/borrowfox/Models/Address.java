/*
 *
 * Address.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.7.16 11:54
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

public class Address {

    private int id;
    private int number;
    private String psc;
    private String street;
    private String city;

    public Address() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPsc() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc = psc;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
