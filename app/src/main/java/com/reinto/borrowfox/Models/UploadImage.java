/*
 *
 * UploadImage.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 11.8.16 9:25
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Models;

public class UploadImage {

    private int order;
    private String name;
    private String data;
    private int size = 1;

    public UploadImage(int order, String name, String data, int size) {
        this.order = order;
        this.name = name;
        this.data = data;
        this.size = size;
    }

    public UploadImage() {
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "UploadImage{" +
                "order=" + order +
                ", name='" + name + '\'' +
                ", data='" + data.substring(0,50) + '\'' +
                ", size=" + size +
                '}';
    }
}
