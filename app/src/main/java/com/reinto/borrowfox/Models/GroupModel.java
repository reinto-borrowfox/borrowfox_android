package com.reinto.borrowfox.Models;

import java.util.ArrayList;

/**
 * Created by Reinto Workstation on 20.11.2016.
 */

public class GroupModel {


    private String image_url;
    private int id;
    private String price, name, location;
    private int imageIndex = 0;
    private ArrayList<String> urls;
    private int type;

    public GroupModel(String image_url, int id, String price, String name, String location, int imageIndex, ArrayList<String> urls, int type) {
        this.image_url = image_url;
        this.id = id;
        this.price = price;
        this.name = name;
        this.location = location;
        this.imageIndex = imageIndex;
        this.urls = urls;
        this.type = type;
    }

    public GroupModel(String urls, String name, int id) {
        this.image_url = urls;
        this.id = id;
        this.name = name;
        type = 1;
    }

    public GroupModel() {
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
