package com.reinto.borrowfox.Models;

import java.util.ArrayList;

public class SingleCategoryItemModel {

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    private int item_id;
    private String price, name, location;
    private int imageIndex = 0;
    private ArrayList<String> urls;
    private String user_filename;

    public SingleCategoryItemModel(int item_id, String price, String name, String location, ArrayList<String> urls, String user_filename) {
        this.price = price;
        this.name = name;
        this.location = location;
        this.urls = urls;
        this.item_id = item_id;
        this.user_filename = user_filename;
    }

    @Override
    public String toString() {
        return "SingleCategoryItemModel{" +
                "price='" + price + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", imageIndex=" + imageIndex +
                ", urls=" + urls +
                '}';
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public String getUser_filename() {
        return user_filename;
    }

    public void setUser_filename(String user_filename) {
        this.user_filename = user_filename;
    }
}
