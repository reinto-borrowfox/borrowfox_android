package com.reinto.borrowfox.Models;

import android.util.Log;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;

import java.util.ArrayList;

public class BorrowData {

    int id;
    private int eventId;
    String name;
    String description;
    int price_per_day;
    int price_per_extra_day;
    int deposit;
    int minimum_rental_period;
    int delivery;
    int item_image_id;
    long eventBegin, eventEnd;
    int status;
    String userImageUrl;
    String imageUrl = "";
    private String userLocality;
    private int lenderId;
    private int eventBorrowPrice;

    OnMyDialogResult onMyDialogResult;

    public BorrowData(int id, int eventId, String name, String description, int price_per_day,
                      int price_per_extra_day, int deposit, int minimum_rental_period,
                      int delivery, int item_image_id, long eventBegin, long eventEnd,
                      int status, String userImageUrl, String imageUrl, String userLocality, int lenderId, int eventBorrowPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price_per_day = price_per_day;
        this.price_per_extra_day = price_per_extra_day;
        this.deposit = deposit;
        this.minimum_rental_period = minimum_rental_period;
        this.delivery = delivery;
        this.item_image_id = item_image_id;
        this.eventBegin = eventBegin;
        this.eventEnd = eventEnd;
        this.status = status;
        this.userImageUrl = userImageUrl;
        this.imageUrl = imageUrl;
        this.userLocality = userLocality;
        this.lenderId = lenderId;
        this.eventBorrowPrice = eventBorrowPrice;
        this.eventId = eventId;
    }

    public int getLenderId() {
        return lenderId;
    }

    public void setLenderId(int lenderId) {
        this.lenderId = lenderId;
    }

    public String getUserLocality() {
        return userLocality;
    }

    public void setUserLocality(String userLocality) {
        this.userLocality = userLocality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice_per_day() {
        return price_per_day;
    }

    public void setPrice_per_day(int price_per_day) {
        this.price_per_day = price_per_day;
    }

    public int getPrice_per_extra_day() {
        return price_per_extra_day;
    }

    public void setPrice_per_extra_day(int price_per_extra_day) {
        this.price_per_extra_day = price_per_extra_day;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getMinimum_rental_period() {
        return minimum_rental_period;
    }

    public void setMinimum_rental_period(int minimum_rental_period) {
        this.minimum_rental_period = minimum_rental_period;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getItem_image_id() {
        return item_image_id;
    }

    public void setItem_image_id(int item_image_id) {
        this.item_image_id = item_image_id;
    }

    public long getEventBegin() {
        return eventBegin;
    }

    public void setEventBegin(long eventBegin) {
        this.eventBegin = eventBegin;
    }

    public long getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(long eventEnd) {
        this.eventEnd = eventEnd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getEventBorrowPrice() {
        return eventBorrowPrice;
    }

    public void setEventBorrowPrice(int eventBorrowPrice) {
        this.eventBorrowPrice = eventBorrowPrice;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public void downloadImages(final OnMyDialogResult onMyDialogResult) {
        this.onMyDialogResult = onMyDialogResult;
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadImagesForItems(id, new TaskDoneListener<ArrayList<String>>() {
            @Override
            public void OnTaskDone(ArrayList<String> object) {
                if (object == null) {
                    Log.d(App.TAG, "ItemData, OnTaskDone: error");
                    return;
                }

                if (object.size() == 0) {
                    imageUrl = "";
                    onMyDialogResult.finish();
                    return;
                }

                imageUrl = object.get(0);
                onMyDialogResult.finish();
            }
        });
    }

    public interface OnMyDialogResult {
        void finish();
    }
}
