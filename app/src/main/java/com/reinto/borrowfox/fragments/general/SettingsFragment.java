package com.reinto.borrowfox.fragments.general;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.Card;
import com.reinto.borrowfox.Models.CardInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CardInfoView;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ConstantConditions")
public class SettingsFragment extends Fragment {

    private View rootView;
    private boolean notifActive = false, notifInvite = false, whyRentB = false, payment = false;
    private boolean cardDetailIsShown = false;
    private ArrayList<CardInfoView> cardss;
    private View cardInfoView, addNewCardView;
    private LinearLayout rootParent;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        resetVisibility();

        setupCards();
        setupNotifications();
        setupInvite();
        setupWhyUse();
        setupHelpButton();

        return rootView;
    }

    private void setupHelpButton() {
        Button helpMe = (Button) rootView.findViewById(R.id.help_me);
        helpMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"support@borrowfox.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Help me!");
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });
    }

    private void resetVisibility() {
        notifActive = false;
        notifInvite = false;
        whyRentB = false;
        payment = false;
    }

    private void setupCards() {
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        //        downloadAPIService.getAllCards(new TaskDoneListener<List<Card>>() {
        //            @Override
        //            public void OnTaskDone(List<Card> object) {
        //                if (object == null) {
        //                    Toast.makeText(getContext(), "Error while downloading your cards", Toast.LENGTH_SHORT).show();
        //                    return;
        //                }
        //
        //                setupCardsUI(object);
        //            }
        //        });

        downloadAPIService.getAllCards(new ResultListenerList<List<Card>>() {
            @Override
            public void onNullResult(List<Card> result) {
                Toast.makeText(getContext(), R.string.error_downloading_cards, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmptyList(List<Card> result) {
                setupCardsUI(result);
            }

            @Override
            public void onFilledList(List<Card> result) {
                setupCardsUI(result);
            }
        });


        rootParent = (LinearLayout) rootView.findViewById(R.id.pokus);
        cardss = new ArrayList<>();

        hideAddNewCardButton();

        final Button payment = (Button) rootView.findViewById(R.id.payment);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SettingsFragment.this.payment) {
                    for (int i = 0; i < cardss.size(); i++) {
                        cardss.get(i).setVisibility(View.GONE);
                    }

                    if (cardInfoView != null) {
                        cardInfoView.setVisibility(View.GONE);
                    }

                    if (addNewCardView != null) {
                        addNewCardView.setVisibility(View.GONE);
                    }

                    SettingsFragment.this.payment = false;
                    rootView.findViewById(R.id.add_new_card).setVisibility(View.GONE);
                    rootView.findViewById(R.id.divider).setVisibility(View.GONE);
                } else {
                    if (addNewCardView != null) {
                        addNewCardView.setVisibility(View.VISIBLE);
                    } else if (cardInfoView != null) {
                        cardInfoView.setVisibility(View.VISIBLE);
                        rootParent.getChildAt(rootParent.indexOfChild(cardInfoView) - 1).setVisibility(View.VISIBLE);
                    } else {
                        for (int i = 0; i < cardss.size(); i++) {
                            cardss.get(i).setVisibility(View.VISIBLE);
                        }

                        rootView.findViewById(R.id.add_new_card).setVisibility(View.VISIBLE);
                        rootView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
                    }

                    SettingsFragment.this.payment = true;
                }
            }
        });
    }

    private void setupCardsUI(List<Card> object) {

        final List<Card> cards = new ArrayList<>(object);

        List<String> c = new ArrayList<>();
        // cardss = new ArrayList<>();

        for (int i = 0; i < cards.size(); i++) {
            c.add(cards.get(i).getInfo());
        }

        final ArrayList<CardInfo> ccc = new ArrayList<>();

        for (int i = 0; i < c.size(); i++) {
            ccc.add(new CardInfo(c.get(i), object.get(i).getUser_name(), 3, 17));
        }

        Button add_new_card = (Button) rootView.findViewById(R.id.add_new_card);
        add_new_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupAddNewCardLayout();
            }
        });

        for (int i = 0; i < ccc.size(); i++) {
            final CardInfoView cardInfoView = new CardInfoView(getContext(), ccc.get(i), null);
            setupCardInfo(cardInfoView, ccc.get(i));
            rootParent.addView(cardInfoView, 1);
            cardss.add(cardInfoView);
            final int finalI1 = i;

            cardInfoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cardDetailIsShown) {
                        rootParent.removeViewAt(rootParent.indexOfChild(view) + 1);
                        SettingsFragment.this.cardInfoView = null;
                        cardDetailIsShown = false;

                        for (int i = 0; i < cardss.size(); i++) {
                            cardss.get(i).setVisibility(View.VISIBLE);
                        }

                        showAddNewCardButton();

                    } else {
                        final View v = View.inflate(getContext(), R.layout.setting_layout_sub_card, null);
                        SettingsFragment.this.cardInfoView = v;

                        TextView name = (TextView) v.findViewById(R.id.name);
                        name.setText(ccc.get(finalI1).getUser_name());

                        TextView month = (TextView) v.findViewById(R.id.month);
                        month.setText(String.valueOf(ccc.get(finalI1).getMonth()));

                        TextView year = (TextView) v.findViewById(R.id.year);
                        year.setText(String.valueOf(ccc.get(finalI1).getYear()));

                        Button delete = (Button) v.findViewById(R.id.delete);
                        delete.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                                b.setTitle(R.string.do_you_want_to_delete_this_card);
                                b.setMessage(R.string.this_step_cant_be_taken_back);
                                b.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        rootParent.removeView(v);
                                        cardss.remove(cardInfoView);
                                        rootParent.removeView(cardInfoView);
                                        cardDetailIsShown = false;
                                        if (SettingsFragment.this.cardInfoView != null) {
                                            rootParent.removeView(SettingsFragment.this.cardInfoView);
                                            SettingsFragment.this.cardInfoView = null;
                                        }

                                        for (int j = 0; j < cardss.size(); j++) {
                                            cardss.get(j).setVisibility(View.VISIBLE);
                                        }

                                        showAddNewCardButton();
                                    }
                                });
                                b.setNegativeButton(R.string.no, null);
                                b.show();
                            }
                        });

                        rootParent.addView(v, rootParent.indexOfChild(view) + 1);
                        cardDetailIsShown = true;

                        for (int i = 0; i < cardss.size(); i++) {
                            if (cardss.get(i) != view) {
                                cardss.get(i).setVisibility(View.GONE);
                            }
                        }

                        hideAddNewCardButton();
                    }
                }
            });
        }
        hideAllCards();
    }

    private void setupAddNewCardLayout() {
        final View v = View.inflate(getContext(), R.layout.layout_add_new_card, null);
        addNewCardView = v;

        final Button cancel = (Button) v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setTitle(R.string.are_you_sure);
                b.setMessage(R.string.filled_data_will_be_discard);
                b.setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        rootParent.removeView(v);
                        showAddNewCardButton();
                        showAllCards();
                        addNewCardView = null;
                    }
                });
                b.setNegativeButton(getString(R.string.no), null);
                b.show();
            }
        });

        final EditText card_number = (EditText) v.findViewById(R.id.number);
        card_number.addTextChangedListener(new TextWatcher() {

            boolean last_letter_is_empty = false;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    last_letter_is_empty = charSequence.toString().charAt(charSequence.length() - 1) == ' ';
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!last_letter_is_empty) {
                    if (charSequence.toString().length() == 4) {
                        card_number.setText(card_number.getText().toString() + " ");
                        card_number.setSelection(card_number.getText().length());
                    }

                    if (charSequence.toString().length() == 9) {
                        card_number.setText(card_number.getText().toString() + " ");
                        card_number.setSelection(card_number.getText().length());
                    }

                    if (charSequence.toString().length() == 14) {
                        card_number.setText(card_number.getText().toString() + " ");
                        card_number.setSelection(card_number.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        String[] typer = getContext().getResources().getStringArray(R.array.cards);
        Spinner card_type = (Spinner) v.findViewById(R.id.card_type);
        final ArrayAdapter adapter2 = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, typer);
        card_type.setAdapter(adapter2);

        card_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ImageView card_logo = (ImageView) v.findViewById(R.id.imageView14);
                switch (i) {
                    case 0:
                        card_logo.setImageResource(R.drawable.visa_mini_two);
                        break;
                    case 1:
                        card_logo.setImageResource(R.drawable.mastercard_mini_two);
                        break;
                    case 2:
                        card_logo.setImageResource(R.drawable.amex_mini_two);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final EditText number = (EditText) v.findViewById(R.id.number);
        final EditText name = (EditText) v.findViewById(R.id.name);
        final EditText year = (EditText) v.findViewById(R.id.year);
        final EditText month = (EditText) v.findViewById(R.id.month);
        final EditText cvc = (EditText) v.findViewById(R.id.textView47);

        final Button save = (Button) v.findViewById(R.id.create);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkCardData(number, name, year, month, cvc)) {

                    final String card_name_correct = number.getText().toString().replace(" ", "");
                    Log.d(App.TAG, "SettingsFragment, onClick: " + card_name_correct);

                    final com.stripe.android.model.Card card = new com.stripe.android.model.Card(card_name_correct,
                            Integer.valueOf(month.getText().toString()),
                            Integer.valueOf(year.getText().toString()),
                            cvc.getText().toString());

                    if (!card.validateCard()) {
                        Toast.makeText(getContext(), "Entered card data are wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    save.setEnabled(false);
                    save.setBackgroundResource(R.color.black54);
                    cancel.setBackgroundResource(R.color.black54);
                    cancel.setEnabled(false);

                    Stripe stripe = null;
                    try {
                        stripe = new Stripe("pk_test_DUTeYNGmwUyw94Gv1iDbL01B");
                    } catch (AuthenticationException e) {
                        e.printStackTrace();
                    }
                    stripe.createToken(
                            card,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    Log.d(App.TAG, "SettingsFragment, onSuccess: " + token.getId());

                                    final Spinner s = (Spinner) v.findViewById(R.id.card_type);

                                    DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
                                    downloadAPIService.createNewCard(token.getId(), s.getSelectedItemPosition() + 1,
                                            name.getText().toString(), card_name_correct.substring(12, 16),
                                            new TaskDoneListener<Boolean>() {
                                                @Override
                                                public void OnTaskDone(Boolean object) {
                                                    save.setEnabled(true);
                                                    cancel.setEnabled(true);
                                                    save.setBackgroundResource(R.color.weird_green);
                                                    cancel.setBackgroundResource(R.color.button_red);

                                                    if (object) {
                                                        Toast.makeText(getContext(), "Card was successfully created", Toast.LENGTH_SHORT).show();

                                                        CardInfo cardInfo = new CardInfo(getManufacturer(s.getSelectedItemPosition() + 1) +
                                                                " **** **** **** " + card_name_correct.substring(12, 16),
                                                                name.getText().toString(),
                                                                0,
                                                                0);

                                                        final CardInfoView cardInfoView = new CardInfoView(getContext(), cardInfo, null);

                                                        cardInfoView.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                if (cardDetailIsShown) {
                                                                    rootParent.removeViewAt(rootParent.indexOfChild(view) + 1);
                                                                    SettingsFragment.this.cardInfoView = null;
                                                                    cardDetailIsShown = false;

                                                                    for (int i = 0; i < cardss.size(); i++) {
                                                                        cardss.get(i).setVisibility(View.VISIBLE);
                                                                    }

                                                                    showAddNewCardButton();

                                                                } else {
                                                                    final View v = View.inflate(getContext(), R.layout.setting_layout_sub_card, null);
                                                                    SettingsFragment.this.cardInfoView = v;

                                                                    TextView namex = (TextView) v.findViewById(R.id.name);
                                                                    namex.setText(name.getText().toString());

                                                                    Button delete = (Button) v.findViewById(R.id.delete);
                                                                    delete.setOnClickListener(new View.OnClickListener() {

                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                                                                            b.setTitle(R.string.do_you_want_to_delete_this_card);
                                                                            b.setMessage(R.string.this_step_cant_be_taken_back);
                                                                            b.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                                    rootParent.removeView(v);
                                                                                    cardss.remove(cardInfoView);
                                                                                    rootParent.removeView(cardInfoView);
                                                                                    cardDetailIsShown = false;
                                                                                    if (SettingsFragment.this.cardInfoView != null) {
                                                                                        rootParent.removeView(SettingsFragment.this.cardInfoView);
                                                                                        SettingsFragment.this.cardInfoView = null;
                                                                                    }

                                                                                    for (int j = 0; j < cardss.size(); j++) {
                                                                                        cardss.get(j).setVisibility(View.VISIBLE);
                                                                                    }

                                                                                    showAddNewCardButton();
                                                                                }
                                                                            });
                                                                            b.setNegativeButton(R.string.no, null);
                                                                            b.show();
                                                                        }
                                                                    });

                                                                    rootParent.addView(v, rootParent.indexOfChild(view) + 1);
                                                                    cardDetailIsShown = true;

                                                                    for (int i = 0; i < cardss.size(); i++) {
                                                                        if (cardss.get(i) != view) {
                                                                            cardss.get(i).setVisibility(View.GONE);
                                                                        }
                                                                    }

                                                                    hideAddNewCardButton();
                                                                }

                                                            }
                                                        });

                                                        setupCardInfo(cardInfoView, cardInfo);
                                                        rootParent.addView(cardInfoView, 1);
                                                        cardss.add(cardInfoView);

                                                        rootParent.removeView(v);
                                                        showAddNewCardButton();
                                                        showAllCards();
                                                        addNewCardView = null;
                                                    } else {
                                                        Toast.makeText(getContext(), "Error while sending credit card", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                }

                                public void onError(Exception error) {
                                    save.setEnabled(true);
                                    cancel.setEnabled(true);
                                    save.setBackgroundResource(R.color.weird_green);
                                    cancel.setBackgroundResource(R.color.button_red);
                                    Toast.makeText(getContext(), "Error occured while communicating with Stripe!", Toast.LENGTH_SHORT).show();
                                    error.printStackTrace();
                                    Log.d(App.TAG, "SettingsFragment, onError: error");
                                }
                            }
                    );
                }
            }
        });

        rootParent.addView(v, 1);

        hideAllCards();
        hideAddNewCardButton();
    }

    private String getManufacturer(int i) {
        switch (i) {
            case 1:
                return "VISA";
            case 2:
                return "MASTERCARD";
            case 3:
                return "AMERICAN EXPRESS";
            default:
                return "VISA";
        }
    }

    private boolean checkCardData(EditText number, EditText name, EditText year, EditText month, EditText cvc) {
        if (number.getText().toString().length() == 0) {
            Toast.makeText(getContext(), "Credit card number is not entered", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (name.getText().toString().length() == 0) {
            Toast.makeText(getContext(), "Name is not entered", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (year.getText().toString().length() == 0) {
            Toast.makeText(getContext(), "Expiration year is not entered", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (month.getText().toString().length() == 0) {
            Toast.makeText(getContext(), "Exporation month is not entered", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (cvc.getText().toString().length() == 0) {
            Toast.makeText(getContext(), "Secret code is not entered", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void showAllCards() {
        for (int i = 0; i < cardss.size(); i++) {
            cardss.get(i).setVisibility(View.VISIBLE);
        }
    }

    private void hideAllCards() {
        for (int i = 0; i < cardss.size(); i++) {
            cardss.get(i).setVisibility(View.GONE);
        }
    }

    private void showAddNewCardButton() {
        rootView.findViewById(R.id.add_new_card).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
    }

    private void hideAddNewCardButton() {
        rootView.findViewById(R.id.add_new_card).setVisibility(View.GONE);
        rootView.findViewById(R.id.divider).setVisibility(View.GONE);
    }

    private void setupCardInfo(CardInfoView v, CardInfo cardInfo) {
        TextView cardnumber = (TextView) v.findViewById(R.id.card_number);
        cardnumber.setText(cardInfo.getCard_number());
    }

    private void setupWhyUse() {
        final Button why_rent = (Button) rootView.findViewById(R.id.rent);
        final LinearLayout content = (LinearLayout) rootView.findViewById(R.id.content);

        why_rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (whyRentB) {
                    content.setVisibility(View.GONE);
                    rootView.findViewById(R.id.divider_why_rent).setVisibility(View.GONE);
                    whyRentB = false;
                } else {
                    content.setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.divider_why_rent).setVisibility(View.VISIBLE);
                    whyRentB = true;
                }
            }
        });
    }

    private void setupInvite() {
        Button invite = (Button) rootView.findViewById(R.id.invite);
        final LinearLayout content = (LinearLayout) rootView.findViewById(R.id.content_invite);

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notifInvite) {
                    content.setVisibility(View.GONE);
                    notifInvite = false;
                    rootView.findViewById(R.id.divider_invite).setVisibility(View.GONE);
                } else {
                    content.setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.divider_invite).setVisibility(View.VISIBLE);
                    notifInvite = true;
                }
            }
        });

        LinearLayout email_invite = (LinearLayout) rootView.findViewById(R.id.email_invite);
        LinearLayout sms_invite = (LinearLayout) rootView.findViewById(R.id.sms_invite);

        email_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.borrowfox_invite));
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "ASDLKSJadlksjadlsjadlksadsadj");
                getContext().startActivity(Intent.createChooser(emailIntent, getString(R.string.send_mail)));
            }
        });

        sms_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));
                intent.putExtra("sms_body", "Sms invite");
                startActivity(intent);
            }
        });
    }

    private void setupNotifications() {
        Log.d(App.TAG, "SettingsFragment, setupNotifications:");
        boolean[] switches = new boolean[3];
        switches[0] = Utilities.getNotifMessages(getContext());
        switches[1] = Utilities.getNotifActivity(getContext());
        switches[2] = Utilities.getNotifAccount(getContext());

        Log.d(App.TAG, "SettingsFragment, setupNotifications: " + switches[0] + ", " + switches[1] + ", " + switches[2]);

        TextView ac1 = (TextView) rootView.findViewById(R.id.title2);
        ac1.setText(R.string.activity_notifications);

        TextView ac2 = (TextView) rootView.findViewById(R.id.note2);
        ac2.setText(R.string.requests_notifications_etc);

        TextView ac3 = (TextView) rootView.findViewById(R.id.title3);
        ac3.setText(R.string.account_notifications);

        TextView ac4 = (TextView) rootView.findViewById(R.id.note3);
        ac4.setText(R.string.changes_made_to_your_acoutn);

        Button nofit = (Button) rootView.findViewById(R.id.notif);
        nofit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notifActive) {
                    rootView.findViewById(R.id.divider_notif).setVisibility(View.GONE);
                    rootView.findViewById(R.id.r1).setVisibility(View.GONE);
                    rootView.findViewById(R.id.r2).setVisibility(View.GONE);
                    rootView.findViewById(R.id.r3).setVisibility(View.GONE);
                    notifActive = false;
                } else {
                    rootView.findViewById(R.id.divider_notif).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.r1).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.r2).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.r3).setVisibility(View.VISIBLE);
                    notifActive = true;
                }
            }
        });

        Switch messages = (Switch) rootView.findViewById(R.id.switch1);
        messages.setChecked(switches[0]);
        messages.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(App.TAG, "SettingsFragment, onCheckedChanged: 1" + b);
                Utilities.setNotifMessages(getContext(), b);
            }
        });

        Switch activity = (Switch) rootView.findViewById(R.id.switch2);
        activity.setChecked(switches[1]);
        activity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(App.TAG, "SettingsFragment, onCheckedChanged: 2" + b);
                Utilities.setNotifActivity(getContext(), b);
            }
        });

        Switch account = (Switch) rootView.findViewById(R.id.switch3);
        account.setChecked(switches[2]);
        account.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(App.TAG, "SettingsFragment, onCheckedChanged: 3" + b);
                Utilities.setNotifAccount(getContext(), b);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.settings));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(7);
    }
}