/*
 *
 * RequestItemStepTwoFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 4.7.16 8:22
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.request_item;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSwitch;
import com.reinto.borrowfox.Views.TopBarCustomView;
import com.reinto.borrowfox.fragments.general.ChatFragment;
import com.reinto.borrowfox.managers.FragManager;
import com.reinto.borrowfox.managers.RequestItemManager;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class RequestItemStepTwoFragment extends Fragment {

    public Spinner start, end, address;
    private View rootView;

    public static RequestItemStepTwoFragment newInstance() {
        Bundle args = new Bundle();
        RequestItemStepTwoFragment fragment = new RequestItemStepTwoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_request_item_step_two, container, false);

        TopBarCustomView topBarCustomView = (TopBarCustomView) rootView.findViewById(R.id.topbar);
        topBarCustomView.setupStepTwo();

        final Fragment f = ((MainActivity) getContext()).getSupportFragmentManager().findFragmentByTag(RequestItemStepOneFragment.class.getName());
        if (f != null) {
            LinearLayout chat = (LinearLayout) rootView.findViewById(R.id.chatt);
            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragManager.get().setFragment(
                            ChatFragment.newInstance(((RequestItemStepOneFragment) f).lend_user_id,
                            ((RequestItemStepOneFragment) f).item_id,
                            ((RequestItemStepOneFragment) f).lend_user_name));
                }
            });

            CircleImageView user_image = (CircleImageView) rootView.findViewById(R.id.icon);

            Glide.clear(user_image);
            Glide.with(getContext())
                    .load(APIService.URL_BASE_DATA + ((RequestItemStepOneFragment) f).user_url)
                    .centerCrop()
                    .override(Utilities.convertDpToPixels(56, getContext()), Utilities.convertDpToPixels(56, getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.profile_default)
                    .into(user_image);
        }

        TextView price = (TextView) rootView.findViewById(R.id.textView38);
        price.setText("£0.00");

        setupSpinners();
        setupSwitch();

        return rootView;
    }

    private void setupSwitch() {
        LinearLayout l = (LinearLayout) rootView.findViewById(R.id.top_part);
        CustomSwitch customSwitch = (CustomSwitch) rootView.findViewById(R.id.customswithc);
        customSwitch.setupTargetLayout(l);

        TextView t1 = (TextView) rootView.findViewById(R.id.text01);
        TextView t2 = (TextView) rootView.findViewById(R.id.text02);
        customSwitch.setupText(t1, t2);

        customSwitch.setOnStatusChange(new CustomSwitch.OnStatusChange() {
            @Override
            public void OnChange(int x) {
                RequestItemManager.get().getItem().deliveryType = x;
            }
        });
    }

    private void setupSpinners() {
        String[] objects = getResources().getStringArray(R.array.address);
        address = (Spinner) rootView.findViewById(R.id.address);
        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, objects);
        adapter.setDropDownViewResource(R.layout.layout_custom_spinner_grey);
        address.setAdapter(adapter);

        address.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                RequestItemManager.get().getItem().beginAddressId = address.getSelectedItemPosition() == 0 ?
                        Utilities.getHomeAddressId(getContext()) : Utilities.getWorkAddressId(getContext());
                RequestItemManager.get().getItem().endAddressId = address.getSelectedItemPosition() == 0 ?
                        Utilities.getHomeAddressId(getContext()) : Utilities.getWorkAddressId(getContext());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        String[] objects2 = getResources().getStringArray(R.array.times);
        start = (Spinner) rootView.findViewById(R.id.delivery_time);
        final ArrayAdapter adapter2 = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, objects2);
        start.setAdapter(adapter2);

        start.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                RequestItemManager.get().getItem().beginTime = getLongTimeWithHours(RequestItemManager.get().getItem().beginDate,
                        start.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String[] objects3 = getResources().getStringArray(R.array.times);
        end = (Spinner) rootView.findViewById(R.id.collection_time);
        final ArrayAdapter adapter3 = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, objects3);
        end.setAdapter(adapter3);
        end.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                RequestItemManager.get().getItem().endTime = getLongTimeWithHours(RequestItemManager.get().getItem().endDate,
                        end.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.request_item));
        Utilities.hideToolbarIcons(getContext());
    }

    private long getLongTimeWithHours(long time, int x) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        c.set(Calendar.HOUR_OF_DAY, x);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        return c.getTimeInMillis();
    }
}
