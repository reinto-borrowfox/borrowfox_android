/*
 *
 * MyMapFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 8.7.16 9:04
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reinto.borrowfox.R;

public class MyMapFragment extends SupportMapFragment implements OnMapReadyCallback {

    public GoogleMap map;
    private OnTouchListener onTouchListener;
    private OnMapReady onMapReady;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = super.onCreateView(inflater, container, savedInstanceState);

        getMapAsync(this);

        TouchableWrapper frameLayout = new TouchableWrapper(getActivity());
        frameLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        ((ViewGroup) layout).addView(frameLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return layout;
    }

    public void setListener(OnTouchListener listener) {
        onTouchListener = listener;
    }

    public void setOnMapReadyListener(OnMapReady onMapReadyListener) {
        this.onMapReady = onMapReadyListener;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;

        LatLng point = new LatLng(51.513672, -0.111839);

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                point, 14));
    }

    public void setMarker(double latitude, double longitude) {
        LatLng point = new LatLng(latitude, longitude);

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                point, 14));
        map.addMarker(new MarkerOptions()
                .position(point));

        map.addCircle(new CircleOptions().center(point).strokeWidth(5f).radius(500).strokeColor(getResources().getColor(R.color.button_red)));
        map.addCircle(new CircleOptions().center(point).strokeWidth(3f).radius(550).strokeColor(getResources().getColor(R.color.button_red)));
    }

    public interface OnTouchListener {
        void onTouch();
    }

    public interface OnMapReady {
        void mapIsReady();
    }

    public class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    onTouchListener.onTouch();
                    break;
                case MotionEvent.ACTION_UP:
                    onTouchListener.onTouch();
                    break;
            }
            return super.dispatchTouchEvent(event);
        }
    }
}