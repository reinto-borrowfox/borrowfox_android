/*
 *
 * NotificationFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 30.6.16 10:37
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.NotificationListAdapter;
import com.reinto.borrowfox.Models.NotificationItem;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.EndlessListView;
import com.reinto.borrowfox.Views.OnEndReachListener;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment {

    boolean loading;
    boolean shouldLoadNext = true;
    private View rootView;
    private NotificationListAdapter adapter;

    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NotificationListAdapter(getContext(), new ArrayList<NotificationItem>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat_list, container, false);

        ((MainActivity) getContext()).fragment = this.getClass();

        setupUI();

        if (adapter.getCount() == 0) {
            loadData();
        } else {
            Utilities.dissapearView(rootView.findViewById(R.id.progressBaro));
            Utilities.dissapearView(rootView.findViewById(R.id.clona));
        }

        return rootView;
    }

    private void loadData() {
        loading = true;
        Bootstrapper.getDownloadAPIService().downloadNotificationInfo(adapter.getCount(), new TaskDoneListener<ArrayList<NotificationItem>>() {
            @Override
            public void OnTaskDone(ArrayList<NotificationItem> object) {
                if (adapter.getCount() == 0) {
                    Utilities.dissapearView(rootView.findViewById(R.id.progressBaro));
                    Utilities.dissapearView(rootView.findViewById(R.id.clona));
                }

                if (object == null) {
                    Toast.makeText(getContext(), R.string.error_downloading_notifications, Toast.LENGTH_SHORT).show();
                    return;
                }

                shouldLoadNext = object.size() != 0;
                adapter.addData(object);
                loading = false;
            }
        });
    }

    private void setupUI() {
        EndlessListView listView = (EndlessListView) rootView.findViewById(R.id.chat_list_listview);
        listView.setDivider(new ColorDrawable(ResourcesCompat.getColor(getResources(), R.color.divider, null)));
        listView.setDividerHeight(Utilities.convertDpToPixels(1, getContext()));
        listView.setAdapter(adapter);
        listView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if (!loading && shouldLoadNext)
                    loadData();
            }
        });
    }

    public void addNotification(NotificationItem notificationItem) {
        List<NotificationItem> temp = new ArrayList<>();
        temp.add(notificationItem);
        adapter.addData(temp);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.notifilcations));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(6);
    }
}
