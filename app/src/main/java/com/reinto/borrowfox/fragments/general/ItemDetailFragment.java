package com.reinto.borrowfox.fragments.general;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.CalendaDayInfo;
import com.reinto.borrowfox.Models.CalendarBlockedDay;
import com.reinto.borrowfox.Models.FullItemData;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CalendarRequestItem;
import com.reinto.borrowfox.Views.ExpandableTextView;
import com.reinto.borrowfox.Views.ImageAdapter;
import com.reinto.borrowfox.Views.RectangleIndicator;
import com.reinto.borrowfox.Views.StartRating;
import com.reinto.borrowfox.fragments.request_item.RequestItemStepOneFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemDetailFragment extends Fragment {

    public static final String ITEM_ID = "item_id";
    public static final String OPEN_CALENDAR = "open_calendar";

    private List<Long> selectedDays;
    private ArrayList<CalendaDayInfo> calendarData;
    private View rootView;
    private FullItemData fullItemData;
    private Button from, to;
    private List<CalendarBlockedDay> blockedData, unavailableData;
    private MyMapFragment myMapFragment;

    private int minimumRental;

    public static ItemDetailFragment newInstance(int id, boolean openCalendar) {
        Bundle args = new Bundle();
        args.putInt(ITEM_ID, id);
        args.putBoolean(OPEN_CALENDAR, openCalendar);
        ItemDetailFragment fragment = new ItemDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_item_detail, container, false);

        myMapFragment = new MyMapFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.map_frame, myMapFragment)
                .commit();

        if (fullItemData == null) {
            DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
            downloadAPIService.downloadFullItem(getArguments().getInt(ITEM_ID), new TaskDoneListener<FullItemData>() {
                @Override
                public void OnTaskDone(FullItemData object) {
                    if (object == null) {
                        Toast.makeText(getContext(), R.string.error_downlading_items, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    blockedData = new ArrayList<>();
                    unavailableData = new ArrayList<>();

                    blockedData.addAll(object.getBlockedDays());
                    unavailableData.addAll(object.getUnavailableDays());

                    fullItemData = object;
                    Utilities.dissapearView(rootView.findViewById(R.id.clona));
                    Utilities.dissapearView(rootView.findViewById(R.id.progressoBar));
                    setupUI(object);
                    setupMap();
                }
            });
        } else {
            Utilities.dissapearView(rootView.findViewById(R.id.clona));
            Utilities.dissapearView(rootView.findViewById(R.id.progressoBar));
            setupMap();
            setupUI(fullItemData);
        }

        final ScrollView scroll = (ScrollView) rootView.findViewById(R.id.scroll);
        myMapFragment.setListener(new MyMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scroll.requestDisallowInterceptTouchEvent(true);
            }
        });

        if (getArguments().getBoolean(OPEN_CALENDAR, false)) {
            calendarData = new ArrayList<>();
            /*for (int i = 0; i < 3; i++) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_MONTH, i);
                calendarData.add(new CalendaDayInfo(c, 2));
            }

            for (int i = 0; i < 3; i++) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_MONTH, 8 + i);
                calendarData.add(new CalendaDayInfo(c, 1));
            }

            Calendar c = Calendar.getInstance();
            c.add(Calendar.WEEK_OF_MONTH, 3);
            calendarData.add(new CalendaDayInfo(c, 2));*/

            Log.d(App.TAG, "ItemDetailFragment, onCreateView: blockeddays " + blockedData.toString());
            Log.d(App.TAG, "ItemDetailFragment, onCreateView: unavailabledays " + unavailableData.toString());

            for (int i = 0; i < blockedData.size(); i++) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(blockedData.get(i).getDate());
                calendarData.add(new CalendaDayInfo(c, 1));
            }
            for (int i = 0; i < unavailableData.size(); i++) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(unavailableData.get(i).getDate());
                calendarData.add(new CalendaDayInfo(c, 2));
            }

            // minimumRental +1 -> one extra day for user to return item
            CalendarRequestItem dialog = new CalendarRequestItem(getContext(), calendarData, minimumRental);
            dialog.setCancelable(true);
            dialog.setDialogResult(new CalendarRequestItem.OnMyDialogResult() {
                @Override
                public void finish(String fromm, String too, List<Long> days) {
                    from.setText(fromm.replace("\n", " "));
                    to.setText(too.replace("\n", " "));
                    selectedDays = days;
                }
            });

            dialog.show();
        }

        return rootView;
    }

    private void setupMap() {
        myMapFragment.setOnMapReadyListener(new MyMapFragment.OnMapReady() {
            @Override
            public void mapIsReady() {
                try {
                    Geocoder geocoder = new Geocoder(getContext());
                    List<Address> addresses;
                    addresses = geocoder.getFromLocationName(fullItemData.getUser_locality(), 1);
                    if (addresses.size() > 0) {
                        double latitude = addresses.get(0).getLatitude();
                        double longitude = addresses.get(0).getLongitude();
                        Log.d(App.TAG, "ItemDetailFragment, setupUI: " + latitude + ", " + longitude);
                        myMapFragment.setMarker(latitude, longitude);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setupUI(final FullItemData data) {
        final int pricePerDay = data.getPrice_per_day();
        final int pricePerExtraDay = data.getPrice_per_extra_day();
        final int deposit = data.getDeposit();
        minimumRental = data.getMinimum_rental_period() == 0 ? 1 : data.getMinimum_rental_period();
        calendarData = new ArrayList<>();

        final TextView perDay = (TextView) rootView.findViewById(R.id.price_per_day);
        perDay.setText(String.format("£%d", pricePerDay));

        final TextView extraDay = (TextView) rootView.findViewById(R.id.extraDay);
        extraDay.setText(String.format("£%d", pricePerExtraDay));

        TextView depositTV = (TextView) rootView.findViewById(R.id.deposit);
        depositTV.setText(String.format("£%d", deposit));

        StartRating rating = (StartRating) rootView.findViewById(R.id.rating);
        rating.setFilledStart(data.getRating());

        TextView price = (TextView) rootView.findViewById(R.id.item_available_price);
        price.setText(Utilities.getFormatedPrice(String.valueOf(pricePerDay)));

        TextView name = (TextView) rootView.findViewById(R.id.name);
        name.setText(data.getName());

        TextView location = (TextView) rootView.findViewById(R.id.location);
        location.setText(data.getUser_locality());

        TextView rental = (TextView) rootView.findViewById(R.id.minimal_rental);
        rental.setText(data.getMinimum_rental_period() + getString(R.string.day_minimum_rental));

        TextView delivery = (TextView) rootView.findViewById(R.id.delivery);
        if (data.getDelivery() == 1) {
            delivery.setText(R.string.courrier_delivery_and_colection);
        } else {
            delivery.setText(R.string.personal_pick_up);
        }

        ArrayList<String> urls = new ArrayList<>(data.getImages());
        selectedDays = new ArrayList<>();

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        ImageAdapter adapter = new ImageAdapter(getContext(), urls);
        viewPager.setAdapter(adapter);

        final RectangleIndicator indicator = (RectangleIndicator) rootView.findViewById(R.id.rectangleIndicator);
        indicator.setNumber_of_pages(urls.size());
        indicator.setIndicator_height(8);
        indicator.setupUI();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                indicator.changeIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        ExpandableTextView expandableTextView = (ExpandableTextView) rootView.findViewById(R.id.expandableTextView);
        expandableTextView.setText(data.getDescription());

        LinearLayout fromAndTo = (LinearLayout) rootView.findViewById(R.id.from_and_to);

        to = (Button) fromAndTo.findViewById(R.id.to);
        from = (Button) fromAndTo.findViewById(R.id.from);

        from.setOnClickListener(new CalendarOnClickListener(from, to));
        to.setOnClickListener(new CalendarOnClickListener(from, to));

        Button continueRequest = (Button) rootView.findViewById(R.id.send_request);
        continueRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedDays.size() == 0) {
                    Toast.makeText(getContext(), R.string.please_choose_day_to_rent_this_item, Toast.LENGTH_SHORT).show();
                    return;
                }
                FragManager.get().setFragment(RequestItemStepOneFragment.newInstance(selectedDays.get(0),
                        selectedDays.get(1), pricePerDay, pricePerExtraDay, deposit, minimumRental,
                        data.getUser_url(), data.getLend_user_id(), data.getUser_full_name(), data.getId(), calendarData));
            }
        });

        CircleImageView userImage = (CircleImageView) rootView.findViewById(R.id.user_image);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.getLend_user_id() == Utilities.getUserId(getContext())) {
                    //my item
                    FragManager.get().setFragment(MyProfileFragment.newInstance());
                } else {
                    // Someone elses item
                    FragManager.get().setFragment(NotMyProfileFragment.newInstance(data.getLend_user_id()));
                }
            }
        });

        Glide.clear(userImage);
        Glide.with(getContext())
                .load(APIService.URL_BASE_DATA + data.getUser_url())
                .centerCrop()
                .override(Utilities.convertDpToPixels(56, getContext()), Utilities.convertDpToPixels(56, getContext()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.profile_default)
                .into(userImage);

        Button sendMessage = (Button) rootView.findViewById(R.id.button2);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragment(ChatFragment.newInstance(data.getLend_user_id(),
                        data.getId(), data.getUser_full_name()));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.borrowfox));
        Utilities.showToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(0);
    }

    //    private void openRequestItemFragment(int day, int extra, int deposit, int min_day,
    //                                         String user_url, int lender_id, int item_id,
    //                                         String full_name) {
    //        Fragment f;
    //        try {
    //            f = RequestItemStepOneFragment.class.newInstance();
    //            Bundle bundle = new Bundle();
    //            bundle.putLong(FROM, selectedDays.get(0));
    //            bundle.putLong(TO, selectedDays.get(1));
    //            bundle.putInt(PRICE_DAY, day);
    //            bundle.putInt(EXTRA_DAY, extra);
    //            bundle.putInt(DEPOSIT, deposit);
    //            bundle.putInt(MIN_DAY, min_day);
    //            bundle.putString(USER_URL, user_url);
    //            bundle.putInt(LENDER_ID, lender_id);
    //            bundle.putInt(ITEM_ID, item_id);
    //            bundle.putString(USER_NAME, full_name);
    //            bundle.putParcelableArrayList(API, calendarData);
    //            f.setArguments(bundle);
    //
    //            ((MainActivity) getContext()).getSupportFragmentManager().beginTransaction()
    //                    .addToBackStack(RequestItemStepOneFragment.class.getName())
    //                    .replace(R.id.content, f, RequestItemStepOneFragment.class.getName())
    //                    .commit();
    //        } catch (java.lang.InstantiationException e) {
    //            e.printStackTrace();
    //        } catch (IllegalAccessException e) {
    //            e.printStackTrace();
    //        }
    //    }

    private class CalendarOnClickListener implements View.OnClickListener {

        private Button from, to;

        CalendarOnClickListener(Button from, Button to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public void onClick(View view) {
            Log.d(App.TAG, "ItemDetailFragment, onCreateView: " + blockedData.toString());
            Log.d(App.TAG, "ItemDetailFragment, onCreateView: " + unavailableData.toString());

            for (int i = 0; i < blockedData.size(); i++) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(blockedData.get(i).getDate());
                calendarData.add(new CalendaDayInfo(c, 1));
            }

            for (int i = 0; i < unavailableData.size(); i++) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(unavailableData.get(i).getDate());
                calendarData.add(new CalendaDayInfo(c, 2));
            }

            CalendarRequestItem dialog = new CalendarRequestItem(getContext(), calendarData, minimumRental);
            dialog.setCancelable(true);
            dialog.setDialogResult(new CalendarRequestItem.OnMyDialogResult() {
                @Override
                public void finish(String fromm, String too, List<Long> days) {
                    from.setText(fromm.replace("\n", " "));
                    to.setText(too.replace("\n", " "));
                    selectedDays = days;
                }
            });

            dialog.show();
        }
    }
}
