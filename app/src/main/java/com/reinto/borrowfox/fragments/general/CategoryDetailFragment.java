package com.reinto.borrowfox.fragments.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.RecyclerViewAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.CustomListeners.RecyclerItemClickListener;
import com.reinto.borrowfox.Models.ItemData;
import com.reinto.borrowfox.Models.SingleCategoryItemModel;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSearchView;
import com.reinto.borrowfox.Views.EndlessRecyclerView;
import com.reinto.borrowfox.Views.OnEndReachListener;
import com.reinto.borrowfox.managers.FragManager;

import java.util.ArrayList;
import java.util.List;

public class CategoryDetailFragment extends Fragment {

    public static final String CATEGORY_ID = "category_id";
    public static final String TITLE = "title";
    private ArrayList<SingleCategoryItemModel> dataToAdd;
    private View rootView;
    private boolean shouldLoadNext = true;
    private boolean loading;
    private RecyclerViewAdapter adapter;

    public static CategoryDetailFragment newInstance(String s, int id) {
        Bundle args = new Bundle();
        args.putString(TITLE, s);
        args.putInt(CATEGORY_ID, id);
        CategoryDetailFragment fragment = new CategoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new RecyclerViewAdapter(getContext(), new ArrayList<SingleCategoryItemModel>());
        dataToAdd = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_detail, container, false);

        setupListView();

        if (adapter.getItemCount() == 0) {
            loadItems();
        } else {
            rootView.findViewById(R.id.progressBaroo).setVisibility(View.GONE);
        }

        return rootView;
    }

    private void downloadImages(final List<ItemData> objecto) {
        for (int i = 0; i < objecto.size(); i++) {

            final int finalI = i;
            objecto.get(i).downloadImages(new ItemData.OnMyDialogResult() {
                @Override
                public void finish() {
                    setupObjects(objecto, finalI);
                }
            });
        }
    }

    void loadItems(){
        loading = true;
        Bootstrapper.getDownloadAPIService().downloadCategoryitems(getArguments().getInt(CATEGORY_ID), adapter.getItemCount(), new ResultListenerList<List<ItemData>>() {

            @Override
            public void onNullResult(List<ItemData> result) {
                loading = false;
                shouldLoadNext = true;
                Toast.makeText(getContext(), R.string.error_downloading_items_category, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmptyList(List<ItemData> result) {
                if(adapter.getItemCount() == 0) {
                    rootView.findViewById(R.id.category_detail_no_items).setVisibility(View.VISIBLE);
                    Utilities.dissapearView(rootView.findViewById(R.id.clona));
                    Utilities.dissapearView(rootView.findViewById(R.id.progressBaroo));
                } else {
                    shouldLoadNext = false;
                }
                loading = false;
            }

            @Override
            public void onFilledList(List<ItemData> result) {
                downloadImages(result);
                loading = false;
                shouldLoadNext = result.size() != 0;
            }
        });
    }

    private void setupObjects(List<ItemData> objecto, int i) {
        final SingleCategoryItemModel x = new SingleCategoryItemModel(
                objecto.get(i).getId(),
                String.valueOf(objecto.get(i).getPrice_per_day()),
                objecto.get(i).getName(),
                objecto.get(i).getUser_locality(),
                objecto.get(i).getImages(),
                objecto.get(i).getUser_filename());

        Log.d(App.TAG, "CategoryDetailFragment, setupObjects: " + x.toString() + " || size: " + objecto.size() + " || i: " + i);
        dataToAdd.add(x);

        if (objecto.size() == (i + 1)) {
            adapter.addData(dataToAdd);
            dataToAdd.clear();
        }
    }

    private void setupListView() {
        EndlessRecyclerView recyclerView = (EndlessRecyclerView) rootView.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FragManager.get().setFragment(ItemDetailFragment.newInstance(adapter.getItem(position).getItem_id(), false));
            }
        }));
        recyclerView.setThreshold(3);
        recyclerView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if(!loading && shouldLoadNext)
                    loadItems();
            }
        });


        Utilities.dissapearView(rootView.findViewById(R.id.clona));
        dissapearView(rootView.findViewById(R.id.progressBaroo));

        if (((MainActivity) getContext()).customSearchView != null) {
            CustomSearchView customSearchView = ((MainActivity) getContext()).customSearchView;
            customSearchView.setOnSearchIconClick(new CustomSearchView.OnSearchClick() {
                @Override
                public void onSeachClick(String text) {
                    adapter.filter(text);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getArguments().getString(TITLE).equals("")) {
            Utilities.setToolbarTitle(getContext(), getArguments().getString(TITLE));
            Utilities.showToolbarIcons(getContext());
        }
        ((MainActivity) getContext()).selectItemInMenu(0);
    }

    private void dissapearView(final View view) {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(600);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animation);
    }
}