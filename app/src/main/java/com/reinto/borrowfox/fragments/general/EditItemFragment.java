/*
 *
 * EditItemFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 11.7.16 10:05
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.FullItemData;
import com.reinto.borrowfox.Models.NewItemInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.fragments.add_new_item.PhotosListFragment;
import com.reinto.borrowfox.managers.CategoryManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class EditItemFragment extends Fragment {

    public static final String ITEM_ID = "item_id";
    public static final String ADDRESSES = "addresses";

    private View rootView;
    private Spinner category;
    private ImageView main_photo;
    private int itemId;

    private NewItemInfo newItemInfo;

    private String name, desc;
    private int categoryId, perDay, deposit, perExtraDay, minimumRentalPeriod, online, collection;
    private ArrayList<String> addresses;
    private CategoryManager categories;

    public static EditItemFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(ITEM_ID, id);
        EditItemFragment fragment = new EditItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemId = getArguments().getInt(ITEM_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_edit_item, container, false);

        addresses = new ArrayList<>();

        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadFullItem(itemId, new TaskDoneListener<FullItemData>() {
            @Override
            public void OnTaskDone(FullItemData object) {
                if (object == null) {
                    Toast.makeText(getContext(), R.string.error_downloading_item, Toast.LENGTH_SHORT).show();
                    return;
                }

                name = object.getName();
                desc = object.getDescription();
                categoryId = object.getCategoryId();
                perDay = object.getPrice_per_day();
                deposit = object.getDeposit();
                perExtraDay = object.getPrice_per_extra_day();
                minimumRentalPeriod = object.getMinimum_rental_period();
                collection = object.getDelivery();
                online = object.getStatus();
                addresses.addAll(object.getImages());

                Log.d(App.TAG, "EditItemFragment, OnTaskDone: " + object.toString());

                List<Long> list = new ArrayList<>();

                for (int i = 0; i < object.getBlockedDays().size(); i++) {
                    list.add(object.getBlockedDays().get(i).getDate());
                }

                newItemInfo = new NewItemInfo(name, desc, addresses, categoryId, perDay, perExtraDay, deposit, minimumRentalPeriod, collection, list);

                setupUI();
            }
        });

        return rootView;
    }

    private void setupUI() {
        setupSpinners(categoryId, collection, 1 - online);
        setupEdittexts(name, desc, perDay, deposit, perExtraDay, minimumRentalPeriod);
        setupMainPhoto(addresses);
    }

    public void setAddresses(ArrayList<String> list) {
        this.addresses.clear();
        this.addresses = list;
    }

    private void setupMainPhoto(final ArrayList<String> main_photo_url) {
        main_photo = (ImageView) rootView.findViewById(R.id.main_photo);

        if (main_photo_url.size() != 0) {
            Glide.with(getContext()).load(main_photo_url.get(0)).centerCrop().into(main_photo);
        } else {
            main_photo.setBackgroundResource(R.drawable.profile_default);
        }

        main_photo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                openPhotosListFragmentWithAddresses(main_photo_url);
            }
        });
    }

    private void setupEdittexts(String name, String desc, int per_day, int deposit, int per_extra_day,
                                int minimum_rental_period) {
        EditText item_name = (EditText) rootView.findViewById(R.id.item_name);
        item_name.setText(name.toUpperCase());
        item_name.setAllCaps(true);

        EditText description = (EditText) rootView.findViewById(R.id.description);
        description.setAllCaps(true);
        description.setText(desc.toUpperCase());
        description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                return false;
            }
        });

        EditText pd = (EditText) rootView.findViewById(R.id.per_day);
        pd.setText(String.valueOf(per_day));

        EditText depisot = (EditText) rootView.findViewById(R.id.deposit);
        depisot.setText(String.valueOf(deposit));

        EditText per_ex_day = (EditText) rootView.findViewById(R.id.per_extra_day);
        per_ex_day.setText(String.valueOf(per_extra_day));

        EditText min = (EditText) rootView.findViewById(R.id.min_rental);
        min.setText(String.valueOf(minimum_rental_period));
    }

    private void setupSpinners(final int categoryId, int collectio, final int online) {
        DownloadAPIService d = Bootstrapper.getDownloadAPIService();

        category = (Spinner) rootView.findViewById(R.id.category);
        d.getJustCategoryNames(new TaskDoneListener<CategoryManager>() {
            @Override
            public void OnTaskDone(CategoryManager object) {
                if (object != null) {
                    categories = object;

                    final ArrayAdapter adapter = new ArrayAdapter(getContext(),
                            R.layout.layout_custom_spinner, categories.getNames());

                    category.setAdapter(adapter);
                    category.setSelection(categories.getIndexForSpinner(categoryId));
                } else {
                    Toast.makeText(getContext(), R.string.error_downloading_categories,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        Spinner collection = (Spinner) rootView.findViewById(R.id.item_collection);
        String[] objects = getResources().getStringArray(R.array.item_collection);
        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner,
                objects);
        collection.setAdapter(adapter);
        collection.setSelection(collectio - 1);

        Spinner online_offline = (Spinner) rootView.findViewById(R.id.online_offline);
        String[] objects2 = getResources().getStringArray(R.array.online_offline);
        final ArrayAdapter adapter2 = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner,
                objects2);
        online_offline.setAdapter(adapter2);
        online_offline.setSelection(online);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.edit_item));
        Utilities.hideToolbarIcons(getContext());

        if (addresses.size() > 0) {
            File f = new File(addresses.get(0));
            if (f.exists()) {
                CreateBitmap cc = new CreateBitmap(addresses.get(0));
                cc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                fillMainImageWithGlide(addresses.get(0));
            }
        }
    }

    private void openPhotosListFragmentWithAddresses(ArrayList<String> address) {
        Fragment f;
        try {
            f = PhotosListFragment.class.newInstance();
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ADDRESSES, address);
            f.setArguments(bundle);

            ((MainActivity) getContext()).getSupportFragmentManager().beginTransaction()
                    .addToBackStack(PhotosListFragment.class.getName())
                    .replace(R.id.content, f, PhotosListFragment.class.getName())
                    .commit();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void fillMainImageWithGlide(String s) {
        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                main_photo.setImageBitmap(resource);
            }
        };

        Glide.with(getContext()).load(s).asBitmap().into(target);
    }

    private class CreateBitmap extends AsyncTask<Void, Void, Bitmap> {
        String address;

        CreateBitmap(String address) {
            this.address = address;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 4;
            o.inPreferredConfig = Bitmap.Config.RGB_565;

            return BitmapFactory.decodeFile(address, o);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            main_photo.setImageBitmap(bitmap);
        }
    }
}