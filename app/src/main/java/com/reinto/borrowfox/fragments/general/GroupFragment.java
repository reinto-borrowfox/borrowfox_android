package com.reinto.borrowfox.fragments.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.SimpleGroupAdapter;
import com.reinto.borrowfox.Models.GroupModel;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSearchView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reinto Workstation on 18.11.2016.
 */

public class GroupFragment extends Fragment {

    public static final String CATEGORY_ID = "category_id";
    public static final String TITLE = "title";
    public TextView tip;
    private View rootView;
    private ArrayList<GroupModel> list;
    private SimpleGroupAdapter adapter;

    public static GroupFragment newInstance(String s, int id){
        Bundle args = new Bundle();
        args.putString(TITLE, s);
        args.putInt(CATEGORY_ID, id);
        GroupFragment fragment = new GroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_groups, container, false);
        final ProgressBar p = (ProgressBar) rootView.findViewById(R.id.pb_fragment_group);
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        if (list == null){

            downloadAPIService.getGroupsUpdated(new TaskDoneListener<List<GroupModel>>() {
                @Override
                public void OnTaskDone(List<GroupModel> object) {
                    dissapearView(p);
                    if (object == null) {
                        return;
                    }
                    list = new ArrayList<>(object);
                    setupListView();
                }

            });

        }else{
            p.setVisibility(View.GONE);
            setupListView();
        }
        ((MainActivity) getContext()).customSearchView.setOnSearchIconClick(new CustomSearchView.OnSearchClick() {
            @Override
            public void onSeachClick(String text) {
                adapter.getFilter().filter(text);
            }
        });

        return rootView;
    }

    private void setupListView() {
        adapter = new SimpleGroupAdapter(getContext(), list);
        ListView listView = (ListView) rootView.findViewById(R.id.lw_fragment_group);
        listView.setAdapter(adapter);
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (tip != null) {
                    if (tip.getVisibility() == View.VISIBLE) {
                        dissapearView(tip);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        dissapearView(rootView.findViewById(R.id.v_fragment_group_clona));

    }
    private void dissapearView(final View view) {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(600);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        view.startAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.borrowfox));
        Utilities.showToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(0);
    }


}
