/*
 *
 * BorrowLendsFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 6.7.16 9:32
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.LendAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.LendData;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.EndlessListView;
import com.reinto.borrowfox.Views.OnEndReachListener;
import com.reinto.borrowfox.fragments.add_new_item.FirstStepFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.util.ArrayList;
import java.util.List;

public class LendFragment extends Fragment {

    private View rootView;
    private Spinner spinner;
    private EndlessListView listView;
    private LendAdapter adapter;
    private List<LendData> data;
    private int startType = 1;
    private boolean loading;
    private boolean shouldLoadNext = true;
    private boolean isLoadingViewVisible;
    private int selectedType = Emuns.LendItemType.LEND_REQUESTED;

    public static LendFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        LendFragment fragment = new LendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new LendAdapter(getContext(), new ArrayList<LendData>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lend, container, false);

        spinner = (Spinner) rootView.findViewById(R.id.lend_spinner);
        listView = (EndlessListView) rootView.findViewById(R.id.lend_listView);
        listView.setEmptyView(rootView.findViewById(R.id.lend_emptyView));
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setAdapter(adapter);
        listView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if (!loading && shouldLoadNext)
                    downloadCorrectItems(selectedType, false);
            }
        });

        setupUI();
        setupButtons();

        try {
            startType = getArguments().getInt("type");
        } catch (NullPointerException e) {
            // did not go from notification
        }

        if (data == null) {
            Log.d(App.TAG, "LendFragment, onCreateView: null");
            downloadCorrectItems(startType,true);

            if (adapter.getCount() == 0) {
                showLoading();
                downloadCorrectItems(Emuns.LendItemType.LEND_REQUESTED, true);
            } else {
                hideLoading();
            }
        }

        return rootView;
    }

    private void downloadCorrectItems(int type, boolean reset) {
        loading = true;

        if (reset) {
            showLoading();
            adapter = new LendAdapter(getContext(), new ArrayList<LendData>());
        }

        final DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadItemByLender(type, adapter.getCount(), new ResultListenerList<List<LendData>>() {

            @Override
            public void onNullResult(List<LendData> result) {
                hideLoading();
                loading = false;
                shouldLoadNext = true;
                Toast.makeText(getContext(), R.string.error_download_borrow_items, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmptyList(List<LendData> result) {
                hideLoading();
                loading = false;
                shouldLoadNext = false;
            }

            @Override
            public void onFilledList(List<LendData> result) {
                hideLoading();
                Log.d(App.TAG, "LendFragment, OnTaskDone: " + result.toString());
                downloadImages(result);

                loading = false;
            }
        });
    }

    private void downloadImages(final List<LendData> object) {
        final int[] temp = {0};

        for (int i = 0; i < object.size(); i++) {
            object.get(i).downloadImages(new LendData.OnMyDialogResult() {
                @Override
                public void finish() {
                    temp[0]++;
                    if (temp[0] == object.size()) {
                        adapter.addData(object);
                        hideLoading();
                    }
                }
            });
        }
    }

    private void showLoading() {
        if (!isLoadingViewVisible) {
            rootView.findViewById(R.id.lend_clona).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.lend_progressBar).setVisibility(View.VISIBLE);
        }
        isLoadingViewVisible = true;
    }

    private void hideLoading() {
        if (isLoadingViewVisible) {
            Utilities.dissapearView(rootView.findViewById(R.id.lend_clona));
            Utilities.dissapearView(rootView.findViewById(R.id.lend_progressBar));
        }
        isLoadingViewVisible = false;
    }

    private void setupButtons() {
        final Button borrow = (Button) rootView.findViewById(R.id.lend_borrow);
        borrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragmentControl(BorrowFragment.class);
            }
        });

        Button addItem = (Button) rootView.findViewById(R.id.lend_addNewItem);
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragment(FirstStepFragment.newInstance());
            }
        });
    }

    private void setupUI() {
        String[] objects = getResources().getStringArray(R.array.my_lends_options);

        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey_two, objects);
        adapter.setDropDownViewResource(R.layout.layout_custom_spinner_grey_two);
        spinner.setAdapter(adapter);
        spinner.setSelection(0, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        selectedType = Emuns.LendItemType.LEND_REQUESTED;
                        break;
                    case 1:
                        selectedType = Emuns.LendItemType.LEND_ON_LOAN;
                        break;
                    case 2:
                        selectedType = Emuns.LendItemType.LEND_AVAILABLE;
                        break;
                    case 3:
                        selectedType = Emuns.LendItemType.LEND_OFFLINE;
                        break;
                }

                downloadCorrectItems(selectedType, true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setType(int type) {
        downloadCorrectItems(type, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.borrowfox));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(4);
    }
}