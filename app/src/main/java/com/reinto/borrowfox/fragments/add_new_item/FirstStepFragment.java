package com.reinto.borrowfox.fragments.add_new_item;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerBooleanTwo;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.managers.CategoryManager;
import com.reinto.borrowfox.managers.FragManager;

import java.io.File;
import java.util.ArrayList;

public class FirstStepFragment extends Fragment {

    public static final int CAMERA_ID = 150;
    public static final int REQUEST_CODE = 40;
    public static final int GALLERY_ID = 200;
    private ArrayList<String> addresses;
    private View rootView;

    private EditText name, desc;
    private Spinner category;
    private ImageView photos;
    private CategoryManager categories;
    private boolean checkBank = false;

    public static FirstStepFragment newInstance() {
        Bundle args = new Bundle();
        FirstStepFragment fragment = new FirstStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_item_first_step, container, false);

        if (addresses == null) {
            addresses = new ArrayList<>();
        }

        name = (EditText) rootView.findViewById(R.id.item_name);
        desc = (EditText) rootView.findViewById(R.id.description);
        category = (Spinner) rootView.findViewById(R.id.category);
        photos = (ImageView) rootView.findViewById(R.id.photos);

        if (!checkBank) {
            checkBankAccount();
            checkBank = true;
        }

        if (categories == null) {
            DownloadAPIService d = Bootstrapper.getDownloadAPIService();
            d.getJustCategoryNames(new TaskDoneListener<CategoryManager>() {
                @Override
                public void OnTaskDone(CategoryManager object) {
                    if (object == null) {
                        return;
                    }

                    categories = object;

                    final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner, object.getNames());
                    category.setAdapter(adapter);
                }
            });


            //            d.getJustCategoryNames(new TaskDoneListener<List<Category>>() {
            //                @Override
            //                public void OnTaskDone(List<Category> object) {
            //                    if (object != null && object.size() > 0) {
            //
            //                        List<String> names = new ArrayList<>();
            //                        for (int i = 0; i < object.size(); i++) {
            //                            if (!object.get(i).getName().equals(MOST_POPULAR))
            //                                names.add(object.get(i).getName());
            //                        }
            //
            //                        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner, names);
            //                        category.setAdapter(adapter);
            //
            //                        try {
            //                            category.setSelection(((MainActivity) getContext()).newItem.getCategory());
            //                        } catch (NullPointerException e) {
            //                            category.setSelection(0);
            //                        }
            //                    }
            //                }
            //            });
        } else {
            final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner, categories.getNames());
            category.setAdapter(adapter);

            try {
                category.setSelection(((MainActivity) getContext()).newItem.getCategory());
            } catch (NullPointerException e) {

            }
        }

        photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addresses.size() != 0) {

                    FragManager.get().setFragment(PhotosListFragment.newInstance(addresses));

                    //openPhotosListFragmentWithAddresses(addresses);
                    return;
                }

                final CharSequence[] items = {getString(R.string.take_new_photo), getString(R.string.pick_image_from_gallery),
                        getString(R.string.cancel)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.add_item_photo);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(getString(R.string.take_new_photo))) {
                            requestPermission(new OnFinish() {
                                @Override
                                public void onFinish() {
                                    getImageFromStorage();
                                }
                            });
                        } else if (items[item].equals(getString(R.string.pick_image_from_gallery))) {
                            requestPermission(new OnFinish() {
                                @Override
                                public void onFinish() {
                                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, 200);
                                }
                            });
                        } else if (items[item].equals(getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

        Button saveAndProceed = (Button) rootView.findViewById(R.id.save);
        saveAndProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkDataCompletion()) {
                    ((MainActivity) getContext()).newItem.setItem_name(name.getText().toString());
                    ((MainActivity) getContext()).newItem.setDescription(desc.getText().toString());
                    ((MainActivity) getContext()).newItem.setCategory(categories.getId((String) category.getSelectedItem()));
                    ((MainActivity) getContext()).newItem.setAddresses(addresses);

                    FragManager.get().setFragment(SecondStepFragment.newInstance());
                }
            }
        });

        return rootView;
    }

    private void checkBankAccount() {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.check_bank_account));
        dialog.show();

        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.isUserBankActive(new ResultListenerBooleanTwo() {
            @Override
            public void onResult(boolean x) {
                dialog.dismiss();

                if (!x) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(R.string.warning);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.you_cannot_create_new_item);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            FragManager.get().pop();
                        }
                    });

                    builder.show();
                }
            }
        });
    }

    public void setAddresses(ArrayList<String> list) {
        this.addresses.clear();
        this.addresses = list;
    }

    private boolean checkDataCompletion() {
        if (name.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.please_enter_item_name, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (desc.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.please_enter_item_description, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (addresses.size() == 0) {
            Toast.makeText(getContext(), R.string.please_add_at_least_one_photo, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void requestPermission(OnFinish onfish) {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);
        } else {
            // permission is granted
            onfish.onFinish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromStorage();
            }
        }
    }

    private void getImageFromStorage() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, CAMERA_ID);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_ID && resultCode == Activity.RESULT_OK) {
            File imageFile = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
            FragManager.get().setFragment(PhotosListFragment.newInstance(imageFile.getAbsolutePath()));
            //openPhotosListFragment(imageFile.getAbsolutePath());
        } else if (requestCode == GALLERY_ID && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getContext().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 4;
            o.inPreferredConfig = Bitmap.Config.RGB_565;

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            cursor.close();

            //  openPhotosListFragment(imgDecodableString);
            FragManager.get().setFragment(PhotosListFragment.newInstance(imgDecodableString));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.step_one));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).setToggleState(true);

        if (addresses.size() > 0) {
            File f = new File(addresses.get(0));
            if (f.exists()) {
                CreateBitmap cc = new CreateBitmap(addresses.get(0));
                cc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                fillMainImageWithGlide(addresses.get(0));
            }
        }
        ((MainActivity) getContext()).selectItemInMenu(2);
    }

    private void fillMainImageWithGlide(String s) {
        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                photos.setImageBitmap(resource);
            }
        };

        Glide.with(getContext()).load(s).asBitmap().into(target);
    }

    private interface OnFinish {
        void onFinish();
    }

    private class CreateBitmap extends AsyncTask<Bitmap, Void, Bitmap> {

        String address;
        private int screen_width;

        public CreateBitmap(String address) {
            this.address = address;

            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screen_width = metrics.widthPixels;
        }

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            return Utilities.decodeSampledBitmapFromResource(address, screen_width, 20);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(App.TAG, "CreateBitmap, onPostExecute: " + bitmap.getWidth() + ", " + bitmap.getHeight());
            ImageView add_photos = (ImageView) rootView.findViewById(R.id.photos);
            add_photos.setImageBitmap(bitmap);
        }
    }
}