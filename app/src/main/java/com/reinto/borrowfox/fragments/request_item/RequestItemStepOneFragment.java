/*
 *
 * RequestItemStepOne.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 30.6.16 13:36
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.request_item;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reinto.borrowfox.Models.CalendaDayInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CalendarCreateItem;
import com.reinto.borrowfox.managers.RequestItemManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RequestItemStepOneFragment extends Fragment {

    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String MIN_DAY = "min_day";
    public static final String USER_URL = "user_url";
    public static final String LENDER_ID = "lender_id";
    public static final String USER_NAME = "user_name";
    public static final String ITEM_ID = "item_id";
    public static final String PRICE_DAY = "price_day";
    public static final String EXTRA_DAY = "extra_day";
    public static final String DEPOSIT = "deposit";
    public static final String API = "api";

    public Long start, end;
    public int minimum_rental_period = 0;
    public int extra_days = 0;
    public int deposit = 0;
    public int price_per_day;
    public int extra_day_price;
    public String user_url;
    public int lend_user_id;
    public int item_id;
    public String lend_user_name;

    private View rootView;
    private int number_of_days = 0;
    private String date_from_n;
    private String date_to_n;

    public static RequestItemStepOneFragment newInstance() {
        return new RequestItemStepOneFragment();
    }

    public static RequestItemStepOneFragment newInstance(long from, long to, int priceDay, int extraDay,
                                                         int deposit, int minDay, String userUrl,
                                                         int lenderId, String userName, int itemId, ArrayList<CalendaDayInfo> list) {
        Bundle args = new Bundle();
        args.putLong(FROM, from);
        args.putLong(TO, to);
        args.putInt(PRICE_DAY, priceDay);
        args.putInt(EXTRA_DAY, extraDay);
        args.putInt(DEPOSIT, deposit);
        args.putInt(MIN_DAY, minDay);
        args.putString(USER_URL, userUrl);
        args.putInt(LENDER_ID, lenderId);
        args.putInt(ITEM_ID, itemId);
        args.putString(USER_NAME, userName);
        args.putParcelableArrayList(API, list);
        RequestItemStepOneFragment fragment = new RequestItemStepOneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        start = getArguments().getLong(FROM);
        end = getArguments().getLong(TO);
        minimum_rental_period = getArguments().getInt(MIN_DAY);
        user_url = getArguments().getString(USER_URL);
        lend_user_id = getArguments().getInt(LENDER_ID);
        lend_user_name = getArguments().getString(USER_NAME);
        item_id = getArguments().getInt(ITEM_ID);
        deposit = getArguments().getInt(DEPOSIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_request_item_part_one, container, false);

        RequestItemManager.get().getItem().beginDate = start;
        RequestItemManager.get().getItem().endDate = end;
        RequestItemManager.get().getItem().itemId = item_id;

        setupCalendar();
        setupTextViews();

        return rootView;
    }

    private void setupCalendar() {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(getArguments().getLong(FROM));

        date_from_n = getResources().getStringArray(R.array.month_short)[start.get(Calendar.MONTH)] + "\n" + Utilities.addZero(start.get(Calendar.DAY_OF_MONTH));

        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(getArguments().getLong(TO));

        date_to_n = getResources().getStringArray(R.array.month_short)[end.get(Calendar.MONTH)] + "\n" + Utilities.addZero(end.get(Calendar.DAY_OF_MONTH));

        CalendarCreateItem calendar = (CalendarCreateItem) rootView.findViewById(R.id.calendar);
        List<CalendaDayInfo> api_data = getArguments().getParcelableArrayList(API);
        calendar.showApiData = true;

        List<Long> sele = new ArrayList<>();

        for (long i = start.getTimeInMillis(); i <= end.getTimeInMillis(); i += (1000 * 60 * 60 * 24)) {
            sele.add(i);
        }

        number_of_days = sele.size();
        extra_days = number_of_days - minimum_rental_period;

        calendar.importSelectedData(sele);
        calendar.importAPIData(api_data);
    }

    private void setupTextViews() {
        TextView price_per_day = (TextView) rootView.findViewById(R.id.per_day);
        String s1 = "£" + getArguments().getInt(PRICE_DAY) + " / Day";
        final SpannableStringBuilder span = new SpannableStringBuilder(s1);
        span.setSpan(new RelativeSizeSpan(0.75f), s1.length() - 6, s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(Typeface.BOLD), 0, s1.length() - 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        price_per_day.setText(span);

        TextView extra_day = (TextView) rootView.findViewById(R.id.extra_day);
        String s2 = "£" + getArguments().getInt(EXTRA_DAY) + " / Extra Day";
        final SpannableStringBuilder span2 = new SpannableStringBuilder(s2);
        span2.setSpan(new RelativeSizeSpan(0.75f), s2.length() - 12, s2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span2.setSpan(new StyleSpan(Typeface.BOLD), 0, s2.length() - 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        extra_day.setText(span2);

        TextView deposit = (TextView) rootView.findViewById(R.id.deposit);
        String s3 = "£" + getArguments().getInt(DEPOSIT) + " / Deposit";
        final SpannableStringBuilder span3 = new SpannableStringBuilder(s3);
        span3.setSpan(new RelativeSizeSpan(0.75f), s3.length() - 10, s3.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span3.setSpan(new StyleSpan(Typeface.BOLD), 0, s3.length() - 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        deposit.setText(span3);

        TextView total = (TextView) rootView.findViewById(R.id.total_info);
        // int total_price = getArguments().getInt(PRICE_DAY) * minimum_rental_period + getArguments().getInt(EXTRA_DAY) * (number_of_days - minimum_rental_period);
        int total_price = getArguments().getInt(PRICE_DAY) + getArguments().getInt(EXTRA_DAY) * (number_of_days - 1);

        this.price_per_day = getArguments().getInt(PRICE_DAY);
        this.extra_day_price = getArguments().getInt(EXTRA_DAY);

        if (number_of_days > 9) {
            String s4 = "TOTAL\n" + number_of_days + " DAYS\n" + "£" + total_price;
            final SpannableStringBuilder span4 = new SpannableStringBuilder(s4);
            span4.setSpan(new RelativeSizeSpan(0.55f), 0, 14, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            total.setText(span4);
        } else if (number_of_days == 1) {
            String s4 = "TOTAL\n" + number_of_days + " DAY\n" + "£" + total_price;
            final SpannableStringBuilder span4 = new SpannableStringBuilder(s4);
            span4.setSpan(new RelativeSizeSpan(0.55f), 0, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            total.setText(span4);
        } else {
            String s4 = "TOTAL\n" + number_of_days + " DAYS\n" + "£" + total_price;
            final SpannableStringBuilder span4 = new SpannableStringBuilder(s4);
            span4.setSpan(new RelativeSizeSpan(0.55f), 0, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            total.setText(span4);
        }

        TextView date_from = (TextView) rootView.findViewById(R.id.date_from);
        final SpannableStringBuilder span5 = new SpannableStringBuilder(date_from_n);
        span5.setSpan(new RelativeSizeSpan(1.8f), date_from_n.length() - 2, date_from_n.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        date_from.setText(span5);

        TextView date_to = (TextView) rootView.findViewById(R.id.date_to);
        final SpannableStringBuilder span6 = new SpannableStringBuilder(date_to_n);
        span6.setSpan(new RelativeSizeSpan(1.8f), date_to_n.length() - 2, date_to_n.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        date_to.setText(span6);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.request_item));
        Utilities.hideToolbarIcons(getContext());
    }
}