package com.reinto.borrowfox.fragments.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.BorrowAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.BorrowData;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.EndlessListView;
import com.reinto.borrowfox.Views.OnEndReachListener;
import com.reinto.borrowfox.managers.FragManager;

import java.util.ArrayList;
import java.util.List;

public class BorrowFragment extends Fragment {

    private View rootView;
    private Spinner spinner;
    private BorrowAdapter adapter;
    private boolean loading;
    private boolean shouldLoadNext = true;
    private boolean isLoadingViewVisible;
    private int selectedType = Emuns.BorrowItemType.BORROW_REQUESTED;

    public static BorrowFragment newInstance() {
        Bundle args = new Bundle();
        BorrowFragment fragment = new BorrowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new BorrowAdapter(getContext(), new ArrayList<BorrowData>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_borrow, container, false);

        spinner = (Spinner) rootView.findViewById(R.id.borrow_spinner);
        EndlessListView listView = (EndlessListView) rootView.findViewById(R.id.borrow_listView);
        listView.setEmptyView(rootView.findViewById(R.id.borrow_emptyView));
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setAdapter(adapter);
        listView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if(!loading && shouldLoadNext)
                    downloadCorrectItems(selectedType, false);
            }
        });

        setupUI();
        setupButtons();

        if (adapter.getCount() == 0) {
            showLoading();
            downloadCorrectItems(Emuns.BorrowItemType.BORROW_REQUESTED, true);
        } else {
            hideLoading();
        }

        return rootView;
    }

    private void downloadCorrectItems(int type, boolean reset) {
        loading = true;

        if(reset) {
            showLoading();
            adapter = new BorrowAdapter(getContext(), new ArrayList<BorrowData>());
        }

        final DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloaditemByBorrower(type, adapter.getCount(), new ResultListenerList<List<BorrowData>>() {

            @Override
            public void onNullResult(List<BorrowData> result) {
                hideLoading();
                loading = false;
                shouldLoadNext = true;
                Toast.makeText(getContext(), R.string.error_download_borrow_items, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmptyList(List<BorrowData> result) {
                hideLoading();
                loading = false;
                shouldLoadNext = false;
            }

            @Override
            public void onFilledList(List<BorrowData> result) {
                hideLoading();
                Log.d(App.TAG, "LendFragment, OnTaskDone: " + result.toString());
                downloadImages(result);

                loading = false;
            }
        });
    }

    private void showLoading(){
        if(!isLoadingViewVisible) {
            rootView.findViewById(R.id.borrow_clona).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.borrow_progressBar).setVisibility(View.VISIBLE);
        }
        isLoadingViewVisible = true;
    }

    private void hideLoading(){
        if(isLoadingViewVisible) {
            Utilities.dissapearView(rootView.findViewById(R.id.borrow_clona));
            Utilities.dissapearView(rootView.findViewById(R.id.borrow_progressBar));
        }
        isLoadingViewVisible = false;
    }

    private void downloadImages(final List<BorrowData> object) {
        final int[] temp = {0};

        for (int i = 0; i < object.size(); i++) {
            object.get(i).downloadImages(new BorrowData.OnMyDialogResult() {
                @Override
                public void finish() {
                    temp[0]++;
                    if (temp[0] == object.size()) {
                        adapter.addData(object);
                        hideLoading();
                    }
                }
            });
        }
    }

    private void setupButtons() {
        final Button lend = (Button) rootView.findViewById(R.id.borrow_lend);
        lend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragmentControl(LendFragment.class);
            }
        });
    }

    private void setupUI() {
        String[] objects = getResources().getStringArray(R.array.my_borrow_options);

        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey_two, objects);
        adapter.setDropDownViewResource(R.layout.layout_custom_spinner_grey_two);
        spinner.setAdapter(adapter);
        spinner.setSelection(0, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        selectedType = Emuns.BorrowItemType.BORROW_REQUESTED;
                        break;
                    case 1:
                        selectedType = Emuns.BorrowItemType.BORROW_BORROWING;
                        break;
                    case 2:
                        selectedType = Emuns.BorrowItemType.BORROW_BOOKED;
                        break;
                    case 3:
                        selectedType = Emuns.BorrowItemType.BORROW_PREV_BORROWED;
                        break;
                }

                downloadCorrectItems(selectedType, true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.borrowfox));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(3);
    }
}
