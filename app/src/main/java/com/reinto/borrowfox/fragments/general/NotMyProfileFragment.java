/*
 *
 * NotMyProfileFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 30.6.16 12:11
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.reinto.borrowfox.Adapters.RecyclerViewAdapter;
import com.reinto.borrowfox.CustomListeners.RecyclerItemClickListener;
import com.reinto.borrowfox.Models.ItemData;
import com.reinto.borrowfox.Models.SingleCategoryItemModel;
import com.reinto.borrowfox.Models.User;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.EndlessListView;
import com.reinto.borrowfox.Views.EndlessRecyclerView;
import com.reinto.borrowfox.Views.OnEndReachListener;
import com.reinto.borrowfox.Views.StartRating;
import com.reinto.borrowfox.managers.FragManager;

import java.util.ArrayList;
import java.util.List;

public class NotMyProfileFragment extends Fragment {

    public static final String LENDER_ID = "lender_id";
    public static final String TRANSLATION_Y = "translationY";
    public static final String ITEM_ID = "item_id";

    private int lenderId;
    private View rootView;
    private User user;
    private RecyclerViewAdapter adapter;
    private boolean loading;
    private boolean shouldLoadNext = true;
    private ArrayList<SingleCategoryItemModel> itemsToAdd;


    public static NotMyProfileFragment newInstance(int lenderId) {
        Bundle args = new Bundle();
        args.putInt(LENDER_ID, lenderId);
        NotMyProfileFragment fragment = new NotMyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lenderId = getArguments().getInt(LENDER_ID);
        itemsToAdd = new ArrayList<>();
        adapter = new RecyclerViewAdapter(getContext(), new ArrayList<SingleCategoryItemModel>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_hisher_profile, container, false);

        setupListView();

        if (user == null) {
            final DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
            downloadAPIService.downloadUserData(lenderId, new TaskDoneListener<User>() {
                @Override
                public void OnTaskDone(User user) {

                    if (user == null) {
                        Toast.makeText(getContext(), R.string.error_downloading_user_info, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    NotMyProfileFragment.this.user = user;

                    setupUserInfo(user);

                    // Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_progressBar));
                    Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_clona));

                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int height = size.y;
                    RelativeLayout parent = (RelativeLayout) rootView.findViewById(R.id.fragment_hisher_profile_relative);
                    ObjectAnimator anim = ObjectAnimator.ofFloat(rootView.findViewById(R.id.fragment_hisher_profile_progressBar), TRANSLATION_Y, 0, (height / 2) - (height - parent.getHeight()) / 2);
                    anim.setDuration(500);
                    anim.start();

                    downloadItems(user.getId());
                }
            });
        } else {
            setupUserInfo(user);
            Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_progressBar));
            Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_clona));
            Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_clona2));
        }

        return rootView;
    }

    private void downloadItems(int id) {
        loading = true;
        Bootstrapper.getDownloadAPIService().downloadLendItems(id, adapter.getItemCount(), new ResultListenerList<List<ItemData>>() {

            @Override
            public void onNullResult(List<ItemData> result) {
                loading = false;
                shouldLoadNext = true;
                Toast.makeText(getContext(), R.string.error_download_user_items, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmptyList(List<ItemData> result) {
                loading = false;
                shouldLoadNext = false;
            }

            @Override
            public void onFilledList(List<ItemData> result) {
                loading = false;
                shouldLoadNext = result.size() != 0;
                downloadImages(result);
            }
        });
    }

    private void downloadImages(final List<ItemData> objecto) {
        for (int i = 0; i < objecto.size(); i++) {

            final int finalI = i;
            objecto.get(i).downloadImages(new ItemData.OnMyDialogResult() {
                @Override
                public void finish() {
                    setupObjects(objecto, finalI);
                }
            });
        }
    }

    private void setupListView() {
        EndlessRecyclerView recyclerView = (EndlessRecyclerView) rootView.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FragManager.get().setFragment(ItemDetailFragment.newInstance( adapter.getItem(position).getItem_id(), false));
            }
        }));

        recyclerView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if(!loading && shouldLoadNext)
                    downloadItems(user.getId());
            }
        });
    }

    private void setupObjects(List<ItemData> objecto, int i) {
        final SingleCategoryItemModel x = new SingleCategoryItemModel(
                objecto.get(i).getId(),
                String.valueOf(objecto.get(i).getPrice_per_day()),
                objecto.get(i).getName(),
                objecto.get(i).getUser_locality(),
                objecto.get(i).getImages(),
                user.getUserImage());

        itemsToAdd.add(x);

        if (objecto.size() == (i + 1)) {
            if(adapter.getItemCount() == 0) {
                Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_progressBar));
                Utilities.dissapearView(rootView.findViewById(R.id.fragment_hisher_profile_clona2));
            }

            adapter.addData(itemsToAdd);
            itemsToAdd.clear();
        }
    }

    private void setupUserInfo(User user) {
        Utilities.setToolbarTitle(getContext(), user.getFirstName() + getString(R.string.s_profile));

        TextView location = (TextView) rootView.findViewById(R.id.hisher_profile_location);
        String loc = user.getLocation();
        if (loc.equals("")) {
            location.setText(R.string.location_unknown);
        } else {
            location.setText(loc.replace(", ", "\n"));
        }

        StartRating rating = (StartRating) rootView.findViewById(R.id.fragment_hisher_profile_rating);
        rating.setFilledStart(user.getRating());

        TextView aboutMe = (TextView) rootView.findViewById(R.id.fragment_hisher_profile_aboutme);
        aboutMe.setText(user.getAboutMe());

        TextView title = (TextView) rootView.findViewById(R.id.fragment_hisher_profile_title);
        title.setText(user.getFirstName() + getString(R.string.s_items));

        ImageView profileImage = (ImageView) rootView.findViewById(R.id.hisher_profile_profile_image);
        Glide.with(getContext()).load(APIService.URL_BASE_DATA +
                user.getUserImage()).placeholder(R.drawable.profile_default).centerCrop().into(profileImage);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.hideToolbarIcons(getContext());
    }
}
