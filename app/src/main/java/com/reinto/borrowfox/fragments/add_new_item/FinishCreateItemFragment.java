/*
 *
 * FinishCreateItemFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 15:49
 * Copyright (c) year Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.add_new_item;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.NewItemInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.io.File;

public class FinishCreateItemFragment extends Fragment {

    private View rootView;

    public static FinishCreateItemFragment newInstance() {
        Bundle args = new Bundle();
        FinishCreateItemFragment fragment = new FinishCreateItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_item_finish, container, false);

        final NewItemInfo newItem = ((MainActivity) getContext()).newItem;

        Log.d(App.TAG, "FinishCreateItemFragment, onCreateView: " + newItem);

        TextView price = (TextView) rootView.findViewById(R.id.item_available_price);
        String s1 = getString(R.string.pound_symbol) + newItem.getPrice_per_day() + getContext().getString(R.string.slash_day);
        final SpannableStringBuilder span = new SpannableStringBuilder(s1);
        span.setSpan(new RelativeSizeSpan(0.6f), s1.length() - 6, s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(Typeface.BOLD), 0, s1.length() - 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        price.setText(span);

        TextView name = (TextView) rootView.findViewById(R.id.name);
        name.setText(newItem.getItem_name());

        TextView address = (TextView) rootView.findViewById(R.id.location);
        address.setText(Utilities.getUserLocation(getContext()));

        ImageView image = (ImageView) rootView.findViewById(R.id.imageView9);
        CreateBitmap createBitmap = new CreateBitmap(newItem.getAddresses().get(0), image);
        createBitmap.execute();

        Button close = (Button) rootView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().pop(HomeFragment.class.getName());
            }
        });

        View.OnClickListener onclick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = "image/*";
                // String filename = newItem.getAddresses().get(0);
                String mediaPath = newItem.getAddresses().get(0);

                // Create the new Intent using the 'Send' action.
                Intent share = new Intent(Intent.ACTION_SEND);

                // Set the MIME type
                share.setType(type);

                // Create the URI from the media
                File media = new File(mediaPath);
                Uri uri = Uri.fromFile(media);

                // Add the URI to the Intent.
                share.putExtra(Intent.EXTRA_STREAM, uri);

                // Broadcast the Intent.
                startActivity(Intent.createChooser(share, "Share to"));
            }
        };

        ImageButton instagram = (ImageButton) rootView.findViewById(R.id.instagram_icon);
        ImageButton facebook = (ImageButton) rootView.findViewById(R.id.facebook_icon);
        ImageButton googleplus = (ImageButton) rootView.findViewById(R.id.googleplus_icon);
        ImageButton twitter = (ImageButton) rootView.findViewById(R.id.twitter_icon);
        instagram.setOnClickListener(onclick);
        facebook.setOnClickListener(onclick);
        googleplus.setOnClickListener(onclick);
        twitter.setOnClickListener(onclick);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.add_item_complete));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(2);
    }

    private class CreateBitmap extends AsyncTask<Bitmap, Void, Bitmap> {

        private String address;
        private ImageView image;

        public CreateBitmap(String address, ImageView image) {
            this.address = address;
            this.image = image;
        }

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 4;
            o.inPreferredConfig = Bitmap.Config.RGB_565;

            return BitmapFactory.decodeFile(address, o);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            rootView.findViewById(R.id.progresBar).setVisibility(View.GONE);
            image.setImageBitmap(bitmap);
        }
    }
}