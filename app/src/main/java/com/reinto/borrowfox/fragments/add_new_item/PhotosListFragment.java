/*
 *
 * PhotosListFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 27.6.16 12:25
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.add_new_item;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.DragableView;
import com.reinto.borrowfox.Views.DragableViewBig;
import com.reinto.borrowfox.fragments.general.EditItemFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class PhotosListFragment extends Fragment implements View.OnDragListener {

    public static final String ADDRESSES = "addresses";
    public static final String ADDRESS = "address";
    public int numberOfPhotos = 0;
    public RelativeLayout parent;
    private View rootView;
    private ArrayList<DragableView> dragableViews;
    private Calendar c;
    private boolean editMode = false;
    private DragableViewBig firstImage;

    private ArrayList<String> imageAddreses;

    public static PhotosListFragment newInstance(ArrayList<String> address) {
        Bundle args = new Bundle();
        args.putStringArrayList(ADDRESSES, address);
        PhotosListFragment fragment = new PhotosListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static PhotosListFragment newInstance(String address) {
        Bundle args = new Bundle();
        args.putString(ADDRESS, address);
        PhotosListFragment fragment = new PhotosListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_item_add_photo, container, false);

        dragableViews = new ArrayList<>();
        imageAddreses = new ArrayList<>();
        parent = (RelativeLayout) rootView.findViewById(R.id.parent);
        firstImage = (DragableViewBig) rootView.findViewById(R.id.first_image);

        collectInteractiveViews();

        if (getArguments().getStringArrayList(ADDRESSES) != null) {
            // user already added some photos
            imageAddreses.addAll(getArguments().getStringArrayList(ADDRESSES));

            File f = new File(imageAddreses.get(0));
            if (f.exists()) {
                CreateFirstBitmap cc = new CreateFirstBitmap(imageAddreses.get(0));
                cc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                firstImage.address = imageAddreses.get(0);
            } else {
                fillMainImageWithGlide(imageAddreses.get(0));
                firstImage.address = imageAddreses.get(0);
            }

            for (int i = 1; i < imageAddreses.size(); i++) {
                File ft = new File(imageAddreses.get(i));
                if (ft.exists()) {
                    CreateBitmap cc = new CreateBitmap(imageAddreses.get(i));
                    cc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    fillWithGlide(imageAddreses.get(i));
                }
            }
        } else {
            // user did not add photos before
            String address = getArguments().getString(ADDRESS);
            imageAddreses.add(address);

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 4;
            o.inPreferredConfig = Bitmap.Config.RGB_565;

            Bitmap bitmap = BitmapFactory.decodeFile(address, o);
            firstImage.background.setImageBitmap(bitmap);
            firstImage.address = address;
        }

        Button add_new_photo = (Button) rootView.findViewById(R.id.add_photos);
        add_new_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (numberOfPhotos >= 6) {
                    Toast.makeText(getContext(), R.string.max_photos, Toast.LENGTH_SHORT).show();
                    return;
                }

                final CharSequence[] items = {getString(R.string.take_new_photo), getString(R.string.pick_image_from_gallery),
                        getString(R.string.cancel)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.add_item_photo));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(getString(R.string.take_new_photo))) {
                            requestPermission(new OnFinish() {
                                @Override
                                public void onFinish() {
                                    getImageFromStorage();
                                }
                            });
                        } else if (items[item].equals(getString(R.string.pick_image_from_gallery))) {
                            requestPermission(new OnFinish() {
                                @Override
                                public void onFinish() {
                                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, 200);
                                }
                            });
                        } else if (items[item].equals(getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

        final Button editPhotos = (Button) rootView.findViewById(R.id.create_item_add_photo_edit_photos);
        editPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode = !editMode;
                if (editMode) {
                    for (int i = 0; i < dragableViews.size(); i++) {
                        dragableViews.get(i).setupEditMode(true);
                    }
                    setupListeners();
                    rootView.findViewById(R.id.tip).setVisibility(View.VISIBLE);
                    editPhotos.setText(R.string.save);
                    Utilities.setToolbarTitle(getContext(), getString(R.string.edit_photoss));
                } else {
                    for (int i = 0; i < dragableViews.size(); i++) {
                        dragableViews.get(i).setupEditMode(false);
                    }
                    cleanAllListeners();
                    rootView.findViewById(R.id.tip).setVisibility(View.GONE);
                    editPhotos.setText(getString(R.string.edit_photoss));
                    Utilities.setToolbarTitle(getContext(), getString(R.string.add_photoss));

                }
            }
        });

        return rootView;
    }

    private void collectInteractiveViews() {
        DragableView d1 = (DragableView) rootView.findViewById(R.id.image1);
        DragableView d2 = (DragableView) rootView.findViewById(R.id.image2);
        DragableView d3 = (DragableView) rootView.findViewById(R.id.image3);
        DragableView d4 = (DragableView) rootView.findViewById(R.id.image4);
        DragableView d5 = (DragableView) rootView.findViewById(R.id.image5);
        DragableView d6 = (DragableView) rootView.findViewById(R.id.image6);

        dragableViews.add(d1);
        dragableViews.add(d2);
        dragableViews.add(d3);
        dragableViews.add(d4);
        dragableViews.add(d5);
        dragableViews.add(d6);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.add_photoss));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).setToggleState(false);
        ((MainActivity) getContext()).selectItemInMenu(2);
    }

    private void requestPermission(OnFinish onfish) {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    40);
        } else {
            // permission is granted
            onfish.onFinish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 40) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromStorage();
            }
        }
    }

    private void getImageFromStorage() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        c = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img" + c.getTimeInMillis() + ".jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, 150);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 150 && resultCode == Activity.RESULT_OK) {
            File imageFile = new File(Environment.getExternalStorageDirectory() + File.separator + "img" + c.getTimeInMillis() + ".jpg");

            imageAddreses.add(imageFile.getAbsolutePath());
            CreateBitmap c = new CreateBitmap(imageFile.getAbsolutePath());
            c.execute();
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (null != selectedImageUri) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContext().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                imageAddreses.add(picturePath);
                CreateBitmap c = new CreateBitmap(picturePath);
                c.execute();
            }
        }
    }

    private void cleanAllListeners() {
        for (int i = 0; i < dragableViews.size(); i++) {
            dragableViews.get(i).setOnDragListener(null);
            dragableViews.get(i).setOnLongClickListener(null);
        }

        firstImage.setOnDragListener(null);
    }

    private void setupListeners() {
        MyLongTouchListener myTouchListener = new MyLongTouchListener();
        MyDragEventListener myDragEventListener = new MyDragEventListener();

        for (int i = 0; i < dragableViews.size(); i++) {
            dragableViews.get(i).setOnDragListener(myDragEventListener);
            dragableViews.get(i).setOnLongClickListener(myTouchListener);
        }

        firstImage.setOnDragListener(myDragEventListener);
    }

    public void setOrder() {

        ArrayList<Bitmap> nonEmpty = new ArrayList<>();
        ArrayList<String> addresses = new ArrayList<>();

        for (int i = 0; i < dragableViews.size(); i++) {
            if (dragableViews.get(i).getVisibility() != View.INVISIBLE) {
                nonEmpty.add(((BitmapDrawable) dragableViews.get(i).background.getDrawable()).getBitmap());
                addresses.add(dragableViews.get(i).address);
            }
        }

        for (int i = 0; i < nonEmpty.size(); i++) {
            dragableViews.get(i).background.setImageBitmap(nonEmpty.get(i));
            dragableViews.get(i).setVisibility(View.VISIBLE);
            dragableViews.get(i).address = addresses.get(i);
        }

        for (int i = nonEmpty.size(); i < 6; i++) {
            dragableViews.get(i).background.setImageBitmap(null);
            dragableViews.get(i).setVisibility(View.INVISIBLE);
        }
    }

    public void putBitmapBack(Bitmap b) {
        for (int i = 0; i < dragableViews.size(); i++) {
            if (dragableViews.get(i).getVisibility() == View.INVISIBLE) {
                dragableViews.get(i).background.setImageBitmap(b);
                dragableViews.get(i).setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Fragment f = ((MainActivity) getContext()).getSupportFragmentManager().findFragmentByTag(FirstStepFragment.class.getName());

        if (f != null) {
            ArrayList<String> newOrder = new ArrayList<>();

            newOrder.add(firstImage.address);

            for (int i = 0; i < dragableViews.size(); i++) {
                if (dragableViews.get(i).address != null) {
                    newOrder.add(dragableViews.get(i).address);
                }
            }

            ((FirstStepFragment) f).setAddresses(newOrder);

            return;
        }

        Fragment f2 = ((MainActivity) getContext()).getSupportFragmentManager().findFragmentByTag(EditItemFragment.class.getName());
        if (f2 != null) {

            ArrayList<String> newOrder = new ArrayList<>();

            newOrder.add(firstImage.address);

            for (int i = 0; i < dragableViews.size(); i++) {
                if (dragableViews.get(i).address != null) {
                    newOrder.add(dragableViews.get(i).address);
                }
            }

            ((EditItemFragment) f2).setAddresses(newOrder);
        }

        ((MainActivity) getContext()).setToggleState(true);
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        return false;
    }

    private void fillMainImageWithGlide(final String s) {
        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                firstImage = (DragableViewBig) rootView.findViewById(R.id.first_image);
                firstImage.background.setImageBitmap(resource);
            }
        };

        Glide.with(getContext()).load(s).asBitmap().into(target);
    }

    private void fillWithGlide(final String addresses) {
        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                dragableViews.get(numberOfPhotos).address = addresses;
                dragableViews.get(numberOfPhotos).setBitmap(resource);
                dragableViews.get(numberOfPhotos).setVisibility(View.VISIBLE);
                numberOfPhotos++;
            }
        };

        Glide.with(getContext()).load(addresses).asBitmap().into(target);
    }

    private interface OnFinish {
        void onFinish();
    }

    private final class MyLongTouchListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View view) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            return true;
        }
    }

    protected class MyDragEventListener implements View.OnDragListener {

        public boolean onDrag(View v, DragEvent event) {

            final int action = event.getAction();
            View draggingView = (View) event.getLocalState();

            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;

                case DragEvent.ACTION_DRAG_ENTERED:
                    if (v == draggingView) {
                        return false;
                    }

                    if (v instanceof DragableView) {
                        Bitmap bitmap = ((BitmapDrawable) ((DragableView) v).background.getDrawable()).getBitmap();
                        Bitmap bitmap2 = ((BitmapDrawable) ((DragableView) draggingView).background.getDrawable()).getBitmap();

                        String a1 = ((DragableView) v).address;
                        String a2 = ((DragableView) draggingView).address;


                        ((DragableView) v).background.setImageBitmap(bitmap2);
                        ((DragableView) draggingView).background.setImageBitmap(bitmap);

                        ((DragableView) v).address = a2;
                        ((DragableView) draggingView).address = a1;
                    } else {
                        Bitmap bitmap = ((BitmapDrawable) ((DragableViewBig) v).background.getDrawable()).getBitmap();
                        Bitmap bitmap2 = ((BitmapDrawable) ((DragableView) draggingView).background.getDrawable()).getBitmap();

                        String a1 = ((DragableViewBig) v).address;
                        String a2 = ((DragableView) draggingView).address;


                        ((DragableViewBig) v).background.setImageBitmap(bitmap2);
                        ((DragableView) draggingView).background.setImageBitmap(bitmap);

                        ((DragableViewBig) v).address = a2;
                        ((DragableView) draggingView).address = a1;
                    }

                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                    break;
            }

            return true;
        }
    }

    private class CreateBitmap extends AsyncTask<Void, Void, Bitmap> {

        String address;
        private int screenWidth;

        public CreateBitmap(String address) {
            this.address = address;

            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            return Utilities.decodeSampledBitmapFromResource(address, screenWidth / 3, 20);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(App.TAG, "CreateFirstBitmap, onPostExecute: " + bitmap.getWidth() + ", " + bitmap.getHeight());

            dragableViews.get(numberOfPhotos).address = address;
            dragableViews.get(numberOfPhotos).setBitmap(bitmap);
            dragableViews.get(numberOfPhotos).setVisibility(View.VISIBLE);
            numberOfPhotos++;
        }
    }

    private class CreateFirstBitmap extends AsyncTask<Void, Void, Bitmap> {

        String address;
        private int screenWidth;

        public CreateFirstBitmap(String address) {
            this.address = address;
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            return Utilities.decodeSampledBitmapFromResource(address, screenWidth, 20);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(App.TAG, "CreateFirstBitmap, onPostExecute: " + bitmap.getWidth() + ", " + bitmap.getHeight());
            firstImage = (DragableViewBig) rootView.findViewById(R.id.first_image);
            firstImage.background.setImageBitmap(bitmap);
        }
    }
}