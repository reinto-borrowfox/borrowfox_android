package com.reinto.borrowfox.fragments.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.ChatConversationAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.ChatMessage;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.MessagesDownloadedListener;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChatFragment extends Fragment {

    public static final String LENDER_ID = "lender_id";
    public static final String ITEM_ID = "item_id";
    public static final String USER_NAME = "user_name";
    private View rootView;
    public int chatId;
    private List<ChatMessage> list;
    private ListView listView;
    private ChatConversationAdapter adapter;


    public static ChatFragment newInstance(int lenderId, int itemId, String userName) {
        Bundle args = new Bundle();
        args.putInt(LENDER_ID, lenderId);
        args.putInt(ITEM_ID, itemId);
        args.putString(USER_NAME, userName);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        ((MainActivity) getContext()).fragment = ChatFragment.class;

        Log.d(App.TAG, "ChatFragment, onCreateView: " + getArguments().getInt(LENDER_ID) + ", " + getArguments().getInt(ITEM_ID) + ", " + getArguments().getString(USER_NAME));


        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.openChatAndGetMessages(Utilities.getUserId(getContext()),
                getArguments().getInt(LENDER_ID),
                getArguments().getInt(ITEM_ID),
                0, new MessagesDownloadedListener<List<ChatMessage>>() {
                    @Override
                    public void OnTaskDone(List<ChatMessage> object, int chat_id) {
                        if (object == null) {
                            Toast.makeText(getContext(), R.string.error_downloading_messages, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Log.d(App.TAG, "ChatFragment, OnTaskDone: " + object.toString());

                        ChatFragment.this.chatId = chat_id;
                        Log.d(App.TAG, "ChatFragment, onCreateView: " + chatId);

                        setupMessages(object);
                    }
                });

        return rootView;
    }

    private void setupMessages(List<ChatMessage> object) {
        list = new ArrayList<>(object);

        if (list.size() > 0) {
            if (list.get(0).getType() != 2) {
                list.add(0, new ChatMessage(2, list.get(0).getTime(), null, getCorrectTimeStringLong(list.get(0).getTime()), chatId));
            }
        }

        listView = (ListView) rootView.findViewById(R.id.chat_listview);
        adapter = new ChatConversationAdapter(getContext(), list, getArguments().getString(USER_NAME));
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setAdapter(adapter);

        final EditText text = (EditText) rootView.findViewById(R.id.chat_message);
        final ImageButton send = (ImageButton) rootView.findViewById(R.id.chat_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send.setVisibility(View.INVISIBLE);
                rootView.findViewById(R.id.chat_progressBar).setVisibility(View.VISIBLE);

                if (text.getText().toString().length() > 0) {
                    DownloadAPIService down = Bootstrapper.getDownloadAPIService();
                    down.sendChatMessage(chatId, text.getText().toString(), new TaskDoneListener<Boolean>() {
                        @Override
                        public void OnTaskDone(Boolean object) {
                            send.setVisibility(View.VISIBLE);
                            rootView.findViewById(R.id.chat_progressBar).setVisibility(View.INVISIBLE);

                            if (object) {
                                // success
                                list.add(new ChatMessage(0, Calendar.getInstance().getTimeInMillis(), text.getText().toString(), null, chatId));
                                adapter.notifyDataSetChanged();
                                text.setText("");
                                text.clearFocus();
                                listView.smoothScrollToPosition(listView.getCount() - 1);
                            } else {
                                Toast.makeText(getContext(), R.string.error_sending_message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(getContext(), R.string.nothing_to_send, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setMessage(String text) {
        list.add(new ChatMessage(1, Calendar.getInstance().getTimeInMillis(), text, null, chatId));
        adapter.notifyDataSetChanged();
        listView.smoothScrollToPosition(listView.getCount() - 1);
    }

    private String getCorrectTimeStringLong(long x) {
        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(x);
        Calendar c = Calendar.getInstance();

        // today case
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return getString(R.string.today) + printTime(c2);
                }
            }
        }

        // yesterday case
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) == (c2.get(Calendar.DAY_OF_MONTH) + 1)) {
                    return getString(R.string.yesterday) + printTime(c2);
                }
            }
        }

        // days from last week until week ago
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) > c2.get(Calendar.DAY_OF_MONTH) + 1 && c.get(Calendar.DAY_OF_MONTH) < c2.get(Calendar.DAY_OF_MONTH) + 7) {
                    int temp = c2.get(Calendar.DAY_OF_WEEK);
                    String day = getResources().getStringArray(R.array.days_in_week)[temp - 1];
                    return day + " " + printTime(c2);
                }
            }
        }

        String day = getResources().getStringArray(R.array.days_in_week)[c2.get(Calendar.DAY_OF_WEEK) - 1];
        return day + ", " + c2.get(Calendar.DAY_OF_MONTH) + "/" + (c2.get(Calendar.MONTH) + 1) + "/" + c2.get(Calendar.YEAR) +
                " " + c2.get(Calendar.HOUR_OF_DAY) + ":" + c2.get(Calendar.MINUTE);
    }

    private String printTime(Calendar c) {
        return c.get(Calendar.HOUR_OF_DAY) + ":" + fixMinutes(c.get(Calendar.MINUTE));
    }

    private String fixMinutes(int x) {
        if (x < 10) {
            return "0" + String.valueOf(x);
        } else {
            return String.valueOf(x);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getArguments().getString(USER_NAME));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(5);
    }
}