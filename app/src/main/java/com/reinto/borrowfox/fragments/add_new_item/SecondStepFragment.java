package com.reinto.borrowfox.fragments.add_new_item;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomDiscountSeekBar;
import com.reinto.borrowfox.Views.MinimalPeriodSeekBar;
import com.reinto.borrowfox.managers.FragManager;

public class SecondStepFragment extends Fragment {

    private EditText pricePerDay;
    private EditText deposit;
    private Spinner collection;
    private CustomDiscountSeekBar customDiscountSeekBar;
    private MinimalPeriodSeekBar minimalPeriodSeekBar;

    public static SecondStepFragment newInstance() {
        Bundle args = new Bundle();

        SecondStepFragment fragment = new SecondStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_item_second_step, container, false);

        Log.d(App.TAG, "SecondStepFragment, onCreateView: " + ((MainActivity)getContext()).newItem.toString());

        customDiscountSeekBar = (CustomDiscountSeekBar) rootView.findViewById(R.id.view4);
        minimalPeriodSeekBar = (MinimalPeriodSeekBar) rootView.findViewById(R.id.min_rental_view);

        try {
            customDiscountSeekBar.setPercent(((MainActivity) getContext()).newItem.getPercentStepTwo());
            customDiscountSeekBar.setMoney(((MainActivity) getContext()).newItem.getAdditional_day_discount());
        } catch (NullPointerException e) {
            // not filled before
        }

        try {
            if (((MainActivity) getContext()).newItem.getMinimim_day_rental() != 0) {
                minimalPeriodSeekBar.setValue(((MainActivity) getContext()).newItem.getMinimim_day_rental());
            }
        } catch (NullPointerException e) {
            // not filled before
        }


        deposit = (EditText) rootView.findViewById(R.id.deposit);
        try {
            int x = ((MainActivity) getContext()).newItem.getDeposit();
            if (x == 0) {
            } else {
                deposit.setText(String.valueOf(((MainActivity) getContext()).newItem.getDeposit()));
            }

        } catch (NullPointerException e) {
            // not filled before
        }

        String[] objects = {getString(R.string.allow_in_person_collection), getString(R.string.deny_in_person_collection)};
        collection = (Spinner) rootView.findViewById(R.id.item_collection);
        final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner, objects);
        collection.setAdapter(adapter);

        try {
            collection.setSelection(((MainActivity) getContext()).newItem.getItem_collection_and_return() - 1);
        } catch (NullPointerException e) {
            // not filled before
        }

        pricePerDay = (EditText) rootView.findViewById(R.id.price_per_day);

        try {
            int x = ((MainActivity) getContext()).newItem.getPrice_per_day();
            if (x == 0) {
            } else {
                pricePerDay.setText(String.valueOf(((MainActivity) getContext()).newItem.getPrice_per_day()));
            }

        } catch (NullPointerException e) {
            // not filled before
        }

        pricePerDay.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    int integer = Integer.valueOf(charSequence.toString());
                    customDiscountSeekBar.setMoney(integer);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        Button next = (Button) rootView.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkDataCompletion()) {
                    ((MainActivity) getContext()).newItem.setPrice_per_day(Integer.valueOf(pricePerDay.getText().toString()));
                    ((MainActivity) getContext()).newItem.setDeposit(Integer.valueOf(deposit.getText().toString()));
                    ((MainActivity) getContext()).newItem.setMinimim_day_rental(minimalPeriodSeekBar.getMinRentalPeriod());
                    ((MainActivity) getContext()).newItem.setAdditional_day_discount(customDiscountSeekBar.getDiscount());

                    if (collection.getSelectedItemPosition() == 0) {
                        ((MainActivity) getContext()).newItem.setItem_collection_and_return(1);
                    } else {
                        ((MainActivity) getContext()).newItem.setItem_collection_and_return(2);
                    }

                    //  ((MainActivity) getContext()).setupFragment(ThirdStepFragment.class);
                    FragManager.get().setFragment(ThirdStepFragment.newInstance());
                }
            }
        });

        Button back = (Button) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragment(FirstStepFragment.newInstance());
                // ((MainActivity) getContext()).setupFragment(FirstStepFragment.class);
            }
        });

        return rootView;
    }

    private boolean checkDataCompletion() {
        if (pricePerDay.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.please_enter_item_price_per_day, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (deposit.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.please_enter_deposit_value, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.step_two));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(2);
    }
}