package com.reinto.borrowfox.fragments.general;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.SimpleCategoryAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.SimpleCategoryPlusFox;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerIntegerArray;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSearchView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    public TextView tip;
    private View rootView;
    private ArrayList<SimpleCategoryPlusFox> list;
    private SimpleCategoryAdapter adapter;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        final ProgressBar p = (ProgressBar) rootView.findViewById(R.id.progressBaro);

        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();

        if (list == null) {
            downloadAPIService.getCategoriesUpdated(new TaskDoneListener<List<SimpleCategoryPlusFox>>() {
                @Override
                public void OnTaskDone(List<SimpleCategoryPlusFox> object) {
                    dissapearView(p);
                    if (object == null) {
                        return;
                    }

                    list = new ArrayList<>(object);
                    setupListView();
                }
            });
        } else {
            p.setVisibility(View.GONE);
            setupListView();
        }

        ((MainActivity) getContext()).customSearchView.setOnSearchIconClick(new CustomSearchView.OnSearchClick() {
            @Override
            public void onSeachClick(String text) {
                adapter.getFilter().filter(text);
            }
        });

        downloadAPIService.getChatAndNotifNumber(new ResultListenerIntegerArray() {
            @Override
            public void onResult(int chatNumber, int notificationNumber) {
                if (chatNumber != -1) {

                    LinearLayout parent = (LinearLayout) ((MainActivity) getContext()).navigationView.getMenu().getItem(5).getActionView();
                    TextView nu1 = (TextView) parent.findViewById(R.id.sidebar_chat);

                    if (chatNumber < 1) {
                        nu1.setVisibility(View.GONE);
                    } else {
                        nu1.setText(chatNumber > 0 ? String.valueOf(chatNumber) : null);
                    }

                    LinearLayout parent2 = (LinearLayout) ((MainActivity) getContext()).navigationView.getMenu().getItem(6).getActionView();
                    TextView nu2 = (TextView) parent2.findViewById(R.id.sidebar_chat);

                    if (notificationNumber < 1) {
                        nu2.setVisibility(View.GONE);
                    } else {
                        nu2.setText(notificationNumber > 0 ? String.valueOf(notificationNumber) : null);
                    }
                } else {
                    Log.d(App.TAG, "HomeFragment, onResult: chat number -1");
                }
            }
        });

        return rootView;
    }

    private void createNotification(){
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.putExtra("fragment", "chat");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext())
                .setContentTitle(getString(R.string.app_name))
                .setContentText("asdsad, ITEM REQUESTED")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private void setupListView() {
        adapter = new SimpleCategoryAdapter(getContext(), list);
        ListView listView = (ListView) rootView.findViewById(R.id.listview);
        listView.setAdapter(adapter);
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (tip != null) {
                    if (tip.getVisibility() == View.VISIBLE) {
                        dissapearView(tip);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            }
        });

        dissapearView(rootView.findViewById(R.id.clona));
    }

    private void dissapearView(final View view) {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(600);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        view.startAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.borrowfox));
        Utilities.showToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(0);
    }
}