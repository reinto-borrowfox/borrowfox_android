package com.reinto.borrowfox.fragments.general;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.ChatListAdapter;
import com.reinto.borrowfox.Models.ChatListItem;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.EndlessListView;
import com.reinto.borrowfox.Views.OnEndReachListener;

import java.util.ArrayList;
import java.util.List;

public class ChatListFragment extends Fragment {

    private View rootView;
    private ChatListAdapter adapter;
    private boolean loading;
    private boolean shouldLoadNext = true;

    public static ChatListFragment newInstance() {
        Bundle args = new Bundle();
        ChatListFragment fragment = new ChatListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ChatListAdapter(getContext(), new ArrayList<ChatListItem>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat_list, container, false);

        setupUI();

        if(adapter.getCount() == 0){
            loadData();
        } else {
            Utilities.dissapearView(rootView.findViewById(R.id.progressBaro));
            Utilities.dissapearView(rootView.findViewById(R.id.clona));
        }

        return rootView;
    }

    private void loadData(){
        loading = true;
        Bootstrapper.getDownloadAPIService().downloadChats(adapter.getCount(), new TaskDoneListener<List<ChatListItem>>() {
            @Override
            public void OnTaskDone(List<ChatListItem> object) {
                if(adapter.getCount() == 0) {
                    Utilities.dissapearView(rootView.findViewById(R.id.progressBaro));
                    Utilities.dissapearView(rootView.findViewById(R.id.clona));
                }

                if (object == null) {
                    loading = false;
                    Toast.makeText(getContext(), R.string.error_download_chat, Toast.LENGTH_SHORT).show();
                    return;
                }

                adapter.addData(object);

                loading = false;
                shouldLoadNext = object.size() != 0;
            }
        });
    }

    private void setupUI() {
        EndlessListView listView = (EndlessListView) rootView.findViewById(R.id.chat_list_listview);
        listView.setDivider(new ColorDrawable(ResourcesCompat.getColor(getResources(), R.color.divider, null)));
        listView.setDividerHeight(Utilities.convertDpToPixels(1, getContext()));
        listView.setAdapter(adapter);
        listView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if(!loading && shouldLoadNext)
                    loadData();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.messages));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(5);

    }
}
