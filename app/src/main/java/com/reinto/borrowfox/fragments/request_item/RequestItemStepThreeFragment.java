/*
 *
 * RequestItemStepThreeFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 12.7.16 10:08
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.request_item;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerBoolean;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.TopBarCustomView;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.fragments.general.SettingsFragment;
import com.reinto.borrowfox.managers.CardManager;
import com.reinto.borrowfox.managers.FragManager;
import com.reinto.borrowfox.managers.RequestItemManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RequestItemStepThreeFragment extends Fragment {

    private View rootView;
    private double deliveryPrice = 0.0;
    private int itemId;
    private CardManager manager;
    private Spinner spinner;
    private int deliveryType = 1;

    private String startDate, endDate;
    private long startDateLong, endDateLong;

    public static RequestItemStepThreeFragment newInstance() {
        Bundle args = new Bundle();

        RequestItemStepThreeFragment fragment = new RequestItemStepThreeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_request_item_step_three, container, false);

        TopBarCustomView topBarCustomView = (TopBarCustomView) rootView.findViewById(R.id.topbar);
        topBarCustomView.setupStepThree();

        setupFirstBlock();
        setupSpinner();

        return rootView;
    }

    private void setupFirstBlock() {
        TextView start = (TextView) rootView.findViewById(R.id.start);
        TextView end = (TextView) rootView.findViewById(R.id.end);
        TextView extra_days = (TextView) rootView.findViewById(R.id.request_step_three_extra_days);
        TextView price = (TextView) rootView.findViewById(R.id.item_available_price);
        TextView delivery = (TextView) rootView.findViewById(R.id.item_delivery_price);
        TextView totalPrice = (TextView) rootView.findViewById(R.id.request_step_three_total_price);
        TextView totalDeposit = (TextView) rootView.findViewById(R.id.request_step_three_total_deposit);

        final Fragment f = ((MainActivity) getContext()).getSupportFragmentManager().findFragmentByTag(RequestItemStepOneFragment.class.getName());
        if (f != null) {
            startDateLong = ((RequestItemStepOneFragment) f).start;
            endDateLong = ((RequestItemStepOneFragment) f).end;

            itemId = ((RequestItemStepOneFragment) f).item_id;

            startDate = Utilities.getFormatedDateFromLong2(((RequestItemStepOneFragment) f).start);
            endDate = Utilities.getFormatedDateFromLong2(((RequestItemStepOneFragment) f).end);

            Calendar c1 = Calendar.getInstance();
            c1.setTimeInMillis(startDateLong);
            start.setText(Utilities.addZero(c1.get(Calendar.DAY_OF_MONTH)) + "/" + Utilities.addZero(c1.get(Calendar.MONTH) + 1) + "/" + c1.get(Calendar.YEAR));

            Calendar c2 = Calendar.getInstance();
            c2.setTimeInMillis(endDateLong);
            end.setText(Utilities.addZero(c2.get(Calendar.DAY_OF_MONTH)) + "/" + Utilities.addZero(c2.get(Calendar.MONTH) + 1) + "/" + c2.get(Calendar.YEAR));

            int extra_day = ((RequestItemStepOneFragment) f).extra_days - 1;
            extra_days.setText(getResources().getQuantityString(R.plurals.day, extra_day, extra_day) + getString(R.string.at_extra_day_discount));

            int price_per_day = ((RequestItemStepOneFragment) f).price_per_day;
            int extra_day_price = ((RequestItemStepOneFragment) f).extra_day_price;
            int deposit = ((RequestItemStepOneFragment) f).deposit;

            price.setText("£" + (price_per_day + (extra_day * extra_day_price)));

            delivery.setText("£" + Utilities.formatDoublePrice(deliveryPrice)); // TODO delivery price
            totalPrice.setText("£" + Utilities.formatDoublePrice(price_per_day + (extra_day * extra_day_price) + deliveryPrice));
            totalDeposit.setText("£" + deposit + "\n" + getString(R.string.slash_deposit));

           /* LinearLayout chat = (LinearLayout) rootView.findViewById(R.id.request_ste_three_chat);
            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragManager.get().setFragment(ChatFragment.newInstance(((RequestItemStepOneFragment) f).lend_user_id,
                            ((RequestItemStepOneFragment) f).item_id,
                            ((RequestItemStepOneFragment) f).lend_user_name));
                }
            });*/

            /*CircleImageView user_image = (CircleImageView) rootView.findViewById(R.id.icon);

            Glide.clear(user_image);
            Glide.with(getContext())
                    .load(APIService.URL_BASE_DATA + ((RequestItemStepOneFragment) f).user_url)
                    .centerCrop()
                    .override(Utilities.convertDpToPixels(56, getContext()), Utilities.convertDpToPixels(56, getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.profile_default)
                    .into(user_image);*/
        }
    }

    private void setupSpinner() {
        final Button confirmAndPa = (Button) rootView.findViewById(R.id.request_step_three_confirm);

        DownloadAPIService download = Bootstrapper.getDownloadAPIService();
        download.getAllCards(new ResultListenerList<List<com.reinto.borrowfox.Models.Card>>() {

            @Override
            public void onNullResult(List<com.reinto.borrowfox.Models.Card> result) {
                Toast.makeText(getContext(), getString(R.string.error_downloading_cards), Toast.LENGTH_SHORT).show();
                rootView.findViewById(R.id.request_step_three_progressBar).setVisibility(View.GONE);
            }

            @Override
            public void onEmptyList(List<com.reinto.borrowfox.Models.Card> result) {
                List<String> c = new ArrayList<>();

                Spinner spinner = (Spinner) rootView.findViewById(R.id.card);
                final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, c);
                adapter.setDropDownViewResource(R.layout.layout_custom_spinner_grey);
                spinner.setAdapter(adapter);
                spinner.setVisibility(View.INVISIBLE);
                rootView.findViewById(R.id.request_step_three_progressBar).setVisibility(View.GONE);

                confirmAndPa.setText(getString(R.string.add_new_card));
                confirmAndPa.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.weird_green, null));
                confirmAndPa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(SettingsFragment.newInstance());
                    }
                });
                confirmAndPa.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFilledList(List<com.reinto.borrowfox.Models.Card> result) {
                List<String> c = new ArrayList<>();

                manager = new CardManager();

                for (int i = 0; i < result.size(); i++) {
                    manager.putId(result.get(i).getId());
                    manager.putInfo(result.get(i).getInfo());
                    c.add(result.get(i).getInfo());
                }

                spinner = (Spinner) rootView.findViewById(R.id.card);
                final ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.layout_custom_spinner_grey, manager.getInfo());
                adapter.setDropDownViewResource(R.layout.layout_custom_spinner_grey);
                spinner.setAdapter(adapter);
                spinner.setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.request_step_three_progressBar).setVisibility(View.GONE);


                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        RequestItemManager.get().getItem().paymentCardId = manager.getIdFromSpinner(spinner.getSelectedItemPosition());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                confirmAndPa.setText(R.string.confirm_and_pay);
                confirmAndPa.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.button_red, null));
                confirmAndPa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finishRequest();
                    }
                });
                confirmAndPa.setVisibility(View.VISIBLE);
            }
        });
    }

    private void finishRequest() {
        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.makeRequest2(RequestItemManager.get().getItem(), new ResultListenerBoolean() {
            @Override
            public void onSuccess() {
                Toast.makeText(getContext(), R.string.request_created, Toast.LENGTH_SHORT).show();
                RequestItemManager.get().clear();
                FragManager.get().setFragment(HomeFragment.newInstance());
            }

            @Override
            public void onFail() {
                Toast.makeText(getContext(), R.string.error_making_request, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.request_item));
        Utilities.hideToolbarIcons(getContext());
    }
}
