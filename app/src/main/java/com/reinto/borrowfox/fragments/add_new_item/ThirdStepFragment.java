/*
 *
 * ThirdStepFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 13:03
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.add_new_item;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.NewItemInfo;
import com.reinto.borrowfox.Models.UploadImage;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CalendarCreateItem;
import com.reinto.borrowfox.managers.FragManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ThirdStepFragment extends Fragment {

    private CalendarCreateItem calendarDialogStepThree;
    private NewItemInfo completeItemData;
    private View rootView;

    public static ThirdStepFragment newInstance() {
        Bundle args = new Bundle();
        ThirdStepFragment fragment = new ThirdStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_item_third_step, container, false);

        Log.d(App.TAG, "ThirdStepFragment, onCreateView: " + ((MainActivity) getContext()).newItem.toString());

        calendarDialogStepThree = (CalendarCreateItem) rootView.findViewById(R.id.calendar);
        completeItemData = ((MainActivity) getContext()).newItem;

        try {
            calendarDialogStepThree.setSelectedDays(((MainActivity) getContext()).newItem.getBlocked_days());
        } catch (NullPointerException e) {
            // not filled before
        }

        Button next = (Button) rootView.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                LinearLayout linear = (LinearLayout) rootView.findViewById(R.id.idparent);
                AlphaAnimation animation = new AlphaAnimation(1f, 0f);
                animation.setDuration(600);
                animation.setFillAfter(true);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        PrepareDataAndSend prepareData = new PrepareDataAndSend();
                        prepareData.execute();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                linear.startAnimation(animation);

                ((MainActivity) getContext()).newItem.setBlocked_days(calendarDialogStepThree.getSelectedDays());
                completeItemData.setBlocked_days(calendarDialogStepThree.getSelectedDays());
            }
        });

        Button back = (Button) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragManager.get().setFragment(SecondStepFragment.newInstance());
                //((MainActivity) getContext()).setupFragment(SecondStepFragment.class);
            }
        });

        return rootView;
    }

    private void finishSending(ArrayList<UploadImage> images, ArrayList<String> days) {
        ((TextView) rootView.findViewById(R.id.info_textview)).setText("Uploading images\nplease wait...");

        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.createNewItem(Utilities.getUserId(getContext()),
                completeItemData.getItem_name(),
                completeItemData.getDescription(),
                completeItemData.getCategory(),
                completeItemData.getPrice_per_day(),
                completeItemData.getAdditional_day_discount(),
                completeItemData.getDeposit(),
                completeItemData.getMinimim_day_rental(),
                completeItemData.getItem_collection_and_return(),
                images,
                days, new TaskDoneListener<Boolean>() {
                    @Override
                    public void OnTaskDone(Boolean object) {
                        if (object) {
                            FragManager.get().setFragment(FinishCreateItemFragment.newInstance());
                        } else {
                            LinearLayout linear = (LinearLayout) rootView.findViewById(R.id.idparent);
                            AlphaAnimation animation = new AlphaAnimation(0f, 1f);
                            animation.setDuration(600);
                            animation.setFillAfter(true);
                            linear.startAnimation(animation);

                            Toast.makeText(getContext(), R.string.error_upload_item, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.step_three));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(2);
    }

    private class PrepareDataAndSend extends AsyncTask<Void, Void, Void> {

        ArrayList<UploadImage> images;
        ArrayList<String> days;

        @Override
        protected Void doInBackground(Void... voids) {
            images = new ArrayList<>();
            prepareImageForSend(images);
            days = new ArrayList<>();
            prepareBlockedDaysData(days);

            Log.d(App.TAG, "ThirdStepFragment, onClick: " + images.toString());
            Log.d(App.TAG, "ThirdStepFragment, onClick: " + days.toString());

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finishSending(images, days);
        }

        private void prepareBlockedDaysData(ArrayList<String> days) {
            Date date;
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            for (int i = 0; i < completeItemData.getBlocked_days().size(); i++) {
                date = new Date(completeItemData.getBlocked_days().get(i));
                days.add(df2.format(date));
            }
        }

        /**
         * Loading images and converting the to base64 format
         *
         * @param images list to put data in
         */
        private void prepareImageForSend(ArrayList<UploadImage> images) {
            for (int i = 0; i < completeItemData.getAddresses().size(); i++) {
                Bitmap bitmap = Utilities.decodeSampledBitmapFromResource(completeItemData.getAddresses().get(i), 800, 600);
                images.add(new UploadImage(i, completeItemData.getItem_name() + "_" + i, Utilities.bitMapToString(bitmap), 1));
            }
        }
    }
}