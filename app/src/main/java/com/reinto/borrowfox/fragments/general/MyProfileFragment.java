/*
 *
 * MyProfileFragment.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 29.6.16 9:23
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.fragments.general;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.Address;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyProfileFragment extends Fragment {

    private ImageView addPhotos;
    private Button editSave;
    private EditText firstName, surname, emailEdit;
    private Address homeAddress, workAddress;
    private TextView nameTextview, email;
    private View rootView;
    private boolean imageChanged = false;
    private boolean active = false,
            homeAddressVisible = false,
            workAddressVisible = false;

    private Bitmap bitmap;

    public static MyProfileFragment newInstance() {
        Bundle args = new Bundle();
        MyProfileFragment fragment = new MyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        setupUI();
        setupEditButton();
        setupEditMode();

        return rootView;
    }

    private void setupUI() {
        email = (TextView) rootView.findViewById(R.id.login_email);
        email.setText(Utilities.getUserEmail(getContext()));
        final TextView home_address_textview = (TextView) rootView.findViewById(R.id.home_address_textview);

        rootView.findViewById(R.id.progresBar).setVisibility(View.VISIBLE);

        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
        downloadAPIService.downloadHomeAddress(new TaskDoneListener<Address>() {
            @Override
            public void OnTaskDone(Address object) {
                rootView.findViewById(R.id.progresBar).setVisibility(View.GONE);
                if (object != null) {
                    homeAddress = object;
                    home_address_textview.setText(object.getNumber() + ", " + object.getStreet() + "\n" + object.getCity() + "\n" + object.getPsc());
                    setupHomeEdit();
                }
            }
        });

        downloadAPIService.downloadWorkAddress(new TaskDoneListener<Address>() {
            @Override
            public void OnTaskDone(Address object) {
                if (object != null) {
                    workAddress = object;
                    setupWorkEdit();
                }
            }
        });

        addPhotos = (ImageView) rootView.findViewById(R.id.profile_image);
        Glide.with(this).load(Utilities.getFbImage(getContext())).placeholder(R.drawable.profile_default).centerCrop().into(addPhotos);
    }

    private void setupWorkEdit() {
        RelativeLayout home = (RelativeLayout) rootView.findViewById(R.id.work_address_layout);
        EditText number = (EditText) home.findViewById(R.id.home_number);
        EditText postcode = (EditText) home.findViewById(R.id.postcode);
        EditText street = (EditText) home.findViewById(R.id.street);
        EditText country = (EditText) home.findViewById(R.id.country);

        number.setText(String.valueOf(workAddress.getNumber()));
        postcode.setText(workAddress.getPsc());
        street.setText(workAddress.getStreet());
        country.setText(workAddress.getCity());
    }

    private void setupHomeEdit() {
        RelativeLayout home = (RelativeLayout) rootView.findViewById(R.id.home_address_layout);
        EditText number = (EditText) home.findViewById(R.id.home_number);
        EditText postcode = (EditText) home.findViewById(R.id.postcode);
        EditText street = (EditText) home.findViewById(R.id.street);
        EditText country = (EditText) home.findViewById(R.id.country);

        number.setText(String.valueOf(homeAddress.getNumber()));
        postcode.setText(homeAddress.getPsc());
        street.setText(homeAddress.getStreet());
        country.setText(homeAddress.getCity());
    }

    private void setupEditMode() {
        ImageButton photo_edit = (ImageButton) rootView.findViewById(R.id.photo_edit);
        photo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoDialog();
            }
        });

        nameTextview = (TextView) rootView.findViewById(R.id.textView);
        nameTextview.setText(Utilities.getUserName(getContext()));

        firstName = (EditText) rootView.findViewById(R.id.editText3);
        firstName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        surname = (EditText) rootView.findViewById(R.id.editText2);

        String user_name = Utilities.getUserName(getContext());
        String[] parts = user_name.split(" ");
        firstName.setText(parts[0]);
        surname.setText(parts[1]);

        emailEdit = (EditText) rootView.findViewById(R.id.email_edit);
        emailEdit.setText(Utilities.getUserEmail(getContext()).toUpperCase());

        final Button home_address = (Button) rootView.findViewById(R.id.home_address);
        home_address.setOnClickListener(new View.OnClickListener() {

            RelativeLayout home = (RelativeLayout) rootView.findViewById(R.id.home_address_layout);

            @Override
            public void onClick(View view) {
                if (homeAddressVisible) {
                    home.setVisibility(View.GONE);
                    homeAddressVisible = false;
                } else {
                    home.setVisibility(View.VISIBLE);
                    homeAddressVisible = true;
                }
            }
        });

        final Button work_address = (Button) rootView.findViewById(R.id.work_address);
        work_address.setOnClickListener(new View.OnClickListener() {

            RelativeLayout home = (RelativeLayout) rootView.findViewById(R.id.work_address_layout);

            @Override
            public void onClick(View view) {
                if (workAddressVisible) {
                    home.setVisibility(View.GONE);
                    workAddressVisible = false;
                } else {
                    home.setVisibility(View.VISIBLE);
                    workAddressVisible = true;
                }
            }
        });
    }

    private void showPhotoDialog() {
        final CharSequence[] items = {getString(R.string.take_new_photo), getString(R.string.pick_image_from_gallery),
                getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.add_item_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_new_photo))) {
                    requestPermission(new OnFinish() {
                        @Override
                        public void onFinish() {
                            getImageFromStorage();
                        }
                    });
                } else if (items[item].equals(getString(R.string.pick_image_from_gallery))) {
                    requestPermission(new OnFinish() {
                        @Override
                        public void onFinish() {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(intent, 200);
                        }
                    });
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void setupEditButton() {
        editSave = (Button) rootView.findViewById(R.id.edit_save);
        editSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!active) {
                    rootView.findViewById(R.id.photo_edit).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.texts).setVisibility(View.GONE);
                    rootView.findViewById(R.id.home_address).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.work_address).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.editText3).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.editText2).setVisibility(View.VISIBLE);
                    nameTextview.setVisibility(View.GONE);
                    rootView.findViewById(R.id.email_edit).setVisibility(View.VISIBLE);

                    editSave.setText(getString(R.string.save));

                    active = true;
                    Utilities.setToolbarTitle(getContext(), getString(R.string.edit_profile));
                } else {

                    editSave.setEnabled(false);
                    editSave.setBackgroundResource(R.color.black54tra);
                    rootView.findViewById(R.id.progresBar).setVisibility(View.VISIBLE);
                    checkDataConsistencyAndSendToServer(new OnFinishUpdate() {

                        @Override
                        public void onFinish(boolean success) {
                            rootView.findViewById(R.id.progresBar).setVisibility(View.GONE);
                            editSave.setBackgroundResource(R.color.button_red);
                            editSave.setEnabled(true);
                            if (success) {
                                rootView.findViewById(R.id.photo_edit).setVisibility(View.GONE);
                                rootView.findViewById(R.id.texts).setVisibility(View.VISIBLE);
                                rootView.findViewById(R.id.home_address).setVisibility(View.GONE);
                                rootView.findViewById(R.id.work_address).setVisibility(View.GONE);
                                rootView.findViewById(R.id.editText3).setVisibility(View.GONE);
                                rootView.findViewById(R.id.editText2).setVisibility(View.GONE);
                                nameTextview.setVisibility(View.VISIBLE);
                                rootView.findViewById(R.id.email_edit).setVisibility(View.GONE);
                                rootView.findViewById(R.id.work_address_layout).setVisibility(View.GONE);
                                rootView.findViewById(R.id.home_address_layout).setVisibility(View.GONE);

                                editSave.setText(getString(R.string.edit_profile));

                                updateProfileImage();
                                imageChanged = false;
                                bitmap = null;

                                active = false;
                                Utilities.setToolbarTitle(getContext(), getString(R.string.profile));
                            } else {
                                // Toast.makeText(getContext(), "Error occured", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private void updateProfileImage() {
        Utilities.updateProfileImage(getContext());
    }

    private void checkDataConsistencyAndSendToServer(final OnFinishUpdate onFinish) {
        if (firstName.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.first_name_is_empty, Toast.LENGTH_SHORT).show();
            onFinish.onFinish(false);
            return;
        }

        if (surname.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.last_name_is_empty, Toast.LENGTH_SHORT).show();
            onFinish.onFinish(false);
            return;
        }

        if (emailEdit.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.email_is_empty, Toast.LENGTH_SHORT).show();
            onFinish.onFinish(false);
            return;
        }

        RelativeLayout home = (RelativeLayout) rootView.findViewById(R.id.home_address_layout);
        final EditText number = (EditText) home.findViewById(R.id.home_number);
        final EditText postcode = (EditText) home.findViewById(R.id.postcode);
        final EditText street = (EditText) home.findViewById(R.id.street);
        final EditText country = (EditText) home.findViewById(R.id.country);

        // home address is not completed
        if (number.getText().toString().length() == 0 ||
                postcode.getText().toString().length() == 0 ||
                street.getText().toString().length() == 0 ||
                country.getText().toString().length() == 0) {
            Toast.makeText(getContext(), R.string.home_address_not_filled_correctly, Toast.LENGTH_SHORT).show();
            onFinish.onFinish(false);
            return;
        }

        RelativeLayout work = (RelativeLayout) rootView.findViewById(R.id.work_address_layout);
        final EditText number2 = (EditText) work.findViewById(R.id.home_number);
        final EditText postcode2 = (EditText) work.findViewById(R.id.postcode);
        final EditText street2 = (EditText) work.findViewById(R.id.street);
        final EditText country2 = (EditText) work.findViewById(R.id.country);

        List<Integer> list = new ArrayList<>();
        list.add(number2.getText().toString().length());
        list.add(postcode2.getText().toString().length());
        list.add(street2.getText().toString().length());
        list.add(country2.getText().toString().length());

        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }

        if (list.contains(0) && sum > 0) {
            Toast.makeText(getContext(), R.string.work_address_not_filled_correctly, Toast.LENGTH_SHORT).show();
            onFinish.onFinish(false);
            return;
        }

        if (imageChanged) {
            // update all including image
            DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
            downloadAPIService.fullyUpdateProfile(firstName.getText().toString(),
                    surname.getText().toString(),
                    emailEdit.getText().toString(),
                    country.getText().toString(),
                    street.getText().toString(),
                    number.getText().toString(),
                    postcode.getText().toString(),
                    country2.getText().toString(),
                    street2.getText().toString(),
                    number2.getText().toString(),
                    postcode2.getText().toString(),
                    BitMapToString(bitmap),
                    "",
                    "1",
                    street.getText().toString(),
                    country.getText().toString(),
                    new TaskDoneListener<Boolean>() {
                        @Override
                        public void OnTaskDone(Boolean object) {
                            if (object) {
                                Toast.makeText(getContext(), R.string.data_success_updated, Toast.LENGTH_SHORT).show();

                                nameTextview.setText(firstName.getText().toString() + " " + surname.getText().toString());
                                email.setText(emailEdit.getText().toString());
                                Utilities.saveUserLocation(getContext(), street.getText().toString() + ", " + country.getText().toString());
                                ((MainActivity) getContext()).setupNavigationHeader();
                                rootView.findViewById(R.id.status).setVisibility(View.GONE);

                                final TextView home_address_textview = (TextView) rootView.findViewById(R.id.home_address_textview);
                                home_address_textview.setText(number.getText().toString() + ", " + street.getText().toString() + "\n" +
                                        country.getText().toString() + "\n" + postcode.getText().toString());


                                onFinish.onFinish(true);
                            } else {
                                Toast.makeText(getContext(), R.string.error_uploading_items, Toast.LENGTH_SHORT).show();
                                onFinish.onFinish(false);
                            }
                        }
                    }
            );
        } else {
            DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
            downloadAPIService.fullyUpdateProfileNoImage(firstName.getText().toString(),
                    surname.getText().toString(),
                    emailEdit.getText().toString(),
                    country.getText().toString(),
                    street.getText().toString(),
                    number.getText().toString(),
                    postcode.getText().toString(),
                    country2.getText().toString(),
                    street2.getText().toString(),
                    number2.getText().toString(),
                    postcode2.getText().toString(),
                    street.getText().toString(),
                    country.getText().toString(),
                    new TaskDoneListener<Boolean>() {
                        @Override
                        public void OnTaskDone(Boolean object) {
                            if (object) {
                                Toast.makeText(getContext(), R.string.data_success_updated, Toast.LENGTH_SHORT).show();

                                nameTextview.setText(firstName.getText().toString() + " " + surname.getText().toString());
                                email.setText(emailEdit.getText().toString());
                                Utilities.saveUserLocation(getContext(), street.getText().toString() + ", " + country.getText().toString());
                                ((MainActivity) getContext()).setupNavigationHeader();
                                rootView.findViewById(R.id.status).setVisibility(View.GONE);

                                final TextView home_address_textview = (TextView) rootView.findViewById(R.id.home_address_textview);
                                home_address_textview.setText(number.getText().toString() + ", " + street.getText().toString() + "\n" +
                                        country.getText().toString() + "\n" + postcode.getText().toString());


                                onFinish.onFinish(true);
                            } else {
                                Toast.makeText(getContext(), R.string.error_uploading_items, Toast.LENGTH_SHORT).show();
                                onFinish.onFinish(false);
                            }
                        }
                    }
            );

        }
    }

    public String BitMapToString(Bitmap bitmap) {
        Log.d(App.TAG, "MyProfileFragment, BitMapToString: " + bitmap.getWidth() + ", " + bitmap.getHeight());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.profile));
        Utilities.hideToolbarIcons(getContext());
        ((MainActivity) getContext()).selectItemInMenu(1);
    }

    private void requestPermission(OnFinish onfish) {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    40);
        } else {
            // permission is granted
            onfish.onFinish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 40) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromStorage();
            }
        }
    }

    private void getImageFromStorage() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, 150);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 150 && resultCode == Activity.RESULT_OK) {
            File imageFile = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");

            CreateBitmap c = new CreateBitmap(imageFile.getAbsolutePath());
            c.execute();

            //openPhotosListFragment(imageFile.getAbsolutePath());
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getContext().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            cursor.close();

            CreateBitmap c = new CreateBitmap(imgDecodableString);
            c.execute();
        }
    }

    private interface OnFinish {
        void onFinish();
    }

    private interface OnFinishUpdate {
        void onFinish(boolean success);
    }

    private class CreateBitmap extends AsyncTask<Bitmap, Void, Bitmap> {

        private String address;

        public CreateBitmap(String address) {
            this.address = address;
        }

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            return Utilities.decodeSampledBitmapFromResource(address, 250, 250);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(App.TAG, "CreateBitmap, onPostExecute: " + bitmap.getWidth() + ", " + bitmap.getHeight());

            addPhotos.setImageBitmap(bitmap);
            MyProfileFragment.this.bitmap = bitmap;
            imageChanged = true;
        }
    }
}
