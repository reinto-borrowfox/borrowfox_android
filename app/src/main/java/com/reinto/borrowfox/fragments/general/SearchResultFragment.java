package com.reinto.borrowfox.fragments.general;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Adapters.RecyclerViewAdapter;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.CustomListeners.RecyclerItemClickListener;
import com.reinto.borrowfox.Models.ItemData;
import com.reinto.borrowfox.Models.SingleCategoryItemModel;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.ResultListenerList;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSearchView;
import com.reinto.borrowfox.Views.EndlessRecyclerView;
import com.reinto.borrowfox.Views.OnEndReachListener;
import com.reinto.borrowfox.managers.FragManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchResultFragment extends Fragment {

    public static final String KEY_WORD = "keyWord";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String ORDER = "order";
    public static final String MAX_PRICE = "maxPrice";
    public static final String TYPE = "type";

    public static final int ITEM_TYPE = 1;
    public static final int MAP_TYPE = 2;

    private View rootView;
    private RecyclerViewAdapter adapter;
    private boolean loading;
    private boolean shouldLoadNext = true;
    private ArrayList<SingleCategoryItemModel> itemsToAdd;

    private String keyWork, from, to;
    private int order, maxPrice;
    private int type;
    private MyMapFragment myMapFragment;

    public static SearchResultFragment newInstance(String keyWord, int order, int maxPrice, String from, String to, int type) {
        Bundle args = new Bundle();
        args.putString(KEY_WORD, keyWord);
        args.putString(FROM, from);
        args.putString(TO, to);
        args.putInt(ORDER, order);
        args.putInt(MAX_PRICE, maxPrice);
        args.putInt(TYPE, type);
        SearchResultFragment fragment = new SearchResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        keyWork = getArguments().getString(KEY_WORD);
        from = getArguments().getString(FROM);
        to = getArguments().getString(TO);
        order = getArguments().getInt(ORDER);
        maxPrice = getArguments().getInt(MAX_PRICE);
        type = getArguments().getInt(TYPE);
        adapter = new RecyclerViewAdapter(getContext(), new ArrayList<SingleCategoryItemModel>());
        itemsToAdd = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_detail, container, false);

        setupListView();

        if (adapter.getItemCount() == 0) {
            loadItems();
        } else {
            rootView.findViewById(R.id.progressBaroo).setVisibility(View.GONE);
        }

        return rootView;
    }

    private void putItemToMap(final List<ItemData> result) {
        myMapFragment = new MyMapFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.map_frame, myMapFragment)
                .commit();

        myMapFragment.setOnMapReadyListener(new MyMapFragment.OnMapReady() {
            @Override
            public void mapIsReady() {
                LoadPositionsAsyncTask load = new LoadPositionsAsyncTask(result);
                load.execute();
            }
        });
    }

    private void loadItems() {
        loading = true;
        Bootstrapper.getDownloadAPIService().searchForItems(keyWork, from, to,
                maxPrice, order, 0, adapter.getItemCount(), new ResultListenerList<List<ItemData>>() {

                    @Override
                    public void onNullResult(List<ItemData> result) {
                        loading = false;
                        shouldLoadNext = true;
                        Toast.makeText(getContext(), getContext().getString(R.string.error_downlading_items), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onEmptyList(List<ItemData> result) {
                        loading = false;
                        shouldLoadNext = false;

                        rootView.findViewById(R.id.category_detail_no_items).setVisibility(View.VISIBLE);
                        Utilities.dissapearView(rootView.findViewById(R.id.clona));
                        Utilities.dissapearView(rootView.findViewById(R.id.progressBaroo));
                    }

                    @Override
                    public void onFilledList(List<ItemData> result) {
                        Log.d(App.TAG, "Filter, onFilledList: " + result.toString());
                        loading = false;
                        shouldLoadNext = result.size() != 0;
                        downloadImages(result);
                    }
                });
    }

    private void downloadImages(final List<ItemData> objecto) {
        for (int i = 0; i < objecto.size(); i++) {

            final int finalI = i;
            objecto.get(i).downloadImages(new ItemData.OnMyDialogResult() {
                @Override
                public void finish() {
                    setupObjects(objecto, finalI);
                }
            });
        }
    }

    private void setupObjects(List<ItemData> objecto, int i) {
        final SingleCategoryItemModel x = new SingleCategoryItemModel(
                objecto.get(i).getId(),
                String.valueOf(objecto.get(i).getPrice_per_day()),
                objecto.get(i).getName(),
                objecto.get(i).getUser_locality(),
                objecto.get(i).getImages(),
                objecto.get(i).getUser_filename());

        Log.d(App.TAG, "CategoryDetailFragment, setupObjects: " + x.toString());
        itemsToAdd.add(x);

        if (adapter.getItemCount() == 0) {
            Utilities.dissapearView(rootView.findViewById(R.id.clona));
            dissapearView(rootView.findViewById(R.id.progressBaroo));
        }

        if (objecto.size() == (i + 1)) {
            adapter.addData(itemsToAdd);
            itemsToAdd.clear();
        }
    }

    private void setupListView() {
        EndlessRecyclerView recyclerView = (EndlessRecyclerView) rootView.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FragManager.get().setFragment(ItemDetailFragment.newInstance(adapter.getItem(position).getItem_id(), false));
            }
        }));
        recyclerView.setOnEndReachListener(new OnEndReachListener() {
            @Override
            public void onEndReach() {
                if (!loading && shouldLoadNext)
                    loadItems();
            }
        });


        if (((MainActivity) getContext()).customSearchView != null) {
            CustomSearchView customSearchView = ((MainActivity) getContext()).customSearchView;
            customSearchView.setOnSearchIconClick(new CustomSearchView.OnSearchClick() {
                @Override
                public void onSeachClick(String text) {
                    adapter.filter(text);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utilities.setToolbarTitle(getContext(), getString(R.string.search_results));
        ((MainActivity) getContext()).setToggleState(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MainActivity) getContext()).setToggleState(true);
    }

    private void dissapearView(final View view) {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(600);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        view.startAnimation(animation);
    }

    private void finishMapPreparing(List<LatLng> markers, List<String> itemAddress, List<String> itemNames) {
        if (markers.size() == 0) {
            rootView.findViewById(R.id.category_detail_no_items).setVisibility(View.VISIBLE);
            Utilities.dissapearView(rootView.findViewById(R.id.clona));
            dissapearView(rootView.findViewById(R.id.progressBaroo));
        }

        myMapFragment.setListener(new MyMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
            }
        });

        for (int i = 0; i < markers.size(); i++) {
            // myMapFragment.setMarker(markers.get(i).latitude, markers.get(i).longitude);
            myMapFragment.map.addMarker(new MarkerOptions()
                    .position(markers.get(i))
                    .title(itemAddress.get(i))
                    .snippet(itemNames.get(i)));
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng marker : markers) {
            builder.include(marker);
        }
        LatLngBounds bounds = builder.build();

        int padding = Utilities.convertDpToPixels(64, getContext()); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        myMapFragment.map.moveCamera(cu);

        Utilities.dissapearView(rootView.findViewById(R.id.clona));
        Utilities.dissapearView(rootView.findViewById(R.id.progressBaroo));
        rootView.findViewById(R.id.map_frame).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.recycleView).setVisibility(View.GONE);
    }

    private class LoadPositionsAsyncTask extends AsyncTask<Void, Void, Void> {

        private List<ItemData> list;
        private List<LatLng> markers;
        private List<String> itemNames;
        private List<String> itemAddress;

        public LoadPositionsAsyncTask(List<ItemData> list) {
            this.list = list;
            markers = new ArrayList<>();
            itemNames = new ArrayList<>();
            itemAddress = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < list.size(); i++) {
                try {
                    Geocoder geocoder = new Geocoder(getContext());
                    List<Address> addresses;
                    addresses = geocoder.getFromLocationName(list.get(i).getUser_locality(), 1);
                    if (addresses.size() > 0) {
                        double latitude = addresses.get(0).getLatitude();
                        double longitude = addresses.get(0).getLongitude();
                        Log.d(App.TAG, "ItemDetailFragment, setupUI: " + latitude + ", " + longitude);
                        if (Math.abs(longitude) < 1) {
                            markers.add(new LatLng(latitude, longitude));
                            itemNames.add(list.get(i).getName());
                            itemAddress.add(list.get(i).getUser_locality());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finishMapPreparing(markers, itemNames, itemAddress);
        }
    }
}
