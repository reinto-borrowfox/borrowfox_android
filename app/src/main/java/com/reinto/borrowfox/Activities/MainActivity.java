package com.reinto.borrowfox.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.NewItemInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CustomSearchView;
import com.reinto.borrowfox.Views.Filter;
import com.reinto.borrowfox.fragments.add_new_item.FirstStepFragment;
import com.reinto.borrowfox.fragments.general.BorrowFragment;
import com.reinto.borrowfox.fragments.general.ChatFragment;
import com.reinto.borrowfox.fragments.general.ChatListFragment;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.fragments.general.LendFragment;
import com.reinto.borrowfox.fragments.general.MyProfileFragment;
import com.reinto.borrowfox.fragments.general.NotificationFragment;
import com.reinto.borrowfox.fragments.general.SettingsFragment;
import com.reinto.borrowfox.managers.FragManager;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String PASSWORD = "password";

    public NewItemInfo newItem;
    public CustomSearchView customSearchView;
    public boolean filterIsVisible = false;
    public NavigationView navigationView;
    public Class fragment;
    private DrawerLayout drawer;
    private Filter filter;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        filter = (Filter) findViewById(R.id.filter_layout);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_SETTLING && !drawer.isDrawerOpen(GravityCompat.START)) {
                    hideFilter();
                }

                super.onDrawerStateChanged(newState);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        if (getIntent().getStringExtra(PASSWORD) != null) {
            DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
            downloadAPIService.sendPassword(getIntent().getStringExtra(PASSWORD), new TaskDoneListener<Boolean>() {
                @Override
                public void OnTaskDone(Boolean object) {
                    if (!object) {
                        Toast.makeText(MainActivity.this, R.string.error_sending_password, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Log.d(App.TAG, "MainActivity, OnTaskDone: password sent");
                }
            });
        }

        customSearchView = (CustomSearchView) findViewById(R.id.customsearchview);
        customSearchView.setOnFilterIconClick(new CustomSearchView.OnFilterClick() {

            @Override
            public void onFilterClick() {
                Log.d(App.TAG, "MainActivity, onFilterClick: " + filterIsVisible);
                if (!filterIsVisible) {
                    filter.setVisibility(View.VISIBLE);
                    drawer.closeDrawer(GravityCompat.START);
                    filterIsVisible = true;
                    filter.showFilter();
                    /*filter.setVisibility(View.VISIBLE);
                    Animation bottomUp = AnimationUtils.loadAnimation(MainActivity.this, R.anim.filter_slide_in);
                    bottomUp.setInterpolator(new DecelerateInterpolator());
                    filter.startAnimation(bottomUp);*/
                }
            }
        });

        fragment = HomeFragment.class;
        setMenu(HomeFragment.class);

        try {
            JSONObject message = new JSONObject(getIntent().getStringExtra("message"));
            Log.d(App.TAG, "MainActivity, onCreate: " + message);

            if (message.getInt("type") == 1) {
                FragManager.get().setFragment(ChatFragment.newInstance(message.getInt("lenderId"), message.getInt("itemId"), message.getString("userName")));
            } else if (message.getInt("type") == 2) {
                FragManager.get().setFragment(NotificationFragment.newInstance());
            } else if (message.getInt("type") == 3) {
                FragManager.get().setFragment(LendFragment.newInstance(Emuns.LendItemType.LEND_REQUESTED));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void setupNavigationHeader() {
        View headerLayout = navigationView.getHeaderView(0); // 0-index header
        TextView headerName = (TextView) headerLayout.findViewById(R.id.name);
        headerName.setText(Utilities.getUserName(this));

        TextView headerLocality = (TextView) headerLayout.findViewById(R.id.location);
        headerLocality.setText(Utilities.getUserLocation(this));

        ImageView profileImage = (ImageView) headerLayout.findViewById(R.id.profile_image);
        Glide.with(this).load(Utilities.getFbImage(this)).placeholder(R.drawable.profile_default).centerCrop().into(profileImage);
    }

    private void hideFilter() {
        if (filterIsVisible) {
            filterIsVisible = false;
            filter.hideFilter();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.updateProfileImage(this);
        Utilities.updateHomeAddress(this);
        setupNavigationHeader();
    }

    private void setMenu(Class c) {
        if (c.isInstance(getSupportFragmentManager().findFragmentById(R.id.content))) {
            drawer.closeDrawers();
            return;
        }

        String tag = c.getName();
        Fragment f;

        f = FragManager.get().getFragmentManager().findFragmentByTag(tag);

        if (f == null) {
            try {
                f = (Fragment) c.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragManager.get().setFragment(f);
        } else {
            FragManager.get().pop(tag);
        }

        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        if (filterIsVisible) {
            filterIsVisible = false;
            filter.hideFilter();
            return;
        }

        Fragment mActivityDirectFragment = getSupportFragmentManager().findFragmentById(R.id.content);

        if (mActivityDirectFragment != null && mActivityDirectFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
            mActivityDirectFragment.getChildFragmentManager().popBackStack();
            return;
        }

        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
            return;
        }

        super.onBackPressed();
    }

    public void selectItemInMenu(int x) {
        navigationView.getMenu().getItem(x).setChecked(true);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            setMenu(HomeFragment.class);
            selectItemInMenu(0);

        } else if (id == R.id.profile) {

            setMenu(MyProfileFragment.class);
            selectItemInMenu(1);

        } else if (id == R.id.add_item) {
            setMenu(FirstStepFragment.class);
            newItem = new NewItemInfo();
            selectItemInMenu(2);

        } else if (id == R.id.my_borrows) {
            fragment = BorrowFragment.class;

            setMenu(BorrowFragment.class);
            selectItemInMenu(3);

        } else if (id == R.id.my_lends) {
            fragment = LendFragment.class;
            setMenu(LendFragment.class);
            selectItemInMenu(4);

        } else if (id == R.id.messages) {
            setMenu(ChatListFragment.class);
            selectItemInMenu(5);

        } else if (id == R.id.notifications) {
            fragment = NotificationFragment.class;

            setMenu(NotificationFragment.class);
            selectItemInMenu(6);

        } else if (id == R.id.settings) {
            setMenu(SettingsFragment.class);
            selectItemInMenu(7);

        } else if (id == R.id.log_out) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.main_log_out);
            builder.setMessage(R.string.do_you_want_to_log_out);
            builder.setPositiveButton(R.string.log_out_big, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    FacebookSdk.sdkInitialize(MainActivity.this.getApplicationContext());
                    LoginManager.getInstance().logOut();

                    Utilities.logoutViaFb(MainActivity.this);
                    Utilities.saveUserId(MainActivity.this, 0);
                    Utilities.saveFbImage(MainActivity.this, "");

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            builder.setNegativeButton(R.string.no_big, null);
            builder.show();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setToggleState(boolean isEnabled) {
        if (drawer == null)
            return;

        if (isEnabled) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.setDrawerIndicatorEnabled(true);
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
        toggle.syncState();
    }

    public Class getFragment() {
        return fragment;
    }

    public void setFragment(Class f) {
        this.fragment = f;
    }
}