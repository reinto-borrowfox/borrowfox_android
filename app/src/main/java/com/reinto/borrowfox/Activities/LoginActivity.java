package com.reinto.borrowfox.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

public class LoginActivity extends AppCompatActivity {

    public static final String FIRST_NAME = "first_name";
    public static final String ID_FACEBOOK = "idFacebook";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    public static final String LOCATION = "location";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String PROFILE_PIC = "profile_pic";

    CallbackManager callbackManager;
    private boolean isLogging = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        FacebookSdk.sdkInitialize(this.getApplicationContext());

        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Collections.singletonList(EMAIL));

        callbackManager = CallbackManager.Factory.create();



        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                loginButton.setVisibility(View.INVISIBLE);

                                // Get facebook data from login
                                Bundle bFacebookData = getFacebookData(object);

                                Utilities.saveFbImage(LoginActivity.this, bFacebookData.getString(PROFILE_PIC));

                                findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);

                                DownloadAPIService d = Bootstrapper.getDownloadAPIService();
                                d.sendFBInfoToServer(bFacebookData.getString(EMAIL),
                                        bFacebookData.getString(GENDER),
                                        bFacebookData.getString(ID_FACEBOOK),
                                        bFacebookData.getString(FIRST_NAME) + " " + bFacebookData.getString(LAST_NAME), new TaskDoneListener<Boolean>() {
                                            @Override
                                            public void OnTaskDone(Boolean object) {
                                                findViewById(R.id.login_progress_bar).setVisibility(View.INVISIBLE);

                                                if (!object) {
                                                    Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }

                                                Utilities.loginViaFb(LoginActivity.this);

                                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                                startActivity(i);
                                                finish();

                                                Toast.makeText(LoginActivity.this, R.string.you_have_been_succesfully_logged_in, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(App.TAG, "LoginActivity, onCancel: login canceled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(App.TAG, "LoginActivity, onError: login error");
                    }
                });

        final Button login = (Button) findViewById(R.id.login);
        final Button signup = (Button) findViewById(R.id.signup);
        final ImageView secondBacground = (ImageView) findViewById(R.id.second_background);
        final TextView tvForgotPassword = (TextView) findViewById(R.id.login_forgot_password);

        final EditText firstName = (EditText) findViewById(R.id.login_first_name);
        final EditText lastName = (EditText) findViewById(R.id.login_last_name);
        final EditText password = (EditText) findViewById(R.id.login_password);
        final EditText reenter = (EditText) findViewById(R.id.login_reenter_password);
        final EditText email = (EditText) findViewById(R.id.login_email);

        if (Utilities.isUserLoggedIn(this) || Utilities.isUserLoggedInFb(this)) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
            return;
        }

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogging) {
                    secondBacground.setAlpha(0);
                    secondBacground.setVisibility(View.VISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_in);
                    animation.setFillAfter(true);
                    secondBacground.startAnimation(animation);

                    isLogging = false;

                    firstName.setVisibility(View.VISIBLE);
                    lastName.setVisibility(View.VISIBLE);
                    reenter.setVisibility(View.VISIBLE);

                    TextView forgotPassword = (TextView) findViewById(R.id.login_forgot_password);
                    forgotPassword.setVisibility(View.GONE);

                    login.setBackgroundResource(R.color.white);
                    signup.setBackgroundResource(android.R.color.transparent);
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLogging) {
                    Animation animation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_out);
                    animation.setFillAfter(true);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            secondBacground.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                    secondBacground.startAnimation(animation);


                    isLogging = true;

                    firstName.setVisibility(View.GONE);
                    reenter.setVisibility(View.GONE);
                    lastName.setVisibility(View.GONE);

                    TextView forgotPassword = (TextView) findViewById(R.id.login_forgot_password);
                    forgotPassword.setVisibility(View.VISIBLE);

                    login.setBackgroundResource(android.R.color.transparent);
                    signup.setBackgroundResource(R.color.light_gray);
                }
            }
        });

        final Button logIntoApp = (Button) findViewById(R.id.login_log_into_app);
        logIntoApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();

                findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);
                logIntoApp.setVisibility(View.INVISIBLE);

                if (isLogging) {
                    if (checkIfFilledLogin(email, password)) {

                        downloadAPIService.login(email.getText().toString(), password.getText().toString(), new TaskDoneListener<Boolean>() {
                            @Override
                            public void OnTaskDone(Boolean object) {
                                findViewById(R.id.login_progress_bar).setVisibility(View.INVISIBLE);
                                logIntoApp.setVisibility(View.VISIBLE);
                                if (!object) {
                                    Toast.makeText(LoginActivity.this, R.string.login_was_not_successful, Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                Toast.makeText(LoginActivity.this, R.string.login_was_successful, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        });

                    } else {
                        findViewById(R.id.login_progress_bar).setVisibility(View.INVISIBLE);
                        logIntoApp.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (checkIfFilled(firstName, lastName, email, password, reenter)) {
                        downloadAPIService.register(firstName.getText().toString(), lastName.getText().toString(),
                                email.getText().toString(), new TaskDoneListener<Integer>() {
                                    @Override
                                    public void OnTaskDone(Integer object) {
                                        findViewById(R.id.login_progress_bar).setVisibility(View.INVISIBLE);
                                        logIntoApp.setVisibility(View.VISIBLE);
                                        if (object == 0) {
                                            Toast.makeText(LoginActivity.this, R.string.regi_was_not_successful, Toast.LENGTH_SHORT).show();
                                            return;
                                        }

                                        Toast.makeText(LoginActivity.this, R.string.you_have_been_registered, Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                        i.putExtra(PASSWORD, password.getText().toString());
                                        startActivity(i);
                                        finish();
                                    }
                                });
                    } else {
                        findViewById(R.id.login_progress_bar).setVisibility(View.INVISIBLE);
                        logIntoApp.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {
        try {
            Bundle bundle = new Bundle();
            String id = object.getString(ID);

            try {
                URL profilePic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                bundle.putString(PROFILE_PIC, profilePic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString(ID_FACEBOOK, id);
            if (object.has(FIRST_NAME))
                bundle.putString(FIRST_NAME, object.getString(FIRST_NAME));
            if (object.has(LAST_NAME))
                bundle.putString(LAST_NAME, object.getString(LAST_NAME));
            if (object.has(EMAIL))
                bundle.putString(EMAIL, object.getString(EMAIL));
            if (object.has(GENDER))
                bundle.putString(GENDER, object.getString(GENDER));
            if (object.has(BIRTHDAY))
                bundle.putString(BIRTHDAY, object.getString(BIRTHDAY));
            if (object.has(LOCATION))
                bundle.putString(LOCATION, object.getJSONObject(LOCATION).getString(NAME));

            return bundle;
        } catch (JSONException e) {
            return null;
        }
    }

    private boolean checkIfFilledLogin(EditText email, EditText password) {
        if (email.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.please_enter_email, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.please_enter_password, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean checkIfFilled(EditText firstName, EditText lastName, EditText email, EditText password, EditText reenter) {
        if (firstName.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.enter_first_name, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastName.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.enter_last_name, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (email.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.please_enter_email, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.please_enter_password, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (reenter.getText().toString().length() == 0) {
            Toast.makeText(LoginActivity.this, R.string.please_enter_password_again, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!password.getText().toString().equals(reenter.getText().toString())) {
            Toast.makeText(LoginActivity.this, R.string.passwords_do_not_match, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
