package com.reinto.borrowfox.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.reinto.borrowfox.Models.SingleCategoryItemModel;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.ImageAdapter;
import com.reinto.borrowfox.Views.RectangleIndicator;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {

    public HashMap<Integer, Integer> viewPageStates = new HashMap<>();
    private Context context;
    private ArrayList<SingleCategoryItemModel> items;
    private ArrayList<SingleCategoryItemModel> backup;

    public RecyclerViewAdapter(Context context, ArrayList<SingleCategoryItemModel> items) {
        this.context = context;
        this.items = items;
        this.backup = new ArrayList<>(items);
    }

    public SingleCategoryItemModel getItem(int position){
        return items.get(position);
    }

    public void addData(ArrayList<SingleCategoryItemModel> data){
        items.addAll(data);
        backup.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_category_item, null);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        SingleCategoryItemModel itemModel = items.get(position);

        holder.name.setText(itemModel.getName());
        holder.location.setText(itemModel.getLocation());
        holder.price.setText(Utilities.getFormatedPrice(itemModel.getPrice()));

        ImageAdapter adapter = new ImageAdapter(context, itemModel.getUrls());
        holder.viewPager.setAdapter(adapter);
        holder.indicator.setNumber_of_pages(itemModel.getUrls().size());

        if (viewPageStates.get(position) == null) {
            holder.indicator.setActive_index(0);
        } else {
            holder.indicator.setActive_index(viewPageStates.get(position));
        }

        holder.indicator.setupUI();

        Glide.clear(holder.circleImageView);
        Glide.with(context)
                .load(APIService.URL_BASE_DATA + items.get(position).getUser_filename())
                .centerCrop()
                .override(Utilities.convertDpToPixels(56, context), Utilities.convertDpToPixels(56, context))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.circleImageView.setImageDrawable(resource); // images werent showing on the first views
                        return false;
                    }
                })
                .placeholder(R.drawable.profile_default)
                .into(holder.circleImageView);

        if (viewPageStates.get(position) == null) {
            holder.viewPager.setCurrentItem(0);
        } else {
            holder.viewPager.setCurrentItem(viewPageStates.get(position));
        }

        holder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                holder.indicator.changeIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onViewRecycled(CustomViewHolder holder) {
        viewPageStates.put(holder.getAdapterPosition(), holder.viewPager.getCurrentItem());
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    public void filter(String text) {
        if(text.isEmpty()){
            items.clear();
            items.addAll(backup);
        } else{
            ArrayList<SingleCategoryItemModel> result = new ArrayList<>();
            text = text.toLowerCase();

            for (int i = 0; i < backup.size(); i++) {
                if (backup.get(i).getName().toUpperCase().contains(text.toUpperCase())) {
                    result.add(backup.get(i));
                }
            }

            items.clear();
            items.addAll(result);
        }
        notifyDataSetChanged();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView location;
        TextView price;
        ViewPager viewPager;
        CircleImageView circleImageView;
        RectangleIndicator indicator;

        public CustomViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.name);
            this.location = (TextView) view.findViewById(R.id.location);
            this.price = (TextView) view.findViewById(R.id.item_available_price);
            this.viewPager = (ViewPager) view.findViewById(R.id.viewpager);
            this.indicator = (RectangleIndicator) view.findViewById(R.id.rectangleIndicator);
            this.circleImageView = (CircleImageView)view.findViewById(R.id.circular_image);
        }

        @Override
        public void onClick(View view) {
        }
    }
}
