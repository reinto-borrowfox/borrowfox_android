package com.reinto.borrowfox.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.reinto.borrowfox.Models.ChatMessage;
import com.reinto.borrowfox.R;

import java.util.List;

public class ChatConversationAdapter extends BaseAdapter {

    private static final int TYPE_ME = 0;
    private static final int TYPE_HIM = 1;
    private static final int DATE = 2;

    private Context context;
    private String userName;
    private List<ChatMessage> list;

    public ChatConversationAdapter(Context context, List<ChatMessage> list, String userName) {
        this.context = context;
        this.list = list;
        this.userName = userName;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int rowType = getItemViewType(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (rowType) {
                case TYPE_ME:
                    convertView = inflater.inflate(R.layout.layout_chat_me, parent, false);
                    MessageMe holder = new MessageMe();
                    holder.message = (TextView) convertView.findViewById(R.id.message);
                    convertView.setTag(R.layout.layout_chat_me, holder);
                    break;

                case TYPE_HIM:
                    convertView = inflater.inflate(R.layout.layout_chat_him, parent, false);
                    MessageHer holder2 = new MessageHer();
                    holder2.message = (TextView) convertView.findViewById(R.id.message);
                    holder2.name = (TextView) convertView.findViewById(R.id.name);
                    convertView.setTag(R.layout.layout_chat_him, holder2);
                    break;
                case DATE:
                    convertView = inflater.inflate(R.layout.layout_chat_date, parent, false);
                    DateItem holder3 = new DateItem();
                    holder3.date = (TextView) convertView.findViewById(R.id.date);
                    convertView.setTag(R.layout.layout_chat_date, holder3);
                    break;
            }
        }

        switch (rowType) {
            case TYPE_ME:
                MessageMe holder = (MessageMe) convertView.getTag(R.layout.layout_chat_me);
                holder.message.setText(list.get(position).getMessage());
                break;
            case TYPE_HIM:
                MessageHer barBarHolder = (MessageHer) convertView.getTag(R.layout.layout_chat_him);
                barBarHolder.message.setText(list.get(position).getMessage());
                barBarHolder.name.setText(userName);
                break;
            case DATE:
                DateItem date = (DateItem) convertView.getTag(R.layout.layout_chat_date);
                date.date.setText(list.get(position).getTime_string());
                break;
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()) {
            case 0:
                return TYPE_ME;
            case 1:
                return TYPE_HIM;
            case 2:
                return DATE;
            default:
                return 0;
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    private static class MessageMe {
        TextView message;
    }

    private static class MessageHer {
        TextView message;
        TextView name;
    }

    private static class DateItem {
        TextView date;
    }
}
