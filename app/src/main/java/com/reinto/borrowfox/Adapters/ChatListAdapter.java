package com.reinto.borrowfox.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Models.ChatListItem;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.fragments.general.ChatFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatListAdapter extends BaseAdapter {

    public static final String LENDER_ID = "lender_id";
    public static final String ITEM_ID = "item_id";
    public static final String USER_NAME = "user_name";
    private Context context;
    private List<ChatListItem> chats;
    private ChatListItem backup;
    private int backupPosition;

    public ChatListAdapter(Context context, List<ChatListItem> chats) {
        this.context = context;
        this.chats = chats;
        backup = new ChatListItem();
    }

    public void addData(List<ChatListItem> chats){
        this.chats.addAll(chats);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public Object getItem(int i) {
        return chats.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_chat_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.address = (TextView) convertView.findViewById(R.id.address);
            viewHolder.time = (TextView) convertView.findViewById(R.id.time);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.delete = (ImageButton) convertView.findViewById(R.id.delete);
            convertView.setTag(R.layout.layout_chat_list_item, viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag(R.layout.layout_chat_list_item);
        }

        viewHolder.name.setText(chats.get(position).getName());
        viewHolder.address.setText(chats.get(position).getAddress());

        Date d;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            d = sdf.parse(chats.get(position).getTime());
            long event_time = d.getTime();

            viewHolder.time.setText(getCorrectTimeStringLong(event_time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Glide.with(context)
                .load(APIService.URL_BASE_DATA + chats.get(position).getImage_url())
                .centerCrop()
                .override(Utilities.convertDpToPixels(56, context), Utilities.convertDpToPixels(56, context))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.profile_default)
                .into(viewHolder.image);

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backupPosition = position;
                backup = chats.get(position);
                chats.remove(position);
                notifyDataSetChanged();

                Snackbar snackbar = Snackbar.make(parent, R.string.item_deleted, Snackbar.LENGTH_LONG);
                snackbar.setAction(R.string.take_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chats.add(backupPosition, backup);
                        notifyDataSetChanged();
                    }
                });

                snackbar.setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                            DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
                            downloadAPIService.disableChat(backup.getChat_id(),
                                    Utilities.getUserId(context), new TaskDoneListener<Boolean>() {
                                        @Override
                                        public void OnTaskDone(Boolean object) {
                                            if (object) {
                                                Toast.makeText(context, R.string.chat_was_hidden, Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(context, R.string.error_hide_chat, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }

                        super.onDismissed(snackbar, event);
                    }
                });

                snackbar.setActionTextColor(context.getResources().getColor(R.color.button_red));
                snackbar.show();
            }
        });

        convertView.setOnTouchListener(new View.OnTouchListener() {

            int mov = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        if (mov < 5) {
                            setupFragmentWithInteger(ChatFragment.class, chats.get(position).getLender_id(),
                                    chats.get(position).getItem_id(), chats.get(position).getFull_name());
                        }

                        mov = 0;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mov++;
                        break;
                }
                return false;
            }
        });

        return convertView;
    }

    public void setupFragmentWithInteger(Class c, int lender_id, int item_id, String full_name) {
        String tag = c.getName() + item_id;
        try {
            Fragment f = (Fragment) c.newInstance();
            Bundle bundle = new Bundle();
            bundle.putInt(LENDER_ID, lender_id);
            bundle.putInt(ITEM_ID, item_id);
            bundle.putString(USER_NAME, full_name);
            f.setArguments(bundle);

            ((MainActivity) context).getSupportFragmentManager().beginTransaction()
                    .addToBackStack(tag)
                    .replace(R.id.content, f)
                    .commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private String getCorrectTimeStringLong(long x) {
        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(x);
        Calendar c = Calendar.getInstance();

        // today case
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return c2.get(Calendar.HOUR_OF_DAY) + ":" + c2.get(Calendar.MINUTE);
                }
            }
        }

        // yesterday case
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH) + 1) {
                    return context.getString(R.string.yesterday);
                }
            }
        }

        // days from last week until week ago
        if (c.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c.get(Calendar.DAY_OF_MONTH) > c2.get(Calendar.DAY_OF_MONTH) + 1 && c.get(Calendar.DAY_OF_MONTH) < c2.get(Calendar.DAY_OF_MONTH) + 7) {
                    int temp = c2.get(Calendar.DAY_OF_WEEK);
                    return context.getResources().getStringArray(R.array.days_in_week)[temp - 1];
                }
            }
        }
        return c2.get(Calendar.DAY_OF_MONTH) + "/" + (c2.get(Calendar.MONTH) + 1) + "/" + c2.get(Calendar.YEAR);
    }

    private static class ViewHolder {
        TextView name;
        TextView address;
        TextView time;
        ImageView image;
        ImageButton delete;
    }
}
