package com.reinto.borrowfox.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.LendData;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerBoolean;
import com.reinto.borrowfox.Services.TaskDoneListener;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.fragments.general.EditItemFragment;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.fragments.general.MyProfileFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LendAdapter extends BaseAdapter {

    private static final int ITEM_REQUESTED = Emuns.LendItemType.LEND_REQUESTED;
    private static final int ITEM_ON_LOAN = Emuns.LendItemType.LEND_ON_LOAN;
    private static final int ITEM_AVAILABLE = Emuns.LendItemType.LEND_AVAILABLE;
    private static final int ITEM_OFFLINE = Emuns.LendItemType.LEND_OFFLINE;

    private Context mContext;
    private List<LendData> mData;

    public LendAdapter(Context context, List<LendData> data) {
        mContext = context;
        mData = data;
    }

    public void addData(List<LendData> data){
        this.mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int rowType = getItemViewType(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (rowType) {
                case ITEM_AVAILABLE:
                    convertView = inflater.inflate(R.layout.layout_my_lends_item_item_available, parent, false);
                    ItemAvailableViewHolder available = new ItemAvailableViewHolder();
                    available.name = (TextView) convertView.findViewById(R.id.item_available_name);
                    available.locality = (TextView) convertView.findViewById(R.id.item_available_location);
                    available.price = (TextView) convertView.findViewById(R.id.item_available_price);
                    available.image = (CircleImageView) convertView.findViewById(R.id.item_available_circle);
                    available.itemImage = (ImageView) convertView.findViewById(R.id.item_available_item_image);
                    available.pricePerDay = (TextView) convertView.findViewById(R.id.item_available_complete_price);
                    available.additionalPrice = (TextView) convertView.findViewById(R.id.item_available_extra_day);
                    available.deposit = (TextView) convertView.findViewById(R.id.item_available_deposit);
                    available.delivery = (TextView) convertView.findViewById(R.id.item_available_delivery);
                    available.booking = (TextView) convertView.findViewById(R.id.item_available_booking);
                    available.edit = (ImageButton) convertView.findViewById(R.id.item_available_edit);

                    convertView.setTag(R.layout.layout_my_lends_item_item_available, available);
                    break;

                case ITEM_OFFLINE:
                    convertView = inflater.inflate(R.layout.layout_my_lends_item_item_offline, parent, false);
                    ItemOfflineViewHolder offline = new ItemOfflineViewHolder();
                    offline.name = (TextView) convertView.findViewById(R.id.item_offline_name);
                    offline.locality = (TextView) convertView.findViewById(R.id.item_offline_location);
                    offline.price = (TextView) convertView.findViewById(R.id.item_offline_price);
                    offline.image = (CircleImageView) convertView.findViewById(R.id.item_offline_user_image);
                    offline.itemImage = (ImageView) convertView.findViewById(R.id.item_offline_item_image);
                    offline.pricePerDay = (TextView) convertView.findViewById(R.id.item_offline_price2);
                    offline.additionalPrice = (TextView) convertView.findViewById(R.id.item_offline_extra_day);
                    offline.deposit = (TextView) convertView.findViewById(R.id.item_offline_deposit);
                    offline.lastBorrowed = (TextView) convertView.findViewById(R.id.item_offline_last_borrowed);
                    offline.edit = (ImageButton)convertView.findViewById(R.id.item_offline_edit);

                    convertView.setTag(R.layout.layout_my_lends_item_item_offline, offline);
                    break;

                case ITEM_REQUESTED:
                    convertView = inflater.inflate(R.layout.layout_my_lends_item_item_requested, parent, false);
                    ItemRequestedViewHolder requested = new ItemRequestedViewHolder();
                    requested.name = (TextView) convertView.findViewById(R.id.item_requested_name);
                    requested.locality = (TextView) convertView.findViewById(R.id.item_requested_location);
                    requested.price = (TextView) convertView.findViewById(R.id.item_requested_price);
                    requested.image = (CircleImageView) convertView.findViewById(R.id.item_requested_circle);
                    requested.itemImage = (ImageView) convertView.findViewById(R.id.item_requested_item_image);
                    requested.pricePerDay = (TextView) convertView.findViewById(R.id.item_requested_price_for_length);
                    requested.deposit = (TextView) convertView.findViewById(R.id.item_requested_deposit);
                    requested.length = (TextView) convertView.findViewById(R.id.item_requested_length);
                    requested.dates = (TextView) convertView.findViewById(R.id.item_requested_dates);
                    requested.nextBorrow = (TextView) convertView.findViewById(R.id.item_requested_next_borrow);
                    requested.accept = (Button) convertView.findViewById(R.id.item_requested_accept);
                    requested.cancel = (Button) convertView.findViewById(R.id.item_requested_decline);
                    requested.progressBar = (ProgressBar) convertView.findViewById(R.id.my_lends_item_item_requested_progressbar);
                    requested.edit = (ImageButton)convertView.findViewById(R.id.item_requested_edit);

                    convertView.setTag(R.layout.layout_my_lends_item_item_requested, requested);
                    break;

                case ITEM_ON_LOAN:
                    convertView = inflater.inflate(R.layout.layout_my_lends_item_on_loan, parent, false);
                    ItemOnLoanViewHolder onLoan = new ItemOnLoanViewHolder();
                    onLoan.name = (TextView) convertView.findViewById(R.id.item_on_loan_name);
                    onLoan.locality = (TextView) convertView.findViewById(R.id.item_on_loan_location);
                    onLoan.price = (TextView) convertView.findViewById(R.id.item_on_loan_image_price);
                    onLoan.image = (CircleImageView) convertView.findViewById(R.id.item_on_loan_circle);
                    onLoan.itemImage = (ImageView) convertView.findViewById(R.id.item_on_loan_image);
                    onLoan.deposit = (TextView) convertView.findViewById(R.id.item_on_loan_deposit);
                    onLoan.nextBorrow = (TextView) convertView.findViewById(R.id.item_on_loan_next_borrow);
                    onLoan.duetoReturn = (TextView) convertView.findViewById(R.id.item_on_loan_duetoreturn);
                    onLoan.returnType = (TextView) convertView.findViewById(R.id.item_on_loan_return);
                    onLoan.receivedMoney = (TextView) convertView.findViewById(R.id.item_on_loan_received);
                    onLoan.edit = (ImageButton) convertView.findViewById(R.id.item_on_loan_image_edit);
                    onLoan.messageBorrower = (Button) convertView.findViewById(R.id.layout_my_lends_item_on_loan_message_borrower);

                    convertView.setTag(R.layout.layout_my_lends_item_on_loan, onLoan);
                    break;
            }
        }

        switch (rowType) {

            ///////////////////////////////////////////////////////////////////////////
            // ITEM AVAILABLE
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_AVAILABLE:
                final ItemAvailableViewHolder available = (ItemAvailableViewHolder) convertView.getTag(R.layout.layout_my_lends_item_item_available);
                available.name.setText(mData.get(position).getName());
                available.locality.setText(Utilities.getUserLocation(mContext));
                available.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(available.itemImage);

                Glide.clear(available.image);
                Glide.with(mContext)
                        .load(Utilities.getFbImage(mContext))
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                available.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(available.image);

                available.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(MyProfileFragment.newInstance());
                    }
                });

                available.pricePerDay.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_day(), " /Per Day"));
                available.additionalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_extra_day(), " /Per Extra Day"));
                available.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), " /Deposit"));

                String latestDate = Utilities.getLatestDate(
                        mData.get(position).getLoanEventBegin(),
                        mData.get(position).getRequestedEventBegin(),
                        mData.get(position).getReturnedEventBegin(),
                        mData.get(position).getAcceptedEventBegin()
                );

                if (latestDate.equals("0")) {
                    available.booking.setText(mContext.getString(R.string.no_booking));
                } else {
                    available.booking.setText(mContext.getString(R.string.next_booking) + " " + latestDate);
                }

                if (mData.get(position).getDelivery() == 1) {
                    available.delivery.setText(R.string.delivery_courrier);
                } else {
                    available.delivery.setText(R.string.pick_up_only);
                }

                available.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(EditItemFragment.newInstance(mData.get(position).getId()));
                    }
                });

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM OFFLINE
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_OFFLINE:
                final ItemOfflineViewHolder offline = (ItemOfflineViewHolder) convertView.getTag(R.layout.layout_my_lends_item_item_offline);
                offline.name.setText(mData.get(position).getName());
                offline.locality.setText(Utilities.getUserLocation(mContext));
                offline.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(offline.itemImage);

                Glide.clear(offline.image);
                Glide.with(mContext)
                        .load(Utilities.getFbImage(mContext))
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                offline.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(offline.image);

                offline.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(MyProfileFragment.newInstance());
                    }
                });

                offline.pricePerDay.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_day(), mContext.getString(R.string.slash_per_day)));
                offline.additionalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_extra_day(), mContext.getString(R.string.slash_per_extra_day)));
                offline.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), mContext.getString(R.string.slash_deposit2)));

                if (mData.get(position).getReturnedEventEnd() == 0) {
                    offline.lastBorrowed.setText(R.string.never_borrowed);
                } else {
                    offline.lastBorrowed.setText(Utilities.getFormatedDateFromLong(mData.get(position).getReturnedEventEnd()));
                }

                offline.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(EditItemFragment.newInstance(mData.get(position).getId()));
                    }
                });

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM REQUESTED
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_REQUESTED:
                final ItemRequestedViewHolder requested = (ItemRequestedViewHolder) convertView.getTag(R.layout.layout_my_lends_item_item_requested);
                requested.name.setText(mData.get(position).getName());
                requested.locality.setText(Utilities.getUserLocation(mContext));
                requested.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(requested.itemImage);

                Glide.clear(requested.image);
                Glide.with(mContext)
                        .load(Utilities.getFbImage(mContext))
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                requested.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(requested.image);

                requested.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(MyProfileFragment.newInstance());
                    }
                });

                int calcucatedDays = calculateNumberOfDays(position);

                requested.pricePerDay.setText(Utilities.getFormatedPrice(calculatePrice(position),
                        mContext.getString(R.string.slash_for) + mContext.getResources().getQuantityString(R.plurals.day, calcucatedDays, calcucatedDays)));
                requested.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), mContext.getString(R.string.slash_deposit)));
                requested.length.setText(mContext.getString(R.string.requested_for) + mContext.getResources()
                        .getQuantityString(R.plurals.day, calcucatedDays, calcucatedDays));
                requested.dates.setText(Utilities.getFormatedDateFromLong(mData.get(position).getRequestedEventBegin()) + " - " +
                        Utilities.getFormatedDateFromLong(mData.get(position).getRequestedEventEnd()));

                if (mData.get(position).getReturnedEventEnd() == 0) {
                    requested.nextBorrow.setText(mContext.getString(R.string.never_borrowed));
                } else {
                    requested.nextBorrow.setText(Utilities.getFormatedDateFromLong(mData.get(position).getReturnedEventEnd()));
                }

                requested.progressBar.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.SRC_ATOP);
                requested.accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requested.progressBar.setVisibility(View.VISIBLE);
                        requested.accept.setVisibility(View.INVISIBLE);
                        requested.cancel.setVisibility(View.INVISIBLE);

                        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
                        downloadAPIService.acceptEvent(mData.get(position).getRequestedEventId(), new TaskDoneListener<Boolean>() {
                            @Override
                            public void OnTaskDone(Boolean object) {
                                requested.progressBar.setVisibility(View.GONE);
                                requested.accept.setVisibility(View.VISIBLE);
                                requested.cancel.setVisibility(View.VISIBLE);

                                if (!object) {
                                    Toast.makeText(mContext, R.string.error_accept_request, Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                Toast.makeText(mContext, R.string.request_accepted, Toast.LENGTH_SHORT).show();
                                FragManager.get().setFragment(HomeFragment.newInstance());
                            }
                        });
                    }
                });

                requested.cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requested.progressBar.setVisibility(View.VISIBLE);
                        requested.accept.setVisibility(View.INVISIBLE);
                        requested.cancel.setVisibility(View.INVISIBLE);

                        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();

                        downloadAPIService.cancelEvent(mData.get(position).getRequestedEventId(), new ResultListenerBoolean<Boolean>() {
                            @Override
                            public void onSuccess() {
                                requested.progressBar.setVisibility(View.GONE);
                                requested.accept.setVisibility(View.VISIBLE);
                                requested.cancel.setVisibility(View.VISIBLE);
                                Toast.makeText(mContext, R.string.request_canceled, Toast.LENGTH_SHORT).show();
                                FragManager.get().setFragment(HomeFragment.newInstance());
                            }

                            @Override
                            public void onFail() {
                                requested.progressBar.setVisibility(View.GONE);
                                requested.accept.setVisibility(View.VISIBLE);
                                requested.cancel.setVisibility(View.VISIBLE);
                                Toast.makeText(mContext, R.string.error_canceling_request, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                requested.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(EditItemFragment.newInstance(mData.get(position).getId()));
                    }
                });

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM ON LOAN
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_ON_LOAN:
                final ItemOnLoanViewHolder onLoan = (ItemOnLoanViewHolder) convertView.getTag(R.layout.layout_my_lends_item_on_loan);
                onLoan.name.setText(mData.get(position).getName());
                onLoan.locality.setText(Utilities.getUserLocation(mContext));
                onLoan.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(onLoan.itemImage);

                Glide.clear(onLoan.image);
                Glide.with(mContext)
                        .load(Utilities.getFbImage(mContext))
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                onLoan.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(onLoan.image);

                onLoan.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(MyProfileFragment.newInstance());
                    }
                });

                onLoan.duetoReturn.setText(mContext.getString(R.string.due_to_return) + Utilities.getFormatedDateFromLong(mData.get(position).getLoanEventEnd()));
                onLoan.receivedMoney.setText(Utilities.getFormatedPrice(calculatePrice(position), mContext.getString(R.string.slash_received)));
                onLoan.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), mContext.getString(R.string.slash_deposit_held)));

                if (mData.get(position).getAcceptedEventBegin() < System.currentTimeMillis()) {
                    onLoan.nextBorrow.setText(mContext.getString(R.string.no_booking));
                } else {
                    onLoan.nextBorrow.setText(mContext.getString(R.string.next_booking) + " " + Utilities.getFormatedDateFromLong(mData.get(position).getAcceptedEventBegin()));
                }

                onLoan.messageBorrower.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //FragManager.get().setFragment(ChatFragment.newInstance(mData.get(position).get));
                        // TODO connect to chat
                    }
                });

                if (mData.get(position).getDelivery() == Emuns.DeliveryType.PICK_UP) {
                    onLoan.returnType.setText(R.string.personal_pick_up);
                } else {
                    onLoan.returnType.setText(R.string.return_proff);
                }

                onLoan.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(EditItemFragment.newInstance(mData.get(position).getId()));
                    }
                });

                break;
        }

        return convertView;
    }

    private int calculateNumberOfDays(int position) {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(mData.get(position).getRequestedEventBegin());

        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(mData.get(position).getRequestedEventEnd());

        long diff = end.getTimeInMillis() - start.getTimeInMillis();

        return (int) (diff / (1000 * 60 * 60 * 24));
    }

    private int calculatePrice(int position) {
        int numberOfDays = calculateNumberOfDays(position);
        int minimalRentalPeriod = mData.get(position).getMinimum_rental_period();
        int pricePerDay = mData.get(position).getPrice_per_day();

        return pricePerDay * minimalRentalPeriod + (numberOfDays - minimalRentalPeriod) * mData.get(position).getPrice_per_extra_day();
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        switch (mData.get(position).getStatus()) {
            case 1:
                return ITEM_REQUESTED;
            case 5:
                return ITEM_ON_LOAN;
            case 0:
                return ITEM_AVAILABLE;
            case 2:
                return ITEM_OFFLINE;
            default:
                return ITEM_REQUESTED;
        }
    }

    private static class ItemRequestedViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        TextView pricePerDay;
        TextView deposit;
        TextView length;
        TextView dates;
        TextView nextBorrow;
        ImageButton edit;

        Button accept;
        Button cancel;
        ProgressBar progressBar;
    }

    private static class ItemAvailableViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        TextView pricePerDay;
        TextView additionalPrice;
        TextView deposit;
        TextView delivery;
        TextView booking;
        ImageButton edit;
    }

    private static class ItemOfflineViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        TextView pricePerDay;
        TextView additionalPrice;
        TextView deposit;
        TextView lastBorrowed;
        ImageButton edit;
    }

    private static class ItemOnLoanViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        TextView duetoReturn;
        TextView returnType;
        TextView receivedMoney;
        TextView deposit;
        TextView nextBorrow;
        ImageButton edit;

        Button messageBorrower;
    }
}