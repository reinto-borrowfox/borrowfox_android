package com.reinto.borrowfox.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.BorrowData;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerBoolean;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.fragments.general.ItemDetailFragment;
import com.reinto.borrowfox.fragments.general.NotMyProfileFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BorrowAdapter extends BaseAdapter {

    private static final int ITEM_REQUESTED = Emuns.BorrowItemType.BORROW_REQUESTED;
    private static final int ITEM_CURRENTLY_BORROWING = Emuns.BorrowItemType.BORROW_BORROWING;
    private static final int ITEM_BOOKED = Emuns.BorrowItemType.BORROW_BOOKED;
    private static final int ITEM_PREV_BORROWED = Emuns.BorrowItemType.BORROW_PREV_BORROWED;

    private Context mContext;
    private List<BorrowData> mData;

    public BorrowAdapter(Context context, List<BorrowData> data) {
        mContext = context;
        mData = data;
    }

    public void addData(List<BorrowData> data){
        this.mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int rowType = getItemViewType(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (rowType) {
                case ITEM_REQUESTED:
                    convertView = inflater.inflate(R.layout.layout_my_borrows_item_requested, parent, false);
                    RequesteditemViewHolder requested = new RequesteditemViewHolder();
                    requested.name = (TextView) convertView.findViewById(R.id.borrow_name);
                    requested.locality = (TextView) convertView.findViewById(R.id.borrow_location);
                    requested.price = (TextView) convertView.findViewById(R.id.borrow_price);
                    requested.image = (CircleImageView) convertView.findViewById(R.id.borrow_circle);
                    requested.itemImage = (ImageView) convertView.findViewById(R.id.borrow_image);
                    requested.deposit = (TextView) convertView.findViewById(R.id.borrow_deposit_price);
                    requested.totalPrice = (TextView) convertView.findViewById(R.id.borrow_complete_price);
                    requested.messageLender = (Button) convertView.findViewById(R.id.borrow_chat);
                    requested.dateFromTo = (TextView) convertView.findViewById(R.id.borrow_rental_interval);
                    requested.requestedFor = (TextView) convertView.findViewById(R.id.borrow_requested_x_days);
                    requested.cancel = (Button) convertView.findViewById(R.id.borrow_cancel_request);

                    convertView.setTag(R.layout.layout_my_borrows_item_requested, requested);
                    break;

                case ITEM_CURRENTLY_BORROWING:
                    convertView = inflater.inflate(R.layout.layout_my_borrows_currently_borrowing, parent, false);
                    CurrentlyBowViewHolder currBorrowed = new CurrentlyBowViewHolder();
                    currBorrowed.name = (TextView) convertView.findViewById(R.id.my_borrows_name);
                    currBorrowed.locality = (TextView) convertView.findViewById(R.id.my_borrows_location);
                    currBorrowed.price = (TextView) convertView.findViewById(R.id.my_borrows_price);
                    currBorrowed.image = (CircleImageView) convertView.findViewById(R.id.my_borrows_circle);
                    currBorrowed.itemImage = (ImageView) convertView.findViewById(R.id.my_borrows_image_name);
                    currBorrowed.deposit = (TextView) convertView.findViewById(R.id.my_borrows_deposit_price);
                    currBorrowed.totalPrice = (TextView) convertView.findViewById(R.id.my_borrows_complete_price);
                    currBorrowed.messageLender = (Button) convertView.findViewById(R.id.my_borrows_chat);
                    currBorrowed.returnDate = (TextView) convertView.findViewById(R.id.my_borrows_return_date);
                    currBorrowed.returnCourier = (TextView) convertView.findViewById(R.id.my_borrows_return_courrier);

                    convertView.setTag(R.layout.layout_my_borrows_currently_borrowing, currBorrowed);
                    break;

                case ITEM_BOOKED:
                    convertView = inflater.inflate(R.layout.layout_my_borrows_item_booked, parent, false);
                    ItemBookedViewHolder booked = new ItemBookedViewHolder();
                    booked.name = (TextView) convertView.findViewById(R.id.item_booked_name);
                    booked.locality = (TextView) convertView.findViewById(R.id.item_booked_location);
                    booked.price = (TextView) convertView.findViewById(R.id.item_booked_price);
                    booked.image = (CircleImageView) convertView.findViewById(R.id.item_booked_circle);
                    booked.itemImage = (ImageView) convertView.findViewById(R.id.item_booked_image);
                    booked.deposit = (TextView) convertView.findViewById(R.id.item_booked_deposit_price);
                    booked.totalPrice = (TextView) convertView.findViewById(R.id.item_booked_complete_price);
                    booked.messageLender = (Button) convertView.findViewById(R.id.item_booked_chat);
                    booked.dateFromTo = (TextView) convertView.findViewById(R.id.item_booked_book_interval);
                    booked.pickUpFromLender = (TextView) convertView.findViewById(R.id.item_booked_pick_from_lender);

                    convertView.setTag(R.layout.layout_my_borrows_item_booked, booked);
                    break;
                case ITEM_PREV_BORROWED:
                    convertView = inflater.inflate(R.layout.layout_my_borrows_previously_borrowed, parent, false);
                    ItemPrBorrowedVH prevBorrowed = new ItemPrBorrowedVH();
                    prevBorrowed.name = (TextView) convertView.findViewById(R.id.item_prev_b_name);
                    prevBorrowed.locality = (TextView) convertView.findViewById(R.id.item_prev_b_location);
                    prevBorrowed.price = (TextView) convertView.findViewById(R.id.item_prev_b_price);
                    prevBorrowed.image = (CircleImageView) convertView.findViewById(R.id.item_prev_b_circle);
                    prevBorrowed.itemImage = (ImageView) convertView.findViewById(R.id.item_prev_b_image);
                    prevBorrowed.deposit = (TextView) convertView.findViewById(R.id.item_prev_b_deposit_price);
                    prevBorrowed.perExtraDay = (TextView) convertView.findViewById(R.id.item_prev_b_per_extra_day);
                    prevBorrowed.totalPrice = (TextView) convertView.findViewById(R.id.item_prev_b_complete_price);
                    prevBorrowed.messageLender = (Button) convertView.findViewById(R.id.item_prev_b_chat);
                    prevBorrowed.lastBorrowed = (TextView) convertView.findViewById(R.id.item_prev_b_last_borrowed);
                    prevBorrowed.requestAgain = (Button) convertView.findViewById(R.id.item_prev_b_request_again);

                    convertView.setTag(R.layout.layout_my_borrows_previously_borrowed, prevBorrowed);
                    break;
            }
        }

        switch (rowType) {

            ///////////////////////////////////////////////////////////////////////////
            // ITEM AVAILABLE
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_REQUESTED:
                final RequesteditemViewHolder requested = (RequesteditemViewHolder) convertView.getTag(R.layout.layout_my_borrows_item_requested);
                requested.name.setText(mData.get(position).getName());
                requested.locality.setText(mData.get(position).getUserLocality());
                requested.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(requested.itemImage);

                Glide.clear(requested.image);
                Glide.with(mContext)
                        .load(APIService.URL_BASE_DATA + mData.get(position).getUserImageUrl())
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                requested.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(requested.image);
                requested.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(NotMyProfileFragment.newInstance(mData.get(position).getLenderId()));
                    }
                });

                requested.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), mContext.getString(R.string.slash_deposit_held)));

                requested.messageLender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO connect to chat
                    }
                });

                int numberOfDays = calculateNumberOfDays(mData.get(position).getEventBegin(), mData.get(position).getEventEnd());

                requested.totalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_day(), mContext.getString(R.string.slash_total) +
                        (numberOfDays + 1) + mContext.getString(R.string.day_rental)));

                requested.requestedFor.setText(mContext.getString(R.string.requested) + mContext.getResources().getQuantityString(
                        R.plurals.day, numberOfDays, numberOfDays));

                requested.dateFromTo.setText(Utilities.getFormatedDateFromLong(mData.get(position).getEventBegin()) + " - " +
                        Utilities.getFormatedDateFromLong(mData.get(position).getEventEnd()));

                requested.cancel.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        DownloadAPIService downloadAPIService = Bootstrapper.getDownloadAPIService();
                        downloadAPIService.cancelEvent(mData.get(position).getEventId(), new ResultListenerBoolean<Boolean>() {

                            @Override
                            public void onSuccess() {
                                Toast.makeText(mContext, R.string.request_success_canceled, Toast.LENGTH_SHORT).show();
                                FragManager.get().pop(HomeFragment.class.getName());
                            }

                            @Override
                            public void onFail() {
                                Toast.makeText(mContext, R.string.eror_canceling_request, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM CURRENTLY BORROWING
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_CURRENTLY_BORROWING:
                final CurrentlyBowViewHolder currentlyBorrowing = (CurrentlyBowViewHolder) convertView.getTag(R.layout.layout_my_borrows_currently_borrowing);
                currentlyBorrowing.name.setText(mData.get(position).getName());
                currentlyBorrowing.locality.setText(mData.get(position).getUserLocality());
                currentlyBorrowing.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(currentlyBorrowing.itemImage);

                Glide.clear(currentlyBorrowing.image);
                Glide.with(mContext)
                        .load(APIService.URL_BASE_DATA + mData.get(position).getUserImageUrl())
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                currentlyBorrowing.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(currentlyBorrowing.image);
                currentlyBorrowing.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(NotMyProfileFragment.newInstance(mData.get(position).getLenderId()));
                    }
                });

                int numberOfDays2 = calculateNumberOfDays(mData.get(position).getEventBegin(), mData.get(position).getEventEnd());

                currentlyBorrowing.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(),
                        mContext.getString(R.string.slash_deposit_held)));
                currentlyBorrowing.totalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_day(),
                        mContext.getString(R.string.slash_total) +
                                (numberOfDays2 + 1) + mContext.getString(R.string.day_rental)));

                currentlyBorrowing.messageLender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO connect to chat
                    }
                });

                currentlyBorrowing.returnDate.setText(Utilities.getFormatedColoredDateFromLong(mContext.getString(R.string.return_date),
                        mData.get(position).getEventEnd(), R.color.button_red));

                // TODO currentlyBorrowing.returnCourier

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM BOOKED
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_BOOKED:
                final ItemBookedViewHolder booked = (ItemBookedViewHolder) convertView.getTag(R.layout.layout_my_borrows_item_booked);
                booked.name.setText(mData.get(position).getName());
                booked.locality.setText(mData.get(position).getUserLocality());
                booked.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(booked.itemImage);

                Glide.clear(booked.image);
                Glide.with(mContext)
                        .load(APIService.URL_BASE_DATA + mData.get(position).getUserImageUrl())
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                booked.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(booked.image);
                booked.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(NotMyProfileFragment.newInstance(mData.get(position).getLenderId()));
                    }
                });

                booked.dateFromTo.setText(Utilities.getFormatedDateFromLong(mData.get(position).getEventBegin()) + " - " +
                        Utilities.getFormatedDateFromLong(mData.get(position).getEventEnd()));

                // TODO times
                booked.pickUpFromLender.setText("Pick up from lender X-Xpm");

                booked.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(), ""));
                booked.totalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getEventBorrowPrice(), ""));

                booked.messageLender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO connect to chat
                    }
                });


                // TODO currentlyBorrowing.returnCourier

                break;

            ///////////////////////////////////////////////////////////////////////////
            // ITEM PREVIOSLY BORROWED
            ///////////////////////////////////////////////////////////////////////////

            case ITEM_PREV_BORROWED:
                final ItemPrBorrowedVH itemPrevBorrowed = (ItemPrBorrowedVH) convertView.getTag(R.layout.layout_my_borrows_previously_borrowed);
                itemPrevBorrowed.name.setText(mData.get(position).getName());
                itemPrevBorrowed.locality.setText(mData.get(position).getUserLocality());
                itemPrevBorrowed.price.setText(Utilities.getFormatedPrice(String.valueOf(mData.get(position).getPrice_per_day())));
                Glide.with(mContext).load(mData.get(position).getImageUrl())
                        .centerCrop().placeholder(R.drawable.profile_default).into(itemPrevBorrowed.itemImage);

                Glide.clear(itemPrevBorrowed.image);
                Glide.with(mContext)
                        .load(APIService.URL_BASE_DATA + mData.get(position).getUserImageUrl())
                        .centerCrop()
                        .override(Utilities.convertDpToPixels(56, mContext), Utilities.convertDpToPixels(56, mContext))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                itemPrevBorrowed.image.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .placeholder(R.drawable.profile_default)
                        .into(itemPrevBorrowed.image);
                itemPrevBorrowed.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(NotMyProfileFragment.newInstance(mData.get(position).getLenderId()));
                    }
                });

                itemPrevBorrowed.lastBorrowed.setText(Utilities.getFormatedColoredDateFromLong(mContext.getString(R.string.last_borrowed),
                        mData.get(position).getEventEnd(), R.color.dialog_grey));

                itemPrevBorrowed.deposit.setText(Utilities.getFormatedPrice(mData.get(position).getDeposit(),
                        mContext.getString(R.string.slash_deposit)));
                itemPrevBorrowed.totalPrice.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_day(),
                        mContext.getString(R.string.slash_per_day)));
                itemPrevBorrowed.perExtraDay.setText(Utilities.getFormatedPrice(mData.get(position).getPrice_per_extra_day(),
                        mContext.getString(R.string.slash_per_extra_day)));

                itemPrevBorrowed.messageLender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO connect to chat
                    }
                });

                itemPrevBorrowed.requestAgain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(ItemDetailFragment.newInstance(mData.get(position).getId(),false));
                    }
                });

                break;
        }

        return convertView;
    }

    private int calculateNumberOfDays(long t1, long t2) {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(t1);

        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(t2);

        long diff = end.getTimeInMillis() - start.getTimeInMillis();

        return (int) (diff / (1000 * 60 * 60 * 24));
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        switch (mData.get(position).getStatus()) {
            case 1:
                return ITEM_REQUESTED;
            case 4:
                return ITEM_BOOKED;
            case 5:
                return ITEM_CURRENTLY_BORROWING;
            case 6:
                return ITEM_PREV_BORROWED;
            default:
                return ITEM_REQUESTED;
        }
    }

    private class RequesteditemViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        TextView requestedFor;
        TextView dateFromTo;
        TextView totalPrice;
        TextView deposit;

        Button messageLender;
        Button cancel;
    }

    private class CurrentlyBowViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        Button messageLender;

        TextView returnDate;
        TextView returnCourier;
        TextView totalPrice;
        TextView deposit;
    }

    private class ItemBookedViewHolder {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        Button messageLender;

        TextView dateFromTo;
        TextView pickUpFromLender;
        TextView totalPrice;
        TextView deposit;
    }

    private class ItemPrBorrowedVH {
        TextView name;
        TextView locality;
        TextView price;
        CircleImageView image;
        ImageView itemImage;
        Button messageLender;

        TextView lastBorrowed;
        TextView totalPrice;
        TextView perExtraDay;
        TextView deposit;

        Button requestAgain;
    }
}