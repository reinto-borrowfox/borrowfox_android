package com.reinto.borrowfox.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.Models.GroupModel;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Services.APIService;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.Views.CircularIndicator;
import com.reinto.borrowfox.Views.ImageAdapter;
import com.reinto.borrowfox.fragments.general.CategoryDetailFragment;
import com.reinto.borrowfox.fragments.general.HomeFragment;
import com.reinto.borrowfox.fragments.general.ItemDetailFragment;
import com.reinto.borrowfox.managers.FragManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Reinto Workstation on 18.11.2016.
 */

public class SimpleGroupAdapter extends BaseAdapter implements Filterable {


    private static final int FOX = 0;
    private static final int CATEGORY = 1;
    public HashMap<Integer, Integer> viewPageStates = new HashMap<>();
    private Context context;
    private ArrayList<GroupModel> list;
    private ArrayList<GroupModel> backup;
    private CategoryFilter filter;

    private boolean isTipVisible = true;

    public SimpleGroupAdapter(Context context, ArrayList<GroupModel> list) {
        this.context = context;
        this.list = list;
        backup = new ArrayList<>(list);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int rowType = getItemViewType(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (rowType) {
                case FOX:
                    convertView = inflater.inflate(R.layout.layout_foxs_pick, parent, false);
                    Fox holder = new Fox();
                    holder.tip = (TextView) convertView.findViewById(R.id.tip);
                    holder.name = (TextView) convertView.findViewById(R.id.name);
                    holder.price = (TextView) convertView.findViewById(R.id.item_available_price);
                    holder.location = (TextView) convertView.findViewById(R.id.location);
                    holder.image = (ViewPager) convertView.findViewById(R.id.item_available_item_image);
                    holder.indicator = (CircularIndicator) convertView.findViewById(R.id.rectangleIndicator);
                    holder.parentt = (RelativeLayout) convertView.findViewById(R.id.parentt);
                    convertView.setTag(R.layout.layout_foxs_pick, holder);
                    break;

                case CATEGORY:
                    convertView = inflater.inflate(R.layout.simple_category_layout, parent, false);
                    Category holder2 = new Category();
                    holder2.text = (TextView) convertView.findViewById(R.id.text);
                    holder2.image = (ImageView) convertView.findViewById(R.id.image);
                    convertView.setTag(R.layout.simple_category_layout, holder2);
                    break;
            }
        }

        switch (rowType) {
            case FOX:
                final Fox holder = (Fox) convertView.getTag(R.layout.layout_foxs_pick);

                if (isTipVisible) {
                    Fragment f = ((MainActivity) context).getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getName());
                    ((HomeFragment) f).tip = holder.tip;
                    isTipVisible = false;
                }

                holder.name.setText(list.get(position).getName());
                holder.location.setText(list.get(position).getLocation());
                holder.price.setText(Utilities.getFormatedPrice(list.get(position).getPrice()));

                ImageAdapter adapter = new ImageAdapter(context, list.get(position).getUrls());
                holder.image.setAdapter(adapter);
                holder.indicator.setNumber_of_pages(list.get(position).getUrls().size());

                adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(ItemDetailFragment.newInstance(list.get(position).getId(), false));
                    }
                });

                if (viewPageStates.get(position) == null) {
                    holder.indicator.setActive_index(0);
                } else {
                    holder.indicator.setActive_index(viewPageStates.get(position));
                }


                holder.indicator.setupUI();

                if (viewPageStates.get(position) == null) {
                    holder.image.setCurrentItem(0);
                } else {
                    holder.image.setCurrentItem(viewPageStates.get(position));
                }

                holder.parentt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(ItemDetailFragment.newInstance(list.get(position).getId(), false));
                    }
                });

                holder.image.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        holder.indicator.changeIndicator(position);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });

                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(ItemDetailFragment.newInstance(list.get(position).getId(), false));
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(ItemDetailFragment.newInstance(list.get(position).getId(), false));
                    }
                });

                break;
            case CATEGORY:
                final Category barBarHolder = (Category) convertView.getTag(R.layout.simple_category_layout);
                barBarHolder.text.setText(list.get(position).getName());
                Glide.with(context).load(APIService.URL_BASE_DATA + list.get(position).getImage_url()).centerCrop().into(barBarHolder.image);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragManager.get().setFragment(CategoryDetailFragment.newInstance(barBarHolder.text.getText().toString(),
                                list.get(position).getId()));
                    }
                });
                break;
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()) {
            case 0:
                return FOX;
            case 1:
                return CATEGORY;
            default:
                return 0;
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new CategoryFilter();

        return filter;
    }

    private static class Fox {
        TextView tip;
        TextView name;
        TextView location;
        TextView price;
        ViewPager image;
        CircularIndicator indicator;
        RelativeLayout parentt;
    }

    private static class Category {
        TextView text;
        ImageView image;
    }

    private class CategoryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                results.values = backup;
                results.count = backup.size();
            } else {
                List<GroupModel> nList = new ArrayList<>();

                for (int i = 0; i < backup.size(); i++) {
                    if (backup.get(i).getName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nList.add(backup.get(i));
                    }
                }

                results.values = nList;
                results.count = nList.size();

            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                list = (ArrayList<GroupModel>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
