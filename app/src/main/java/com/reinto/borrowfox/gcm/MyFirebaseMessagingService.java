package com.reinto.borrowfox.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.NotificationItem;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.fragments.general.ChatFragment;
import com.reinto.borrowfox.fragments.general.NotificationFragment;
import com.reinto.borrowfox.managers.FragManager;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "tag";

    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        try {
            final JSONObject message = new JSONObject(remoteMessage.getData());

            if (message.getInt("type") == 1) {
                // chat

                if (App.getInstance().getCurrentActivity() instanceof MainActivity) {
                    if (((MainActivity) App.getInstance().getCurrentActivity()).fragment.equals(ChatFragment.class)) {

                        final ChatFragment chat = (ChatFragment) ((MainActivity) App.getInstance().getCurrentActivity()).getSupportFragmentManager().findFragmentByTag(ChatFragment.class.getName());
                        Log.d(App.TAG, "MyFirebaseMessagingService, onMessageReceived: " + (chat == null));
                        if (chat.chatId == message.getInt("chatId")) {
                            App.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        chat.setMessage(message.getString("message"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            FragManager.get().setFragment(ChatFragment.newInstance(message.getInt("lenderId"), message.getInt("itemId"), message.getString("userName")));
                        }
                    } else {
                        createNotification(message);
                    }
                } else {
                    createNotification(message);
                }
            } else if (message.getInt("type") == 2) {
                ///////////////////////////////////////////////////////////////////////////
                // REQUESTS ETC.
                ///////////////////////////////////////////////////////////////////////////
                if (App.getInstance().getCurrentActivity() instanceof MainActivity) {
                    if (((MainActivity) App.getInstance().getCurrentActivity()).fragment.equals(NotificationFragment.class)) {
                        final NotificationFragment notif = (NotificationFragment) ((MainActivity) App.getInstance().getCurrentActivity()).getSupportFragmentManager().findFragmentByTag(NotificationFragment.class.getName());
                        Log.d(App.TAG, "MyFirebaseMessagingService, onMessageReceived: " + (notif == null));

                        if (notif != null) {
                            App.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        notif.addNotification(new NotificationItem(message.getString("itemName"), message.getString("message"), message.getInt("itemId"), message.getString("image_url"), String.valueOf(message.getLong("time"))));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                    } else {
                        createNotification(message);
                    }
                } else {
                    createNotification(message);
                }
            } else if (message.getInt("type") == 3) {
                //                // item requested
                //                if (App.getInstance().getCurrentActivity() instanceof MainActivity) {
                //                    if (((MainActivity) App.getInstance().getCurrentActivity()).fragment.equals(LendFragment.class)) {
                //                        final LendFragment lend = (LendFragment) ((MainActivity) App.getInstance().getCurrentActivity()).getSupportFragmentManager().findFragmentByTag(LendFragment.class.getName());
                //                        Log.d(App.TAG, "MyFirebaseMessagingService, onMessageReceived: " + (lend == null));
                //
                //                        if (lend != null) {
                //                            App.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                //                                @Override
                //                                public void run() {
                //                                    lend.setType(Emuns.LendItemType.LEND_REQUESTED);
                //                                }
                //                            });
                //                        }
                //                    } else {
                //                        createNotification(message);
                //                    }
                //                } else {
                //                }
                createNotification(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createNotification(JSONObject o) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("message", o.toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = null;

        Log.d(App.TAG, "MyFirebaseMessagingService, createNotification: " + o.toString());

        try {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(o.getString("message"))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}