package com.reinto.borrowfox.gcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Services.Bootstrapper;
import com.reinto.borrowfox.Services.DownloadAPIService;
import com.reinto.borrowfox.Services.ResultListenerBoolean;
import com.reinto.borrowfox.Utilities.Utilities;

public class MyFirebaseinstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TAG", "Refreshed token: " + refreshedToken);

        DownloadAPIService d = Bootstrapper.getDownloadAPIService();
        d.sendRegistrationToken(Utilities.getUserId(App.getInstance().getCurrentActivity()), refreshedToken, new ResultListenerBoolean() {
            @Override
            public void onSuccess() {
                Log.d(App.TAG, "MyFirebaseinstanceIdService, onSuccess: registration token success");
            }

            @Override
            public void onFail() {
                Log.d(App.TAG, "MyFirebaseinstanceIdService, onFail: reigstration token fail");
            }
        });
    }
}
