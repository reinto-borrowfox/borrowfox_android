package com.reinto.borrowfox.managers;

import android.util.Log;

import com.reinto.borrowfox.App;

import java.util.ArrayList;
import java.util.List;

public class CategoryManager {

    private List<String> names;
    private List<Integer> ids;

    public CategoryManager(List<String> names, List<Integer> ids) {
        this.names = names;
        this.ids = ids;
    }

    public CategoryManager() {
        names = new ArrayList<>();
        ids = new ArrayList<>();
    }

    public void addId(int id) {
        ids.add(id);
    }

    public void addName(String name) {
        names.add(name);
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public void removeMostPopular() {
        for (int i = 0; i < names.size(); i++) {
            Log.d(App.TAG, "CategoryManager, removeMostPopular: " + names.get(i));
            if (names.get(i).equals("Most Popular")) {
                names.remove(i);
                ids.remove(i);
            }
        }
    }

    public int getIndexForSpinner(int x) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == x) {
                return i;
            }
        }

        return 0;
    }

    public int getId(String name) {
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).equals(name)) {
                return ids.get(i);
            }
        }

        return -1;
    }

    public String getName(int id) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == id) {
                return names.get(i);
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "CategoryManager{" +
                "names=" + names +
                ", ids=" + ids +
                '}';
    }
}
