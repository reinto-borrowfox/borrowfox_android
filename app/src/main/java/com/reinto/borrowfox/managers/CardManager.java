package com.reinto.borrowfox.managers;

import java.util.ArrayList;
import java.util.List;

public class CardManager {

    private List<Integer> ids;
    private List<String> info;

    public CardManager() {
        ids = new ArrayList<>();
        info = new ArrayList<>();
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }

    public void putId(int id) {
        ids.add(id);
    }

    public void putInfo(String info) {
        this.info.add(info);
    }

    /**
     * @param x spinner position
     * @return corresponding card id
     */
    public int getIdFromSpinner(int x) {
        for (int i = 0; i < ids.size(); i++) {
            if (x == i) {
                return ids.get(i);
            }
        }

        return 0;
    }
}
