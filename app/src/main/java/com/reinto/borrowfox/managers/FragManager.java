package com.reinto.borrowfox.managers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.reinto.borrowfox.Activities.MainActivity;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.R;

public class FragManager {

    private static FragManager ref;
    private FragManager() {
    }

    public static FragManager get() {
        if (ref == null)
            ref = new FragManager();
        return ref;
    }

    public void setFragment(Fragment f, boolean clearBackStack) {
        if (clearBackStack) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        setFragment(f);
    }

    public void setFragment(Fragment f) {
        getFragmentManager().beginTransaction()
                .replace(R.id.content, f, f.getClass().getName())
                .addToBackStack(f.getClass().getName())
                .commit();
    }

    public void setFragmentControl(Class c){

        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
            Log.d(App.TAG, "FragManager, setFragmentControl: " + getFragmentManager().getBackStackEntryAt(i));
        }

        String tag = c.getName();
        Fragment f;

        f = getFragmentManager().findFragmentByTag(tag);

        if (f == null) {
            try {
                f = (Fragment) c.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            setFragment(f);
        } else {
            pop(tag);
        }
    }


    public void pop() {
        getFragmentManager().popBackStack();
    }

    public void pop(String tag) {
        getFragmentManager().popBackStackImmediate(tag, 0);
    }

    public FragmentManager getFragmentManager() {
        return ((MainActivity) App.getInstance().getCurrentActivity()).getSupportFragmentManager();
    }
}
