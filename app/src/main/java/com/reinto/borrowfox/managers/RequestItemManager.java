package com.reinto.borrowfox.managers;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Utilities.Utilities;

public class RequestItemManager {

    private static RequestItemManager manager;
    private RequestItem item;

    private RequestItemManager() {
    }

    public static RequestItemManager get() {
        if (manager == null) {
            manager = new RequestItemManager();
        }

        return manager;
    }

    public RequestItem getItem() {
        if (item == null) {
            item = new RequestItem();
            item.borrowUserId = Utilities.getUserId(App.getAppContext());
        }

        return item;
    }

    public void clear() {
        item = null;
    }

    public class RequestItem {
        public int itemId;
        public int borrowUserId;
        public int paymentCardId;
        public long beginDate;
        public long endDate;
        public long beginTime;
        public long endTime;
        public int deliveryType = 1;
        public int beginAddressId;
        public int endAddressId;

        @Override
        public String toString() {
            return "RequestItem{" +
                    "itemId=" + itemId +
                    ", borrowUserId=" + borrowUserId +
                    ", paymentCardId=" + paymentCardId +
                    ", beginDate=" + beginDate +
                    ", endDate=" + endDate +
                    ", beginTime=" + beginTime +
                    ", endTime=" + endTime +
                    ", deliveryType=" + deliveryType +
                    ", beginAddressId=" + beginAddressId +
                    ", endAddressId=" + endAddressId +
                    '}';
        }
    }
}