package com.reinto.borrowfox.managers;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Models.CalendaDayInfo;
import com.reinto.borrowfox.R;
import com.reinto.borrowfox.Utilities.Utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarManager {

    public List<CalendarItem> month_data;
    private Map<Integer, CalendarItem> map;
    private List<CalendaDayInfo> list;

    private List<CalendarItem> data;
    private int minimalRentalLength = 0;
    private Context context;
    private List<CalendarItem> selectedItems;
    private boolean selectionIsActive = false;

    private TextView from, to;

    public CalendarManager(Context context) {
        this.context = context;
        map = new HashMap<>();
        month_data = new ArrayList<>();
        selectedItems = new ArrayList<>();
        data = new ArrayList<>();
    }

    public void setTextviews(TextView a, TextView b) {
        from = a;
        to = b;
    }

    public void setMinimalRentalLength(int x) {
        minimalRentalLength = x;
    }

    private void reimportAPIData() {
        int temp = 0;
        Calendar c = Calendar.getInstance();

        for (int i = 0; i < list.size(); i++) {
            for (int j = temp; j < month_data.size(); j++) {
                c.setTimeInMillis(month_data.get(j).time);
                if (checkTheSameDay(c, list.get(i).getC())) {
                    month_data.get(j).type = list.get(i).getType();
                    temp = j;
                    break;
                }
            }
        }
    }

    public void importAPIData(List<CalendaDayInfo> list) {
        this.list = new ArrayList<>(list);
        int temp = 0;
        Calendar c = Calendar.getInstance();

        for (int i = 0; i < list.size(); i++) {
            for (int j = temp; j < month_data.size(); j++) {
                c.setTimeInMillis(month_data.get(j).time);
                if (checkTheSameDay(c, list.get(i).getC())) {
                    month_data.get(j).type = list.get(i).getType();
                    temp = j;
                    break;
                }
            }
        }

        analyzeBlockedDays();
        putSelectedItems();
        drawMonth();
        setOnClickListeners();
    }

    private void putSelectedItems() {
        int temp;

        for (int i = 0; i < selectedItems.size(); i++) {
            if (month_data.contains(selectedItems.get(i))) {
                temp = month_data.indexOf(selectedItems.get(i));
                month_data.get(temp).type = 4;
            }
        }
    }


    private void setOnClickListeners() {
        for (int i = 1; i < month_data.size() - 1; i++) {
            final int finalI = i;
            if (!selectionIsActive) {
                month_data.get(i).textview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (month_data.get(finalI).type == 0) {
                            selectionIsActive = true;
                            month_data.get(finalI).type = 4;
                            selectedItems.add(month_data.get(finalI));

                            for (int j = 1; j < minimalRentalLength; j++) {
                                month_data.get(finalI + j).type = 4;
                                selectedItems.add(month_data.get(finalI + j));
                            }
                        }
                        setOnClickListeners();
                    }
                });
            } else {
                month_data.get(i).textview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ((month_data.get(finalI - 1).type == 4 || month_data.get(finalI + 1).type == 4) &&
                                (month_data.get(finalI).type == 0 || month_data.get(finalI).type == 3)) {
                            // add more selected days

                            month_data.get(finalI).type = 4;
                            selectedItems.add(month_data.get(finalI));
                        } else if (month_data.get(finalI).type == 4 && (month_data.get(finalI - 1).type != 4 ||
                                month_data.get(finalI + 1).type != 4)) {
                            // removing border selected days

                            month_data.get(finalI).type = 0;
                            selectedItems.remove(month_data.get(finalI));

                            if (selectedItems.size() < minimalRentalLength) {
                                for (int i = 0; i < selectedItems.size(); i++) {
                                    selectedItems.get(i).type = 0;
                                }

                                selectionIsActive = false;
                                selectedItems.clear();
                            }
                        }
                        setOnClickListeners();
                    }
                });
            }
        }

        setupTextviews();

        reimportAPIData();
        analyzeBlockedDays();
        drawMonth();
    }

    private void setupTextviews() {
        if (selectedItems.size() == 0) {
            return;
        }

        Collections.sort(selectedItems, new Comparator<CalendarItem>() {
            @Override
            public int compare(CalendarItem calendarItem, CalendarItem t1) {
                return calendarItem.time > t1.time ? 1 : -1;
            }
        });

        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(selectedItems.get(0).time);

        String s1 = context.getResources().getStringArray(R.array.months)[c1.get(Calendar.MONTH)] + "\n" + fixDay(c1.get(Calendar.DAY_OF_MONTH));
        SpannableString span = new SpannableString(s1);
        span.setSpan(new RelativeSizeSpan(2f), s1.length() - 2, s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        from.setText(span);

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(selectedItems.get(selectedItems.size() - 1).time);

        String s2 = context.getResources().getStringArray(R.array.months)[c2.get(Calendar.MONTH)] + "\n" + fixDay(c2.get(Calendar.DAY_OF_MONTH));

        SpannableString span2 = new SpannableString(s2);
        span2.setSpan(new RelativeSizeSpan(2f), s2.length() - 2, s2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        to.setText(span2);
    }

    private String fixDay(int x) {
        if (x < 10) {
            return "0" + String.valueOf(x);
        } else {
            return String.valueOf(x);
        }
    }

    public void generateCalendar(int added_months) {
        // data.clear();
        month_data.clear();
        Calendar dynamic_today = Calendar.getInstance();
        dynamic_today.add(Calendar.MONTH, added_months);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, added_months);

        int diff = calendar.get(Calendar.DAY_OF_WEEK) - 2;
        if (diff == -1) diff = 6;
        calendar.setTimeInMillis(calendar.getTimeInMillis() - 1000 * 60 * 60 * 24 * diff);
        // month is the same
        while (dynamic_today.get(Calendar.MONTH) + 1 == calendar.get(Calendar.MONTH) + 1) {
            calendar.add(Calendar.DAY_OF_MONTH, -7);
        }

        calendar.add(Calendar.DAY_OF_MONTH, -7);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(Utilities.convertDpToPixels(30, context),
                Utilities.convertDpToPixels(30, context));
        params2.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;

        for (int i = 0; i < 56; i++) {
            TextView textView = new TextView(context);
            textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.black54, null));
            textView.setTextSize(Utilities.convertSpToPixels(6, context));
            textView.setLayoutParams(params2);
            textView.setGravity(Gravity.CENTER);
            textView.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));

            //  data.add(new CalendarItem(calendar.getTimeInMillis(), textView, 0));
            // map.put(map.size() + 1, new CalendarItem(calendar.getTimeInMillis(), textView, 0));
            month_data.add(new CalendarItem(calendar.getTimeInMillis(), textView, 0));

            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    public Map<Integer, CalendarItem> getMap() {
        return map;
    }

    public int getSize() {
        return data.size();
    }

    //    public void printData(List<CalendarItem> data) {
    //        for (int i = 0; i < data.size() - 7; i += 7) {
    //            data.get(i + 1).textview.getText().toString() + ", " + data.get(i + 2).textview.getText().toString() +
    //                    ", " + data.get(i + 3).textview.getText().toString() + ", " + data.get(i + 4).textview.getText().toString() +
    //                    ", " + data.get(i + 5).textview.getText().toString() + ", " + data.get(i + 6).textview.getText().toString();
    //
    //        }
    //    }

    private void analyzeBlockedDays() {
        for (int i = 1; i < month_data.size() - minimalRentalLength; i++) {

            // block any days between already blocked day and unvailable/booked day
//            if (month_data.get(i - 1).type == 3 && month_data.get(i).type == 0) {
//                month_data.get(i).type = 3;
//                continue;
//            }
//
            // skip booked, unavailable and selected
            if (month_data.get(i).type == 1 || month_data.get(i).type == 2 || month_data.get(i).type == 4) {
                continue;
            }

            if (month_data.get(i + minimalRentalLength - 1).type == 1 ||
                    month_data.get(i + minimalRentalLength - 1).type == 2) {
                month_data.get(i).type = 3;
            }
        }

        // past time
        Calendar cc = Calendar.getInstance();
        for (int i = 0; i < month_data.size(); i++) {
            Log.d(App.TAG, "CalendarManager, analyzeBlockedDays: 2-  " + Utilities.getFormatedDateFromLong(month_data.get(i).time));
            Log.d(App.TAG, "CalendarManager, analyzeBlockedDays: compareto " + Utilities.getFormatedDateFromLong(cc.getTimeInMillis()));
            if (month_data.get(i).time < cc.getTimeInMillis()) {
                if (!(month_data.get(i + minimalRentalLength - 1).type == 1 &&
                        month_data.get(i + minimalRentalLength - 1).type == 2)) {
                    if (!checkTheSameDay2(month_data.get(i).time, cc)) {
                        month_data.get(i).type = 3;
                        Log.d(App.TAG, "CalendarManager, analyzeBlockedDays: " + month_data.get(i).time);
                        Log.d(App.TAG, "CalendarManager, analyzeBlockedDays: " + Utilities.getFormatedDateFromLong(month_data.get(i).time));
                    }
                }
            }
        }
    }

    public void drawMonth() {
        for (int i = 0; i < month_data.size(); i++) {
            switch (month_data.get(i).type) {
                case 0: // normal
                    month_data.get(i).textview.setBackgroundResource(android.R.color.transparent);
                    month_data.get(i).textview.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.black54, null));
                    break;
                case 1: // booked
                    month_data.get(i).textview.setBackgroundResource(R.color.light_weird_green);
                    month_data.get(i).textview.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.black54, null));
                    month_data.get(i).textview.setOnClickListener(null);
                    month_data.get(i).textview.setEnabled(false);
                    break;
                case 2: // not available
                    month_data.get(i).textview.setBackgroundResource(R.color.black_not_ava);
                    month_data.get(i).textview.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.black54, null));
                    month_data.get(i).textview.setOnClickListener(null);
                    month_data.get(i).textview.setEnabled(false);
                    break;
                case 3: // blocked
                    month_data.get(i).textview.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.black54tra, null));
                    month_data.get(i).textview.setBackgroundResource(android.R.color.transparent);
                    break;
                case 4: // selected
                    month_data.get(i).textview.setBackgroundResource(R.color.button_red);
                    month_data.get(i).textview.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.white, null));
                    break;
            }
        }
    }

    private boolean checkTheSameDay(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean checkTheSameDay2(long time, Calendar c2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(time);

        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
            if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)) {
                if (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "CalendarManager{" +
                "data=" + data +
                '}';
    }

    public List<Long> getBorderDays() {
        if (selectedItems.size() == 0) {
            return null;
        }

        List<Long> list = new ArrayList<>();

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(selectedItems.get(0).time);
        Log.d(App.TAG, "getBorderDays: " + c.get(Calendar.DAY_OF_MONTH) + ", " + c.get(Calendar.MONTH) + ", " + c.get(Calendar.YEAR));

        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(selectedItems.get(selectedItems.size() - 1).time);
        Log.d(App.TAG, "getBorderDays: " + c2.get(Calendar.DAY_OF_MONTH) + ", " + c2.get(Calendar.MONTH) + ", " + c2.get(Calendar.YEAR));

        list.add(selectedItems.get(0).time);
        list.add(selectedItems.get(selectedItems.size() - 1).time);
        return list;
    }

    public class CalendarItem {

        public long time;
        public TextView textview;
        int type;

        public CalendarItem(long time, TextView textview, int type) {
            this.time = time;
            this.textview = textview;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof CalendarItem) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(time);

                Calendar cc = Calendar.getInstance();
                cc.setTimeInMillis(((CalendarItem) o).time);

                if (c.get(Calendar.YEAR) == cc.get(Calendar.YEAR)) {
                    if (c.get(Calendar.MONTH) == cc.get(Calendar.MONTH)) {
                        if (c.get(Calendar.DAY_OF_MONTH) == cc.get(Calendar.DAY_OF_MONTH)) {
                            return true;
                        }
                    }
                }

                return false;

            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return (int) (time / 13) * 17;
        }

        @Override
        public String toString() {
            return "CalendarItem{" +
                    "type=" + type +
                    ", textview=" + textview.getText().toString() +
                    '}';
        }
    }
}