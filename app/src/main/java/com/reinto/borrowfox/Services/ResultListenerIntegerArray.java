package com.reinto.borrowfox.Services;

public interface ResultListenerIntegerArray {

    void onResult(int chatNumber, int notificationNumber);

}
