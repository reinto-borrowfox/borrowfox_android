package com.reinto.borrowfox.Services;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class APIService {

    public static final String URL_BASE_API = "https://dev.borrowfox.com/api/";
    public static final String URL_BASE_DATA = "https://dev.borrowfox.com/";

    //public static final String URL_BASE_API = "http://10.1.1.36:8082/api/";
    //public static final String URL_BASE_DATA = "http://10.1.1.36:8082/";

    // static final String URL_BASE_API = "https://www.borrowfox.com/api/";
    // public static final String URL_BASE_DATA = "https://www.borrowfox.com/";

    private RequestQueue mRequestQueue;
    private Context context;

    public APIService(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}