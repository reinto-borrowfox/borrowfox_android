package com.reinto.borrowfox.Services;

import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.reinto.borrowfox.App;

import java.util.HashMap;
import java.util.Map;

public class LogService {
    private APIService apiService;
    private static final String url = APIService.URL_BASE_API + "log/error";

    public static final String API = "API";
    public static final String SYSTEM = "SYSTEM";
    public static final String NETWORK = "NETWORK";

    public LogService(APIService apiService) {
        this.apiService = apiService;
    }

    public void log(String message) {
        String log =
                "Manufacturer: " + Build.MANUFACTURER + " || " +
                        "Model: " + Build.MODEL + " || " +
                        "Release: " + Build.VERSION.RELEASE + " || " +
                        //"AppId: " + App.getAppId() + " || " +
                        " ## "
                        + message;

    }

    public void sendLog(final String stackTrace, final String message) {

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(App.TAG, "onErrorResponse: ");
                NetworkResponse networkResponse = error.networkResponse;

                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("data", stackTrace);
                params.put("appVersion", String.valueOf(Build.VERSION.RELEASE));
                params.put("osType", String.valueOf(1));
                params.put("osVersion", String.valueOf(Build.VERSION.CODENAME));
                params.put("machineModel", String.valueOf(Build.MANUFACTURER) + ", " + String.valueOf(Build.MODEL));
                params.put("message", message);

                return params;
            }
        };

        apiService.addToRequestQueue(req);

        /*
        JSONObject params = new JSONObject();
        try {
            params.put("data", stackTrace);
            params.put("appVersion", String.valueOf(Build.VERSION.RELEASE));
            params.put("osType", String.valueOf(1));
            params.put("machineModel", String.valueOf(Build.MANUFACTURER) + ", " + String.valueOf(Build.MODEL));
            params.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("postVillages", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;

                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                }
            }
        });

        Log.i("post", req.toString());
        apiService.addToRequestQueue(req);*/
    }
}
