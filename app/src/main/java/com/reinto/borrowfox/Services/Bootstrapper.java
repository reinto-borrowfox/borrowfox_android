package com.reinto.borrowfox.Services;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.reinto.borrowfox.App;

@SuppressLint("Registered")
public class Bootstrapper extends Activity {

    private static DownloadAPIService downloadAPIService;
    private static APIService apiService;
    private static LogService logService;

    public static APIService getApiService() {
        if (apiService == null)
            apiService = new APIService(App.getAppContext());

        return apiService;
    }

    public static DownloadAPIService getDownloadAPIService() {
        if (downloadAPIService == null)
            downloadAPIService = new DownloadAPIService(getApiService());

        return downloadAPIService;
    }

    public static LogService getLogService() {
        if (logService == null)
            logService = new LogService(getApiService());

        return logService;
    }
}
