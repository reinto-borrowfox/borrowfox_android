package com.reinto.borrowfox.Services;

public interface ResultListenerBoolean<T> {

    void onSuccess();

    void onFail();

}
