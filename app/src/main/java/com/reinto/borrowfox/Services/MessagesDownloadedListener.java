/*
 *
 * MessagesDownloadedListener.java
 * BorrowFox
 *
 * Created by Tomáš Dvořáček on 10.8.16 14:45
 * Copyright (c) 2016 Reinto. All rights reserved.
 */

package com.reinto.borrowfox.Services;

public interface MessagesDownloadedListener<T> {

    void OnTaskDone(T object, int chat_id);

}
