package com.reinto.borrowfox.Services;

import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.reinto.borrowfox.App;
import com.reinto.borrowfox.Emuns;
import com.reinto.borrowfox.Models.Address;
import com.reinto.borrowfox.Models.BorrowData;
import com.reinto.borrowfox.Models.CalendarBlockedDay;
import com.reinto.borrowfox.Models.Card;
import com.reinto.borrowfox.Models.ChatListItem;
import com.reinto.borrowfox.Models.ChatMessage;
import com.reinto.borrowfox.Models.FullItemData;
import com.reinto.borrowfox.Models.GroupModel;
import com.reinto.borrowfox.Models.ItemData;
import com.reinto.borrowfox.Models.LendData;
import com.reinto.borrowfox.Models.NotificationItem;
import com.reinto.borrowfox.Models.SimpleCategoryPlusFox;
import com.reinto.borrowfox.Models.UploadImage;
import com.reinto.borrowfox.Models.User;
import com.reinto.borrowfox.Utilities.Utilities;
import com.reinto.borrowfox.managers.CategoryManager;
import com.reinto.borrowfox.managers.RequestItemManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@SuppressWarnings("TryWithIdenticalCatches")
public class DownloadAPIService implements IDownloadAPIService {

    private APIService apiService;

    public DownloadAPIService(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void getCategories(final TaskDoneListener<List<SimpleCategoryPlusFox>> list) {
        String url = APIService.URL_BASE_API + "category/extended";

        final JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        List<SimpleCategoryPlusFox> result = new ArrayList<>();

                        JSONArray data = (response.getJSONArray("data"));
                        if (data.length() == 0) {
                            list.OnTaskDone(result);
                            return;
                        }


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject temp = data.getJSONObject(i);
                            result.add(new SimpleCategoryPlusFox(temp.getJSONObject("imageItem").getString("filename"),
                                    temp.getJSONObject("categoryItem").getString("name"),
                                    temp.getJSONObject("categoryItem").getInt("id")));
                        }

                        list.OnTaskDone(result);

                    } else {
                        list.OnTaskDone(null);
                        Toast.makeText(apiService.getContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    list.OnTaskDone(null);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getCategoriesUpdated(final TaskDoneListener<List<SimpleCategoryPlusFox>> result) {
        String url = APIService.URL_BASE_API + "category/extendedwithtoppick";


        final JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                List<SimpleCategoryPlusFox> list = new ArrayList<>();

                try {
                    if (response.getInt("status") == 200) {

                        JSONObject data = response.getJSONObject("data");

                        JSONArray categories = data.getJSONArray("categories");

                        for (int i = 0; i < categories.length(); i++) {
                            SimpleCategoryPlusFox plusFox = new SimpleCategoryPlusFox();

                            JSONObject temp = categories.getJSONObject(i);
                            JSONObject first = temp.getJSONObject("categoryItem");
                            plusFox.setName(first.getString("name"));
                            plusFox.setId(first.getInt("id"));
                            plusFox.setType(1);

                            JSONObject second = temp.getJSONObject("imageItem");
                            plusFox.setImage_url(second.getString("filename"));

                            list.add(plusFox);
                        }

                        JSONObject toppick = data.getJSONObject("top_pick");

                        JSONArray images = data.getJSONArray("top_pick_images");

                        ArrayList<String> urls = new ArrayList<>();

                        for (int i = 0; i < images.length(); i++) {
                            urls.add(APIService.URL_BASE_DATA + images.getJSONObject(i).getString("filename"));
                        }

                        list.add(0, new SimpleCategoryPlusFox(null, toppick.getInt("id"),
                                String.valueOf(toppick.getInt("borrow_price") / 100),
                                toppick.getString("name"),
                                toppick.getString("lend_user_locality"),
                                1, urls, 0));







                       /* JSONArray data = (response.getJSONArray("data"));

                        List<SimpleCategoryPlusFox> result = new ArrayList<>();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject temp = data.getJSONObject(i);
                            result.add(new SimpleCategoryPlusFox(temp.getJSONObject("imageItem").getString("filename"),
                                    temp.getJSONObject("categoryItem").getString("name"),

                                   temp.getJSONObject("categoryItem").getInt("id")));
                        }*/

                        Log.d(App.TAG, "DownloadAPIService, onResponse: " + list.toString());

                        result.OnTaskDone(list);
                    } else {
                        result.OnTaskDone(null);
                        Toast.makeText(apiService.getContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    result.OnTaskDone(null);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void register(final String firstName, final String lastName, final String email, final TaskDoneListener<Integer> result) {
        String url = APIService.URL_BASE_API + "user/register";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject o = new JSONObject(response);

                    JSONObject data = o.getJSONObject("data");

                    Utilities.saveUserId(apiService.getContext(), data.getInt("id"));
                    Utilities.saveUserEmail(apiService.getContext(), data.getString("email"));
                    Utilities.saveUserName(apiService.getContext(), data.getString("name"));
                    Utilities.saveUserLocation(apiService.getContext(), data.getString("locality"));

                    result.OnTaskDone(data.getInt("id"));
                } catch (JSONException e) {
                    result.OnTaskDone(0);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(0);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("firstName", firstName);
                params.put("lastName", lastName);
                params.put("email", email);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

   /* private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                //return true; // verify always returns true, which could cause insecure network traffic due to trusting TLS/SSL server certificates for wrong hostnames
                HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                return hv.verify("localhost", session);
            }
        };
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkClientTrusted", e.toString());
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkServerTrusted", e.toString());
                        }
                    }
                }
        };
    }

    private SSLSocketFactory getSSLSocketFactory()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = getResources().openRawResource(R.raw.my_cert); // this cert file stored in \app\src\main\res\raw folder path

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }*/

    @Override
    public void login(String email, String password, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/authenticate?email=" + email + "&password=" + password + "&setLoginTime=1";

        Log.d(App.TAG, "onResponse: login " + url);


       /* HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory());
                    httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };*/


        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(App.TAG, "DownloadAPIService, onResponse: " + response.toString());

                Log.d(App.TAG, "onResponse: login " + response.toString());

                try {
                    switch (response.getInt("status")) {
                        case 200:

                            JSONObject data = response.getJSONObject("data");

                            Log.d(App.TAG, "DownloadAPIService, onResponse: " + data.getString("name"));

                            Utilities.saveUserId(apiService.getContext(), data.getInt("id"));
                            Utilities.saveUserEmail(apiService.getContext(), data.getString("email"));
                            Utilities.saveUserName(apiService.getContext(), data.getString("name"));
                            Utilities.saveUserLocation(apiService.getContext(), data.getString("locality"));

                            result.OnTaskDone(true);

                            break;
                        default:
                            Toast.makeText(apiService.getContext(), "Error", Toast.LENGTH_SHORT).show();
                            result.OnTaskDone(false);
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: errr");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                Log.d(App.TAG, "DownloadAPIService, onErrorResponse: error tu");

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        //final RequestQueue requestQueue = Volley.newRequestQueue(apiService.getContext(), hurlStack);
        //requestQueue.add(req);

        apiService.addToRequestQueue(req);
    }

    @Override
    public void sendPassword(final String password, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/settings/" + Utilities.getUserId(apiService.getContext());

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(App.TAG, "sendPassword - onResponse: " + response);
                result.OnTaskDone(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", password);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getJustCategoryNames(final TaskDoneListener<CategoryManager> result) {
        String url = APIService.URL_BASE_API + "category/extended";

        final JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        JSONArray data = (response.getJSONArray("data"));

                        CategoryManager manager = new CategoryManager();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject temp = data.getJSONObject(i);

                            manager.addId(temp.getJSONObject("categoryItem").getInt("id"));
                            manager.addName(temp.getJSONObject("categoryItem").getString("name"));
                        }

                        manager.removeMostPopular();

                        Log.d(App.TAG, "DownloadAPIService, onResponse: " + manager.toString());

                        result.OnTaskDone(manager);
                    } else {
                        result.OnTaskDone(null);
                        Toast.makeText(apiService.getContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    result.OnTaskDone(null);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }



    @Override
    public void downloadCategoryitems(int category_id, final ResultListenerList<List<ItemData>> result) {
        String url = APIService.URL_BASE_API + "item/extendedsbycategory?categoryId=" + category_id + "&orderType=1";
        Log.d(App.TAG, "DownloadAPIService, downloadCategoryitems: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<ItemData> list = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        ItemData da = new ItemData(ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price") / 100,
                                ex.getInt("additional_borrow_price") / 100,
                                ex.getInt("deposit_price") / 100,
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                ex.isNull("item_image_id") ? 0 : ex.getInt("item_image_id"),
                                ex.getString("user_locality"),
                                ex.getInt("id"),
                                ex.getInt("lend_user_id"),
                                null, 0,
                                ex.getString("user_image_filename"));

                        try {
                            da.setRating(ex.getInt("user_rating"));
                        } catch (JSONException e) {
                            da.setRating(0);
                        }

                        list.add(da);
                    }
                    result.onFilledList(list);
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);

        /*JsonArrayRequest req = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<ItemData> list = new ArrayList<>();

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject o = response.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        list.add(new ItemData(ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price"),
                                ex.getInt("additional_borrow_price"),
                                ex.getInt("deposit_price"),
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                ex.getInt("item_image_id"),
                                ex.getString("user_locality"),
                                ex.getInt("id"),
                                ex.getInt("lend_user_id")));
                    }

                    result.OnTaskDone(list);

                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: error");
                NetworkResponse networkResponse = error.networkResponse;
                result.OnTaskDone(null);
                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.d(TAG, "onErrorResponse: " + code);
                }
            }
        });

        apiService.addToRequestQueue(req);*/
    }

    @Override
    public void downloadImagesForItems(int item_id, final TaskDoneListener<ArrayList<String>> result) {
        final String url = APIService.URL_BASE_API + "item/images/" + item_id + "?limit=6&size=1";

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ArrayList<String> urls = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        urls.add(APIService.URL_BASE_DATA + o.getString("filename"));
                    }
                    result.OnTaskDone(urls);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: downloadImageForItems");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void sendFBInfoToServer(final String email, final String gender, final String fb_id, final String name, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/facebookauthenticate";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    JSONObject data = xx.getJSONObject("data");

                    Utilities.saveUserId(apiService.getContext(), data.getInt("id"));
                    Utilities.saveUserEmail(apiService.getContext(), data.getString("email"));
                    Utilities.saveUserName(apiService.getContext(), data.getString("name"));
                    Utilities.saveUserLocation(apiService.getContext(), data.getString("locality"));

                    result.OnTaskDone(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("gender", gender);
                params.put("id", String.valueOf(fb_id));
                params.put("name", name);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadHomeAddress(final TaskDoneListener<Address> result) {
        String url = APIService.URL_BASE_API + "user/homeaddress/" + Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");

                    Utilities.saveHomeAddressId(apiService.getContext(), data.getInt("id"));
                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + Utilities.getHomeAddressId(apiService.getContext()));

                    Address address = new Address();
                    address.setId(data.getInt("id"));
                    address.setNumber(Integer.valueOf(data.getString("housenumber")));
                    address.setStreet(data.getString("street"));
                    address.setPsc(data.getString("postcode"));
                    address.setCity(data.getString("city"));

                    result.OnTaskDone(address);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadNotificationInfo(final TaskDoneListener<ArrayList<NotificationItem>> result) {
        String url = APIService.URL_BASE_API + "notification/" + Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(null);
                        return;
                    }

                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + response.toString());

                    ArrayList<NotificationItem> list = new ArrayList<>();

                    JSONArray data = response.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject obj = data.getJSONObject(i);
                        NotificationItem item = new NotificationItem(obj.getString("item_name"),
                                obj.getString("type_name"),
                                obj.getInt("item_id"),
                                obj.getString("item_image_filename"),
                                obj.getString("create_time"));
                        try {
                            String x = obj.getString("update_time");
                            Log.d(App.TAG, "DownloadAPIService, onResponse: " + "->" + x + "<-");
                            if (x.equals("null")) {
                                Log.d(App.TAG, "DownloadAPIService, onResponse: is null");
                            } else {
                                item.setDate(x);
                                Log.d(App.TAG, "DownloadAPIService, onResponse: is not null");
                            }
                        } catch (NullPointerException e) {
                            Log.d(App.TAG, "DownloadAPIService, onResponse: is null");
                        }

                        list.add(item);
                    }

                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + list.toString());

                    result.OnTaskDone(list);

                    //                    JSONObject data = response.getJSONObject("data");
                    //
                    //                    Address address = new Address();
                    //                    address.setNumber(Integer.valueOf(data.getString("housenumber")));
                    //                    address.setStreet(data.getString("street"));
                    //                    address.setPsc(data.getString("postcode"));
                    //                    address.setCity(data.getString("city"));
                    //
                    //                    result.OnTaskDone(address);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadChats(final TaskDoneListener<List<ChatListItem>> result) {
        String url = APIService.URL_BASE_API + "chat/?user=" + Utilities.getUserId(apiService.getContext());

        Log.d(App.TAG, "DownloadAPIService, downloadChats: " + url);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(null);
                        return;
                    }

                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + response.toString());

                    ArrayList<ChatListItem> list = new ArrayList<>();

                    JSONArray data = response.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject obj = data.getJSONObject(i);
                        ChatListItem item = new ChatListItem(obj.getString("user_nickname"),
                                obj.getString("user_locality"),
                                obj.getString("create_time"),
                                obj.getString("user_image_filename"),
                                obj.getInt("id"),
                                obj.getString("user_name"),
                                obj.getInt("item_id"),
                                obj.getInt("lend_user_id"));

                        try {
                            String x = obj.getString("update_time");
                            Log.d(App.TAG, "DownloadAPIService, onResponse: " + "->" + x + "<-");
                            if (x.equals("null")) {
                                Log.d(App.TAG, "DownloadAPIService, onResponse: is null");
                            } else {
                                item.setTime(x);
                                Log.d(App.TAG, "DownloadAPIService, onResponse: is not null");
                            }
                        } catch (NullPointerException e) {
                            Log.d(App.TAG, "DownloadAPIService, onResponse: is null");
                        }

                        list.add(item);
                    }

                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + list.toString());

                    result.OnTaskDone(list);

                    //                    JSONObject data = response.getJSONObject("data");
                    //
                    //                    Address address = new Address();
                    //                    address.setNumber(Integer.valueOf(data.getString("housenumber")));
                    //                    address.setStreet(data.getString("street"));
                    //                    address.setPsc(data.getString("postcode"));
                    //                    address.setCity(data.getString("city"));
                    //
                    //                    result.OnTaskDone(address);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadFullItem(final int item_id, final TaskDoneListener<FullItemData> result) {
        final String url = APIService.URL_BASE_API + "item/detail/" + item_id;

        Log.d(App.TAG, "DownloadAPIService, downloadFullItem: " + url);
        Log.d(App.TAG, "DownloadAPIService, getCategoriesUpdated: start" + System.currentTimeMillis());

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(App.TAG, "DownloadAPIService, getCategoriesUpdated: response" + System.currentTimeMillis());

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(null);
                        return;
                    }

                    JSONObject data = response.getJSONObject("data");

                    JSONObject item = data.getJSONObject("item");

                    FullItemData fullItemData = new FullItemData();

                    fullItemData.setId(item.getInt("id"));
                    fullItemData.setLend_user_id(item.getInt("lend_user_id"));
                    fullItemData.setDelivery(item.getInt("delivery"));
                    fullItemData.setName(item.getString("name"));
                    fullItemData.setDescription(item.getString("description"));
                    fullItemData.setPrice_per_day(item.getInt("borrow_price") / 100);
                    fullItemData.setPrice_per_extra_day(item.getInt("additional_borrow_price") / 100);
                    fullItemData.setDeposit(item.getInt("deposit_price") / 100);
                    fullItemData.setMinimum_rental_period(item.getInt("min_interval_count"));
                    fullItemData.setUser_locality(item.getString("lend_user_locality"));
                    fullItemData.setUser_url(item.getString("user_image_filename"));
                    fullItemData.setUser_full_name(item.getString("user_name"));
                    fullItemData.setStatus(item.getInt("status"));

                    List<CalendarBlockedDay> blockedDays = new ArrayList<>();
                    List<CalendarBlockedDay> unavailableDays = new ArrayList<>();

                    JSONArray avaibility = data.getJSONArray("availability");

                    for (int i = 0; i < avaibility.length(); i++) {
                        JSONObject o = avaibility.getJSONObject(i);
                        if (o.getInt("type") == 1) {
                            unavailableDays.add(new CalendarBlockedDay(getLongFromString(o.getString("date")), o.getInt("type")));
                        } else {
                            blockedDays.add(new CalendarBlockedDay(getLongFromString(o.getString("date")), o.getInt("type")));
                        }
                    }

                    Comparator<CalendarBlockedDay> comparator = new Comparator<CalendarBlockedDay>() {
                        @Override
                        public int compare(CalendarBlockedDay calendarBlockedDay, CalendarBlockedDay t1) {
                            if (calendarBlockedDay.getDate() > t1.getDate()) {
                                return 1;
                            } else if (calendarBlockedDay.getDate() < t1.getDate()) {
                                return -1;
                            }

                            return 0;
                        }
                    };

                    Collections.sort(blockedDays, comparator);
                    Collections.sort(unavailableDays, comparator);

                    fullItemData.setBlockedDays(blockedDays);
                    fullItemData.setUnavailableDays(unavailableDays);

                    try {
                        fullItemData.setRating((int) Math.round(item.getDouble("avg_user_rating")));
                    } catch (JSONException e) {
                        // rating is null
                        fullItemData.setRating(0);
                    }


                    JSONArray images = data.getJSONArray("images");

                    ArrayList<String> urls = new ArrayList<>();
                    for (int i = 0; i < images.length(); i++) {
                        urls.add(APIService.URL_BASE_DATA + images.get(i));
                    }

                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + urls.toString());

                    fullItemData.setImages(urls);

                    fullItemData.setCategoryId(data.isNull("categoryId") ? 1 : data.getInt("categoryId"));


                    result.OnTaskDone(fullItemData);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: downloadImageForItems");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void sendChatMessage(final int chat_id, final String message, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "chat/message";

        Log.d(App.TAG, "DownloadAPIService, sendChatMessage: " + chat_id + " " + message + " " + Utilities.getUserId(apiService.getContext()));

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("chatId", String.valueOf(chat_id));
                params.put("fromUserId", String.valueOf(Utilities.getUserId(apiService.getContext())));
                params.put("message", message);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void createNewCard(final String token, final int type, final String name, final String last4, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/addpaymentcard";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userId", String.valueOf(Utilities.getUserId(apiService.getContext())));
                params.put("stripeCardId", token);
                params.put("type", String.valueOf(type));
                params.put("name", String.valueOf(name));
                params.put("lastNumber", last4);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getAllCards(final ResultListenerList<List<Card>> result) {
        String url = APIService.URL_BASE_API + "user/paymentcards?userId=" +
                Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    List<com.reinto.borrowfox.Models.Card> list = new ArrayList<>();

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject obj = data.getJSONObject(i);
                        com.reinto.borrowfox.Models.Card my_card = new com.reinto.borrowfox.Models.Card(obj.getInt("id"),
                                obj.getInt("last_number"), null, obj.getString("name"));

                        switch (obj.getInt("type")) {
                            case 1:
                                my_card.setManuf("VISA");
                                break;
                            case 2:
                                my_card.setManuf("MASTERCARD");
                                break;
                            case 3:
                                my_card.setManuf("AMERICAN EXPRESS");
                                break;
                        }

                        list.add(my_card);
                    }

                    result.onFilledList(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void fullyUpdateProfile(final String first_name, final String last_name, final String email, final String home_city,
                                   final String home_street, final String home_housenumber, final String home_postcode,
                                   final String work_city, final String work_street, final String work_housenumber, final String work_postcode,
                                   final String image_source, String name, String resampleType, final String street, final String city,
                                   final TaskDoneListener<Boolean> result) {

        String url = APIService.URL_BASE_API + "user/saveprofile";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(App.TAG, "DownloadAPIService, onResponse: " + response);
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userId", String.valueOf(Utilities.getUserId(apiService.getContext())));
                params.put("email", email.toLowerCase());
                params.put("firstName", first_name);
                params.put("lastName", last_name);

                params.put("street", street);
                params.put("city", city);

                params.put("home_city", home_city);
                params.put("home_street", home_street);
                params.put("home_housenumber", home_housenumber);
                params.put("home_postcode", home_postcode);
                params.put("home_billing", "1");

                params.put("work_city", work_city);
                params.put("work_street", work_street);
                params.put("work_housenumber", work_housenumber);
                params.put("work_postcode", work_postcode);
                params.put("work_billing", "0");

                params.put("source", image_source);
                params.put("size", "2");
                params.put("name", "profile" + String.valueOf(Utilities.getUserId(apiService.getContext())));

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void fullyUpdateProfileNoImage(final String first_name, final String last_name, final String email,
                                          final String home_city, final String home_street,
                                          final String home_housenumber, final String home_postcode,
                                          final String work_city, final String work_street, final String work_housenumber,
                                          final String work_postcode, final String street, final String city, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/saveprofile";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(App.TAG, "DownloadAPIService, onResponse: " + response);
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userId", String.valueOf(Utilities.getUserId(apiService.getContext())));
                params.put("email", email.toLowerCase());
                params.put("firstName", first_name);
                params.put("lastName", last_name);

                params.put("street", street);
                params.put("city", city);

                params.put("home_city", home_city);
                params.put("home_street", home_street);
                params.put("home_housenumber", home_housenumber);
                params.put("home_postcode", home_postcode);
                params.put("home_billing", "1");

                params.put("work_city", work_city);
                params.put("work_street", work_street);
                params.put("work_housenumber", work_housenumber);
                params.put("work_postcode", work_postcode);
                params.put("work_billing", "0");

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getProfileImage(final TaskDoneListener<String> result) {
        String url = APIService.URL_BASE_API + "user/image/" + Utilities.getUserId(apiService.getContext()) + "?size=4";

        Log.d(App.TAG, "DownloadAPIService, getProfileImage: " + url);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");

                    result.OnTaskDone(APIService.URL_BASE_DATA + data.getString("filename"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone("");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone("");
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void openChatAndGetMessages(final int my_id, int lender_id, int item_id, int offset, final MessagesDownloadedListener<List<ChatMessage>> result) {
        String url = APIService.URL_BASE_API + "chat/openmessage?borrower=" + my_id +
                "&lender=" + lender_id +
                "&item=" + item_id +
                "&offset=" + offset;

        Log.d(App.TAG, "DownloadAPIService, openChatAndGetMessages: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<ChatMessage> list = new ArrayList<>();

                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject chatItem = data.getJSONObject("chatItem");

                    int chat_id = chatItem.getInt("id");

                    JSONArray messages = data.getJSONArray("chatMessageItems");

                    for (int i = 0; i < messages.length(); i++) {

                        JSONObject m = messages.getJSONObject(i);

                        if (my_id == m.getInt("from_user_id")) {
                            list.add(new ChatMessage(0, getLongFromString(m.getString("create_time")), m.getString("message"), "", chat_id));
                        } else {
                            list.add(new ChatMessage(1, getLongFromString(m.getString("create_time")), m.getString("message"), "", chat_id));
                        }
                    }

                    result.OnTaskDone(list, chat_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(null, 0);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null, 0);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void disableChat(final int chat_id, final int user_id, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "chat/disable?chatId=" + chat_id + "&readingUserId=" + user_id;
        Log.d(App.TAG, "DownloadAPIService, disableChat: " + url);

        /*Map<String, String> params = new HashMap<>();
        params.put("chatId", String.valueOf(chat_id));
        params.put("readingUserId", String.valueOf(user_id));

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                Log.d(App.TAG, "onErrorResponse: ");
                NetworkResponse networkResponse = error.networkResponse;

                Log.d(App.TAG, "DownloadAPIService, onErrorResponse: " + new String(error.networkResponse.data));

                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                }
            }
        });*/


        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("chatId", String.valueOf(chat_id));
                params.put("readingUserId", String.valueOf(user_id));

                return params;
            }*/
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void createNewItem(final int lender_id, final String item_name, final String decription, final int category_id,
                              final int borrowPrice, final int additionalBorrowPrice, final int depositPrice,
                              final int minimalIntervalCount, final int delivery_type, final ArrayList<UploadImage> list,
                              final ArrayList<String> days, final TaskDoneListener<Boolean> result) {

        String url = APIService.URL_BASE_API + "item/addcomplete";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(App.TAG, "DownloadAPIService, onResponse: " + response);
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                JSONObject item = new JSONObject();
                JSONObject da = new JSONObject();
                JSONObject finale = new JSONObject();

                Map<String, String> params = new HashMap<>();

                try {
                    item.put("lendUserId", String.valueOf(lender_id));
                    item.put("delivery", String.valueOf(delivery_type));
                    item.put("name", String.valueOf(item_name));
                    item.put("description", String.valueOf(decription));
                    item.put("borrowPrice", String.valueOf(borrowPrice * 100));
                    item.put("additionalBorrowDiscount", String.valueOf(additionalBorrowPrice));
                    item.put("depositPrice", String.valueOf(depositPrice * 100));
                    item.put("minIntervalCount", String.valueOf(minimalIntervalCount));
                    item.put("status", String.valueOf(1));
                    item.put("priceCurrency", String.valueOf(1));
                    item.put("intervalType", String.valueOf(1));

                    JSONArray a = new JSONArray();
                    a.put(category_id);



                    JSONObject iiima = new JSONObject();

                    for (int i = 0; i < list.size(); i++) {
                        JSONObject temp = new JSONObject();
                        temp.put("source", list.get(i).getData());
                        temp.put("size", String.valueOf(1));
                        temp.put("name", list.get(i).getName());

                        iiima.put("image" + i, temp);
                    }

                    for (int i = 0; i < days.size(); i++) {
                        da.put("day", days.get(i));
                    }

                    JSONArray daysss = new JSONArray();
                    for (int i = 0; i < days.size(); i++) {
                        daysss.put(days.get(i));
                    }

                    finale.put("item", item);
                    finale.put("categories", a);
                    finale.put("unavailables", daysss);
                    finale.put("images", iiima);

                    params.put("item", item.toString());
                    params.put("categories", a.toString());
                    params.put("unavailables", daysss.toString());
                    params.put("images", iiima.toString());

                    //String string = params.toString();
                    //  Utilities.printDebugTxtFile(string);

                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                    return null;
                }

                Log.d(App.TAG, "DownloadAPIService, getParams: " + new JSONObject(params).toString());

                Utilities.printDebugTxtFile(new JSONObject(params).toString());

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(120000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadItemByLender(final int status, final ResultListenerList<List<LendData>> result) {
        String url = APIService.URL_BASE_API + "item/extendedsbylender/" +
                Utilities.getUserId(apiService.getContext()) + "?eventStatus=" + status + "&itemStatus=1";

        if (status == Emuns.LendItemType.LEND_OFFLINE) {
            url = APIService.URL_BASE_API + "item/extendedsbylender/" +
                    Utilities.getUserId(apiService.getContext()) + "?itemStatus=0";
        }

        Log.d(App.TAG, "DownloadAPIService, downloadItemByLender: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<LendData> list = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        LendData da = new LendData(
                                ex.getInt("id"),
                                ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price") / 100,
                                ex.getInt("additional_borrow_price") / 100,
                                ex.getInt("deposit_price") / 100,
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                ex.isNull("item_image_id") ? 0 : ex.getInt("item_image_id"),
                                ex.isNull("requested_event_id") ? 0 : ex.getInt("requested_event_id"),
                                ex.isNull("loan_event_begin_date") ? 0 : getLongFromString(ex.getString("loan_event_begin_date")),
                                ex.isNull("loan_event_end_date") ? 0 : getLongFromString(ex.getString("loan_event_end_date")),
                                ex.isNull("requested_event_begin_date") ? 0 : getLongFromString(ex.getString("requested_event_begin_date")),
                                ex.isNull("requested_event_end_date") ? 0 : getLongFromString(ex.getString("requested_event_end_date")),
                                ex.isNull("returned_event_begin_date") ? 0 : getLongFromString(ex.getString("returned_event_begin_date")),
                                ex.isNull("returned_event_end_date") ? 0 : getLongFromString(ex.getString("returned_event_end_date")),
                                ex.isNull("accepted_event_begin_date") ? 0 : getLongFromString(ex.getString("accepted_event_begin_date")),
                                ex.isNull("accepted_event_end_date") ? 0 : getLongFromString(ex.getString("accepted_event_end_date"))
                                , status);

                        list.add(da);
                    }
                    result.onFilledList(list);
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }


    @Override
    public void acceptEvent(final int eventId, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "event/accept";

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("eventId", String.valueOf(eventId));
                params.put("userId", String.valueOf(Utilities.getUserId(apiService.getContext())));

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void cancelEvent(final int eventId, final ResultListenerBoolean<Boolean> result) {
        String url = APIService.URL_BASE_API + "event/cancel?eventId=" + eventId + "&userId=" + Utilities.getUserId(apiService.getContext());

        Log.d(App.TAG, "DownloadAPIService, cancelEvent: " + url);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onFail();
                        return;
                    }

                    result.onSuccess();
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.onFail();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onFail();
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadUserData(int userId, final TaskDoneListener<User> result) {
        String url = APIService.URL_BASE_API + "user/extended/" + userId;

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Toast.makeText(apiService.getContext(), "Error", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    JSONObject dataa = response.getJSONObject("data");
                    JSONObject data = dataa.getJSONObject("userItem");

                    User user = new User(
                            data.getInt("id"),
                            data.getString("first_name"),
                            data.getString("last_name"),
                            data.getString("email"),
                            data.getString("locality"),
                            data.getString("aboutme"),
                            dataa.getInt("userRating"),
                            dataa.getString("userImageFilename"));

                    result.OnTaskDone(user);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadLendItems(int userId, final ResultListenerList<List<ItemData>> result) {
        String url = APIService.URL_BASE_API + "item/extendedsbylender/" + userId + "?itemStatus=1";

        Log.d(App.TAG, "DownloadAPIService, downloadCategoryitems: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<ItemData> list = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        ItemData da = new ItemData(
                                ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price") / 100,
                                ex.getInt("additional_borrow_price") / 100,
                                ex.getInt("deposit_price") / 100,
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                0,
                                ex.getString("user_locality"),
                                ex.getInt("id"),
                                ex.getInt("lend_user_id"),
                                null, 0,
                                "");


                        list.add(da);
                    }
                    result.onFilledList(list);
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloaditemByBorrower(final int status, final ResultListenerList<List<BorrowData>> result) {
        String url = APIService.URL_BASE_API + "item/extendedsbyborrower/" +
                Utilities.getUserId(apiService.getContext()) + "?eventStatus=" + status + "&itemStatus=1";

        Log.d(App.TAG, "DownloadAPIService, downloaditemByBorrower: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<BorrowData> list = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        BorrowData da = new BorrowData(
                                ex.getInt("id"),
                                ex.getInt("event_id"),
                                ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price") / 100,
                                ex.getInt("additional_borrow_price") / 100,
                                ex.getInt("deposit_price") / 100,
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                ex.isNull("item_image_id") ? 0 : ex.getInt("item_image_id"),
                                ex.isNull("event_begin_time") ? 0 : getLongFromString(ex.getString("event_begin_time")),
                                ex.isNull("event_end_time") ? 0 : getLongFromString(ex.getString("event_end_time")),
                                status,
                                ex.isNull("user_image_filename") ? "" : ex.getString("user_image_filename"),
                                "",
                                ex.isNull("user_locality") ? "" : ex.getString("user_locality"),
                                ex.getInt("lend_user_id"),
                                ex.getInt("event_borrow_price") / 100
                        );

                        list.add(da);
                    }
                    result.onFilledList(list);
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void searchForItems(String keyWord, String from, String to, int maxPrice, int orderType, int categoryId, final ResultListenerList<List<ItemData>> result) {
        String url = APIService.URL_BASE_API + "item/extendedsbysearch?searching=";

        if (!keyWord.equals(" ")) {
            url = url + keyWord;
        } else {
            url = url + "%20"; // space
        }

        if (orderType != 0) {
            url = url + "&orderType=" + orderType;
        }

        if (!from.equals("")) {
            url = url + "&from=" + from;
        }

        if (!to.equals("")) {
            url = url + "&from=" + to;
        }

        if (maxPrice != 0) {
            url = url + "&maxPrice=" + maxPrice;
        }


        //        if (!keyWord.equals(" ")) {
        //            url = url + keyWord + "&orderType=";
        //        } else {
        //            url = url + "%20" + "&orderType=";
        //        }
        //
        //        if (orderType != 0) {
        //            url = url + orderType + "&from=";
        //        } else {
        //            url = url + 0 + "&from=";
        //        }
        //
        //        url = url + from + "&to=";
        //        url = url + to;
        //
        //        if (categoryId != 0) {
        //            url = url + "&category=" + categoryId + "&maxPrice=";
        //        } else {
        //            url = url + "&maxPrice=";
        //        }
        //
        //        url = url + maxPrice;

        Log.d(App.TAG, "DownloadAPIService, downloadCategoryitems: " + url);

        JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<ItemData> list = new ArrayList<>();

                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onNullResult(null);
                        return;
                    }

                    JSONArray data = response.getJSONArray("data");

                    if (data.length() == 0) {
                        result.onEmptyList(list);
                        return;
                    }

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject o = data.getJSONObject(i);
                        JSONObject ex = o.getJSONObject("extendedItemItem");

                        ItemData da = new ItemData(ex.getString("name"),
                                ex.getString("description"),
                                ex.getInt("borrow_price") / 100,
                                ex.getInt("additional_borrow_price") / 100,
                                ex.getInt("deposit_price") / 100,
                                ex.getInt("min_interval_count"),
                                ex.getInt("delivery"),
                                ex.isNull("item_image_id") ? 0 : ex.getInt("item_image_id"),
                                ex.getString("user_locality"),
                                ex.getInt("id"),
                                ex.getInt("lend_user_id"),
                                null, 0,
                                ex.getString("user_image_filename"));

                        try {
                            da.setRating(ex.getInt("user_rating"));
                        } catch (JSONException e) {
                            da.setRating(0);
                        }

                        list.add(da);
                    }
                    result.onFilledList(list);
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: ");
                    e.printStackTrace();
                    result.onNullResult(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onNullResult(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getChatAndNotifNumber(final ResultListenerIntegerArray result) {
        String url = APIService.URL_BASE_API + "user/unread/" + Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: status is not 200");
                        result.onResult(-1, -1);
                        return;
                    }

                    JSONObject data = response.getJSONObject("data");

                    result.onResult(data.getInt("newMessageCount"), data.getInt("newNotificationCount"));
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: JSON parse error");
                    result.onResult(-1, -1);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onResult(-1, -1);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void isUserBankActive(final ResultListenerBooleanTwo result) {
        String url = APIService.URL_BASE_API + "user/bankactive/" + Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: status is not 200");
                        result.onResult(false);
                        return;
                    }

                    result.onResult(response.getBoolean("data"));
                } catch (JSONException e) {
                    Log.d(App.TAG, "DownloadAPIService, onResponse: JSON parse error");
                    result.onResult(false);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onResult(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void makeRequest(final int itemId, final int borrowUserId, final int paymentCardId,
                            final String beginDate, final String endDate, final int deliveryType,
                            final long beginTime, final long endTime, final int beginAddressId,
                            final int endAddressId, final ResultListenerBoolean result) {

        Log.d(App.TAG, "makeRequest() called with: " + "itemId = [" + itemId + "], borrowUserId = [" +
                borrowUserId + "], paymentCardId = [" + paymentCardId + "], beginDate = [" + beginDate +
                "], endDate = [" + endDate + "], deliveryType = [" + deliveryType + "], beginTime = [" +
                beginTime + "], endTime = [" + endTime + "], beginAddressId = [" + beginAddressId + "], endAddressId = [" +
                endAddressId + "], result = [" + result + "]");

        String url = APIService.URL_BASE_API + "event/request";

        Log.d(App.TAG, "DownloadAPIService, makeRequest: " + url);

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onFail();
                        return;
                    }

                    result.onSuccess();
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.onFail();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onFail();
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("itemId", String.valueOf(itemId));
                params.put("borrowUserId", String.valueOf(borrowUserId));
                params.put("paymentCardId", String.valueOf(paymentCardId));
                params.put("beginDate", beginDate);
                params.put("endDate", endDate);
                params.put("deliveryType", String.valueOf(deliveryType));
                params.put("beginTime", DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(beginTime)).toString());
                params.put("endTime", DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(endTime)).toString());
                params.put("beginAddressId", String.valueOf(beginAddressId));
                params.put("endAddressId", String.valueOf(endAddressId));

                Log.d(App.TAG, "DownloadAPIService, getParams: " + new JSONObject(params).toString());

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void makeRequest2(final RequestItemManager.RequestItem item, final ResultListenerBoolean result) {
        Log.d(App.TAG, "DownloadAPIService, makeRequest2: " + item.toString());
        result.onSuccess();

        String url = APIService.URL_BASE_API + "event/request";

        Log.d(App.TAG, "DownloadAPIService, makeRequest: " + url);

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.onFail();
                        return;
                    }

                    result.onSuccess();
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.onFail();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.onFail();
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("itemId", String.valueOf(item.itemId));
                params.put("borrowUserId", String.valueOf(item.borrowUserId));
                params.put("paymentCardId", String.valueOf(item.paymentCardId));
                params.put("beginDate", Utilities.getFormatedDateFromLong2(item.beginDate));
                params.put("endDate", Utilities.getFormatedDateFromLong2(item.endDate));
                params.put("deliveryType", String.valueOf(item.deliveryType));
                params.put("beginTime", DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(item.beginTime)).toString());
                params.put("endTime", DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(item.endTime)).toString());
                params.put("beginAddressId", String.valueOf(item.beginAddressId));
                params.put("endAddressId", String.valueOf(item.endAddressId));

                Log.d(App.TAG, "DownloadAPIService, getParams: " + new JSONObject(params).toString());

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    private long getLongFromString(String text) {
        Date d;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            d = sdf.parse(text);

            return d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return System.currentTimeMillis();
    }

    @Override
    public void saveUserInfo(final String first_name, final String surname, final String email, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/settings/" + Utilities.getUserId(apiService.getContext());

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    Utilities.saveUserEmail(apiService.getContext(), email);
                    Utilities.saveUserName(apiService.getContext(), first_name + " " + surname);

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email.toLowerCase());
                params.put("firstName", first_name);
                params.put("lastName", surname);

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void updateHomeAddress(final String city, final String street, final String housenumber, final String postCode, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/homeaddress/" + Utilities.getUserId(apiService.getContext());

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("street", street);
                params.put("city", city);
                params.put("housenumber", housenumber);
                params.put("postcode", postCode);
                params.put("billing", "1");

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void updateWorkAddress(final String city, final String street, final String housenumber, final String postCode, final TaskDoneListener<Boolean> result) {
        String url = APIService.URL_BASE_API + "user/workaddress/" + Utilities.getUserId(apiService.getContext());

        StringRequest req = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject xx = new JSONObject(response);
                    if (xx.getInt("status") != 200) {
                        Log.d(App.TAG, "DownloadAPIService, onResponse: response is not 200");
                        result.OnTaskDone(false);
                        return;
                    }

                    result.OnTaskDone(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.OnTaskDone(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(false);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("street", street);
                params.put("city", city);
                params.put("housenumber", housenumber);
                params.put("postcode", postCode);
                params.put("billing", "0");

                return params;
            }
        };

        apiService.addToRequestQueue(req);
    }

    @Override
    public void downloadWorkAddress(final TaskDoneListener<Address> result) {
        String url = APIService.URL_BASE_API + "user/workaddress/" + Utilities.getUserId(apiService.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");

                    if (data.getString("housenumber").equals("")) {
                        result.OnTaskDone(null);
                        return;
                    }

                    Utilities.saveWorkAddressId(apiService.getContext(), data.getInt("id"));
                    Log.d(App.TAG, "DownloadAPIService, onResponse: " + Utilities.getWorkAddressId(apiService.getContext()));

                    Address address = new Address();
                    address.setNumber(Integer.valueOf(data.getString("housenumber")));
                    address.setStreet(data.getString("street"));
                    address.setPsc(data.getString("postcode"));
                    address.setCity(data.getString("city"));

                    result.OnTaskDone(address);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.d(App.TAG, "DownloadAPIService, onResponse: address does not exists");
                    result.OnTaskDone(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }

    /*Groups*/

    @Override
    public void getGroups(final TaskDoneListener<List<GroupModel>> list) {
        String url = APIService.URL_BASE_API + "category/extended";

        final JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        List<GroupModel> result = new ArrayList<>();

                        JSONArray data = (response.getJSONArray("data"));
                        if (data.length() == 0) {
                            list.OnTaskDone(result);
                            return;
                        }


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject temp = data.getJSONObject(i);
                            result.add(new GroupModel(temp.getJSONObject("imageItem").getString("filename"),
                                    temp.getJSONObject("categoryItem").getString("name"),
                                    temp.getJSONObject("categoryItem").getInt("id")));
                        }

                        list.OnTaskDone(result);

                    } else {
                        list.OnTaskDone(null);
                        Toast.makeText(apiService.getContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    list.OnTaskDone(null);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        apiService.addToRequestQueue(req);
    }

    @Override
    public void getGroupsUpdated(final TaskDoneListener<List<GroupModel>> result) {
        String url = APIService.URL_BASE_API + "category/extendedwithtoppick";


        final JsonObjectRequest req = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                List<GroupModel> list = new ArrayList<>();

                try {
                    if (response.getInt("status") == 200) {

                        JSONObject data = response.getJSONObject("data");

                        JSONArray categories = data.getJSONArray("categories");

                        for (int i = 0; i < categories.length(); i++) {
                            GroupModel groupModel = new GroupModel();

                            JSONObject temp = categories.getJSONObject(i);
                            JSONObject first = temp.getJSONObject("categoryItem");
                            groupModel.setName(first.getString("name"));
                            groupModel.setId(first.getInt("id"));
                            groupModel.setType(1);

                            JSONObject second = temp.getJSONObject("imageItem");
                            groupModel.setImage_url(second.getString("filename"));

                            list.add(groupModel);
                        }

                        JSONObject toppick = data.getJSONObject("top_pick");

                        JSONArray images = data.getJSONArray("top_pick_images");

                        ArrayList<String> urls = new ArrayList<>();

                        for (int i = 0; i < images.length(); i++) {
                            urls.add(APIService.URL_BASE_DATA + images.getJSONObject(i).getString("filename"));
                        }

                        list.add(0, new GroupModel(null, toppick.getInt("id"),
                                String.valueOf(toppick.getInt("borrow_price") / 100),
                                toppick.getString("name"),
                                toppick.getString("lend_user_locality"),
                                1, urls, 0));

                        Log.d(App.TAG, "DownloadAPIService, onResponse: " + list.toString());

                        result.OnTaskDone(list);
                    } else {
                        result.OnTaskDone(null);
                        Toast.makeText(apiService.getContext(), "error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    result.OnTaskDone(null);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                result.OnTaskDone(null);
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                if (networkResponse != null) {
                    int code = error.networkResponse.statusCode;
                    Log.i("postLog", "ERROR response CODE: " + code);
                    Log.d(App.TAG, "DownloadAPIService, serverResponseData  : " + new String(error.networkResponse.data));
                }
            }
        });

        apiService.addToRequestQueue(req);
    }



}

