package com.reinto.borrowfox.Services;

public interface ResultListenerList<T> {

    void onNullResult(T result);

    void onEmptyList(T result);

    void onFilledList(T result);

}
