package com.reinto.borrowfox.Services;

public interface TaskDoneListener<T> {

    void OnTaskDone(T object);
}
