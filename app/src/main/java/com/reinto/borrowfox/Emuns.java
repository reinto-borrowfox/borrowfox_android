package com.reinto.borrowfox;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Emuns {

    @IntDef({LendItemType.LEND_REQUESTED, LendItemType.LEND_ON_LOAN, LendItemType.LEND_AVAILABLE, LendItemType.LEND_OFFLINE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LendItemType {
        int LEND_AVAILABLE = 0;
        int LEND_REQUESTED = 1;
        int LEND_OFFLINE = 2;
        int LEND_ON_LOAN = 5;
    }

    @IntDef({BorrowItemType.BORROW_BORROWING, BorrowItemType.BORROW_PREV_BORROWED, BorrowItemType.BORROW_REQUESTED, BorrowItemType.BORROW_BOOKED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface BorrowItemType {
        int BORROW_REQUESTED = 1;
        int BORROW_BORROWING = 5;
        int BORROW_BOOKED = 4;
        int BORROW_PREV_BORROWED = 6;
    }


    @IntDef({DeliveryType.PICK_UP, DeliveryType.COURRIER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DeliveryType {
        int PICK_UP = 1;
        int COURRIER = 2;
    }

}
